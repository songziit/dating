//
//  COriginalBarButtonItem.m
//  Dating
//
//  Created by neil on 2019/10/22.
//  Copyright © 2019 admin. All rights reserved.
//

#import "COriginalBarButtonItem.h"

@implementation COriginalBarButtonItem

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setTarget:self];
    [self setAction:@selector(click_item:)];
    
}

/**
 *  @brief 返回事件
 *
 *  @param sender item
 */
- (void)click_item:(id)sender
{
    if (self.needBack && self.parentController) {        
        [self.parentController.navigationController popViewControllerAnimated:YES];
    }
}


- (void)setImage:(UIImage *)image{
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [super setImage:image];
}

@end
