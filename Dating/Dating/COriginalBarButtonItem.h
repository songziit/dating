//
//  COriginalBarButtonItem.h
//  Dating
//
//  Created by neil on 2019/10/22.
//  Copyright © 2019 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface COriginalBarButtonItem : UIBarButtonItem

@property (nonatomic,weak) IBOutlet UIViewController *parentController;
@property (assign,nonatomic) IBInspectable BOOL needBack;

@end

NS_ASSUME_NONNULL_END
