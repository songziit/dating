//
//  DATMainVC.m
//  Dating
//
//  Created by neil on 2019/10/21.
//  Copyright © 2019 admin. All rights reserved.
//

#import "DATMainVC.h"
#import "DATUserItemCell.h"
#import "DATUserActionCell.h"
#import "DATUserInfoVC.h"

@interface DATMainVC ()

@end

@implementation DATMainVC

static NSString * const reuseIdentifier = @"Cell";
static NSString * const actionIdentifier = @"ActionCell";


- (void)viewDidLoad {
    [super viewDidLoad];
    self.collectionView.backgroundColor = [UIColor colorWithHexString:@"000B28"];
    self.navigationController.navigationBar.barTintColor = self.collectionView.backgroundColor;
    [self setNavigationBarTransparent:NO];
    [self.collectionView registerNib:DATUserItemCell.nib forCellWithReuseIdentifier:reuseIdentifier];
    [self.collectionView registerNib:DATUserActionCell.nib forCellWithReuseIdentifier:actionIdentifier];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (section==1) {
        return 1;
    }
    return 20;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = nil;
    if (indexPath.section==1) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:actionIdentifier forIndexPath:indexPath];
    }
    else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return CGSizeMake(SCREEN_BOUND_SIZE.width-30, 72.f);
    }
    
    NSInteger columnNumber = 2;
    NSInteger minimumColumnSpacing = 10;
    NSInteger edge = 30;
    
    CGFloat width = SCREEN_BOUND_SIZE.width;
    CGFloat columnInterspace = (columnNumber-1)*minimumColumnSpacing;
    width = (width - columnInterspace-edge)/columnNumber;
    CGFloat height =  250.f/168.f*width;
    CGSize ret = CGSizeMake(width, height);
    return ret;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        DATUserInfoVC *nextVC = [DATUserInfoVC new];
        [self.navigationController pushViewController:nextVC animated:YES];
    }else{
        
    }
    
}

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
