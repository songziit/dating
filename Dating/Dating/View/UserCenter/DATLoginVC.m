//
//  DATLoginVC.m
//  Dating
//
//  Created by cailu on 2019/10/21.
//  Copyright © 2019 admin. All rights reserved.
//

#import "DATLoginVC.h"
#import "DATExplainVC.h"

@interface DATLoginVC ()<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *startBtn;
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation DATLoginVC

+ (instancetype)new {
    UIStoryboard *aBoard = [UIStoryboard storyboardWithName:@"UserCenterBoard" bundle:nil];
    return [aBoard instantiateViewControllerWithIdentifier:NSStringFromClass(self)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self initLab];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setNavigationBarTransparent:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
//    [self setNavigationBarTransparent:NO];
    [super viewWillDisappear:animated];
}

- (void)initLab {
    self.textView.delegate = self;
    NSString *text = self.textView.text;
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:text attributes:
                                         @{NSFontAttributeName: [UIFont systemFontOfSize:12],
                                           NSForegroundColorAttributeName: [UIColor whiteColor]
                                         }];
    NSDictionary *attributes = @{
        NSLinkAttributeName : @"http://lawsandpolicies",
        NSUnderlineStyleAttributeName : [NSNumber numberWithInteger:NSUnderlineStyleSingle]
    };
    [string addAttributes:attributes range:[text rangeOfString:@"laws and policies"]];
    [string addAttributes:attributes range:[text rangeOfString:@"user agreement"]];
    self.textView.attributedText = string;
    self.textView.textAlignment = NSTextAlignmentCenter;
    self.textView.selectable = true;
    self.textView.dataDetectorTypes = UIDataDetectorTypeLink;
}

#pragma mark - UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL
         inRange:(NSRange)characterRange interaction:(UITextItemInteraction)interaction {
    if (NSEqualRanges([textView.text rangeOfString:@"user agreement"], characterRange)) {
        DATExplainVC *vc = [DATExplainVC new];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (NSEqualRanges([textView.text rangeOfString:@"laws and policies"], characterRange)) {
        DATExplainVC *vc = [DATExplainVC new];
        [self.navigationController pushViewController:vc animated:YES];
    }
    return NO;
}
@end
