//
//  CGradientView.h
//  Dating
//
//  Created by neil on 2019/10/21.
//  Copyright © 2019 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CGradientView : UIView

@property (strong,nonatomic) IBInspectable UIColor *beginColor;
@property (strong,nonatomic) IBInspectable UIColor *endColor;
@property (assign,nonatomic) IBInspectable CGPoint beginPoint;
@property (assign,nonatomic) IBInspectable CGPoint endPoint;

@end

NS_ASSUME_NONNULL_END
