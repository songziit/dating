//
//  AppDelegate.h
//  Dating
//
//  Created by admin on 21/10/2019.
//  Copyright © 2019 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong,nonatomic) UIWindow *window;

@end

