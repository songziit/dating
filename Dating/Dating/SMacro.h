
#ifndef SMacro_h
#define SMacro_h


#define SCREEN_BOUND_SIZE [UIView screenSize]
#define SCREEN_BOUND [UIScreen mainScreen].bounds

#undef DEF_CHECK_VIEWERROR
#define DEF_CHECK_VIEWERROR \
NSError *error = [self.view checkRequired]; \
if (error) {    \
[MessageBox showError:error];   \
return; \
}   \

#define kStatusBarHeight [[UIApplication sharedApplication] statusBarFrame].size.height
#define kNavBarHeight 44.0
#define kTopHeight (kStatusBarHeight + kNavBarHeight)

#ifdef DEBUG
#define NSLog(...) NSLog(__VA_ARGS__)
#else
#define NSLog(...)
#endif

static NSString *TermsLisense = @"http://www.apechat.online/terms_of_use.html";
static NSString *PolicyLisense = @"http://www.apechat.online/privacy_policy.html";


#endif /* SMacro_h */
