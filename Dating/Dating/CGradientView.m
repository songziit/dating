//
//  CGradientView.m
//  Dating
//
//  Created by neil on 2019/10/21.
//  Copyright © 2019 admin. All rights reserved.
//

#import "CGradientView.h"

@implementation CGradientView

- (void)awakeFromNib{
    [super awakeFromNib];

    CAGradientLayer *gl = [CAGradientLayer layer];
    gl.frame = self.bounds;
    gl.startPoint = self.beginPoint;
    gl.endPoint = self.endPoint;
    gl.colors = @[(__bridge id)self.beginColor.CGColor,(__bridge id)self.endColor.CGColor];
    gl.locations = @[@(0.0),@(1.0)];    
    [self.layer addSublayer:gl];
}

@end
