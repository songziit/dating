//
//  AppDelegate.m
//  Dating
//
//  Created by admin on 21/10/2019.
//  Copyright © 2019 admin. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()


@end

@implementation AppDelegate

#pragma mark - Customize RootVC

- (void)customize{
    [self customizeWindow];
    [self customizeRootVC];
}

- (void)customizeWindow{
    self.window = [[UIWindow alloc] initWithFrame:SCREEN_BOUND];
    self.window.tintColor = [UIColor colorWithHexString:@"010820"];
    [self.window becomeKeyWindow];
    [self.window makeKeyAndVisible];
}

- (UIViewController*)getRootVCWithConfig:(NSArray<NSDictionary<NSString*,NSString*>*>*)config{
    UITabBarController *rootVC = [[UITabBarController alloc] init];
    
    NSMutableArray *controllers = [NSMutableArray array];
    for (NSDictionary<NSString*,NSString*> *item in config) {
        NSString *boardName = [item stringForKey:@"boardName"];
        UIStoryboard *board = [UIStoryboard storyboardWithName:boardName bundle:nil];
        UIViewController *itemVC = board.instantiateInitialViewController;
        
        NSString *imageName = [item stringForKey:@"imageName"];
        NSString *imageName_h = [NSString stringWithFormat:@"%@_h",imageName];
        UIImage *image = [UIImage imageNamed:imageName];
        UIImage *image_h = [UIImage imageNamed: imageName_h];
        if (image) {
            itemVC.tabBarItem.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        if (image_h) {
            itemVC.tabBarItem.selectedImage = [image_h imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        }
        UIEdgeInsets imageInsets = UIEdgeInsetsZero;
        CGFloat heightOffset = rootVC.tabBar.height - image.size.height;
        if (heightOffset>0) {
            imageInsets.bottom = -heightOffset/2.f;
        }else{
            imageInsets.bottom = 7.f;
        }
        itemVC.tabBarItem.imageInsets = imageInsets;
        itemVC.tabBarItem.title = nil;
        
        UINavigationController *navVC = (id)itemVC;
        [navVC.navigationBar setBarTintColor:[UIColor colorWithHexString:@"000B28"]];
        navVC.navigationBar.tintColor = UIColor.whiteColor;
        [navVC.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
        [navVC.topViewController setNavigationBarTransparent:NO];
        
        [controllers addObject:itemVC];
    }
    [rootVC setViewControllers:controllers];
    rootVC.tabBar.barTintColor = [UIColor colorWithHexString:@"0B0A20"];
    return rootVC;
}

- (void)customizeRootVC{
    NSArray<NSDictionary<NSString*,NSString*>*> *configValue = @[
      @{
        @"boardName": @"MainBoard",
        @"imageName": @"MainTabBar"
      },
      @{
        @"boardName": @"MatchBoard",
        @"imageName": @"MatchTabBar"
      },
      @{
        @"boardName": @"ChatBoard",
        @"imageName": @"ChatTabBar"
      },
      @{
        @"boardName": @"FansBoard",
        @"imageName": @"FansTabBar"
      },
      @{
        @"boardName": @"UserCenterBoard",
        @"imageName": @"UserCenterTabBar"
      }
    ];
    UIViewController *rootVC = [self getRootVCWithConfig:configValue];
    self.window.rootViewController = rootVC;   
}

#pragma mark - Customize Delegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self customize];
    return YES;
}

@end
