#
# Be sure to run `pod lib lint DATCommon.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DATCommon'
  s.version          = '0.1.0'
  s.summary          = 'A short description of DATCommon.'
  s.description      = 'DATCommon UI And Base View'
  s.homepage         = 'https://github.com/shulianyong/DATCommon'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'shulianyong' => 'shulianyong@163.com' }
  s.source           = { :git => 'https://github.com/shulianyong/DATCommon.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.module_name   = 'DATCommon'
  s.frameworks = 'Foundation','UIKit', 'MapKit'
  s.source_files = 'Classes/**/*.{h,m}'
  s.resources = ['Classes/**/*.{png,xib,storyboard}']
  s.prefix_header_file = 'DATCommon-Prefix.pch'

   s.dependency 'ReactiveCocoa', '~> 2.0'
  # s.dependency 'JSONModel', '~> 1.0'
  # # s.dependency 'Objection', '~> 1.6'
   s.dependency 'SDWebImage', '~> 5.2'
   s.dependency 'MJRefresh', '~> 3.1.0'
   s.dependency 'MBProgressHUD'
   s.dependency 'IQKeyboardManager'
   s.dependency 'MQLogger', '~> 0.1'
   s.dependency 'MQKit'
end
