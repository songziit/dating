//
//  BannerView.h
//  Loan
//
//  Created by neil on 2017/3/13.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BannerView;

@protocol BannerViewImage <NSObject>

@property(nonatomic, readonly) NSString *image;

@end

@protocol BannerViewDelegate <NSObject>

- (NSInteger)numberOfBannerView:(BannerView*)bannerView;
- (id<BannerViewImage>)bannerView:(BannerView*)bannerView imageOfPageNumber:(NSInteger)pageNumber;
- (void)bannerView:(BannerView*)bannerView didSelected:(NSInteger)pageNumber;

@end

@interface BannerView : UIView

@property(nonatomic, weak) id<BannerViewDelegate> delegate;
@property(nonatomic) NSInteger pageNumber;
- (void)start;
- (void)stop;

@end
