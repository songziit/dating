//
//  SColorButton.m
//  Social
//
//  Created by neil on 2019/9/2.
//  Copyright © 2019 songzi. All rights reserved.
//

#import "SColorButton.h"

@implementation SColorButton

- (void)awakeFromNib{
    [super awakeFromNib];
    
    UIImage *image = nil;
    if (self.normalColor) {
        image = [UIImage imageWithColor:self.normalColor];
        [self setBackgroundImage:image forState:UIControlStateNormal];
    }
    if (self.highlightedColor) {
        image = [UIImage imageWithColor:self.highlightedColor];
        [self setBackgroundImage:image forState:UIControlStateHighlighted];
    }
    if (self.selectedColor) {
        image = [UIImage imageWithColor:self.selectedColor];
        [self setBackgroundImage:image forState:UIControlStateSelected];
    }
    if (self.disableColor) {
        image = [UIImage imageWithColor:self.disableColor];
        [self setBackgroundImage:image forState:UIControlStateDisabled];
    }
    
}

@end
