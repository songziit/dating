//
//  CodeButtonView.m
//  SuperRunErrands
//
//  Created by neil on 2018/12/7.
//  Copyright © 2018 Sly. All rights reserved.
//

#import "CodeButtonView.h"
@interface CodeButtonView ()

@property (nonatomic,weak) IBOutlet UIButton *btnResendCode;
@property (weak, nonatomic) IBOutlet UILabel *lblCodeCountDown;
@property (strong,nonatomic) IBInspectable NSString *countFormat;

@end

@implementation CodeButtonView

- (void)awakeFromNib{
    [super awakeFromNib];
    if ([NSString isEmpty:self.countFormat]) {
        self.countFormat = LOCALIZATION(@"%@ seconds");
    }
    @weakify(self);
    [[TimerCounter sharedInstance] endCount];
    [[RACObserve([TimerCounter sharedInstance], codeCount) takeUntil:self.viewController.rac_viewWillDisappear] subscribeNext:^(NSNumber *x) {
        @strongify(self);
        [self setState:CodeButtonViewStateCount withCount:[x integerValue]];
    }];
    [[RACObserve([TimerCounter sharedInstance], countEnd) takeUntil:self.viewController.rac_viewWillDisappear] subscribeNext:^(id x) {
        @strongify(self);
        [self setState:CodeButtonViewStateTitle];
    }];
}

- (void)setState:(CodeButtonViewState)state{
    _state = state;
    if (self.btnResendCode==nil || self.lblCodeCountDown==nil) {
        return;
    }
    if (state == CodeButtonViewStateDisable) {
        self.btnResendCode.hidden = NO;
        self.btnResendCode.enabled = NO;
        self.lblCodeCountDown.hidden = YES;
    }
    else if (state == CodeButtonViewStateCount) {
        self.btnResendCode.hidden = YES;
        self.lblCodeCountDown.hidden = NO;
    }else{
        self.btnResendCode.hidden = NO;
        self.btnResendCode.enabled = YES;  
        self.lblCodeCountDown.hidden = YES;
    }
}

- (void)setState:(CodeButtonViewState)state withCount:(NSInteger)number{
    if (self.btnResendCode==nil || self.lblCodeCountDown==nil) {
        return;
    }
    [self setState:state];
    if (state == CodeButtonViewStateCount) {
        self.lblCodeCountDown.text = [NSString stringWithFormat:self.countFormat,@(number)];
    }else{
        self.lblCodeCountDown.text = @"";
    }
}

@end
