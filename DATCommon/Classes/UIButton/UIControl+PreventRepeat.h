//
//  UIControl+PreventRepeat.h
//  SuperRunErrands
//
//  Created by neil on 2019/1/28.
//  Copyright © 2019 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIControl (PreventRepeat)

@property(nonatomic,assign)NSTimeInterval timeInterval;//用这个给重复点击加间隔
@property(nonatomic,assign)BOOL isIgnoreEvent;//YES不允许点击NO允许点击

@end

NS_ASSUME_NONNULL_END
