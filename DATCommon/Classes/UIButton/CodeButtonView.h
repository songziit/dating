//
//  CodeButtonView.h
//  SuperRunErrands
//
//  Created by neil on 2018/12/7.
//  Copyright © 2018 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSInteger {
    CodeButtonViewStateDisable,
    CodeButtonViewStateCount,
    CodeButtonViewStateTitle,
} CodeButtonViewState;

@interface CodeButtonView : UIView

@property (nonatomic,assign) CodeButtonViewState state;
- (void)setState:(CodeButtonViewState)state withCount:(NSInteger)number;

@end

NS_ASSUME_NONNULL_END
