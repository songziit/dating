//
//  EyeButton.m
//  SuperRunErrandsUser
//
//  Created by neil on 2018/12/12.
//  Copyright © 2018 Sly. All rights reserved.
//

#import "EyeButton.h"
@interface EyeButton()


@end

@implementation EyeButton

- (void)awakeFromNib{
    [super awakeFromNib];
    [self addTarget:self action:@selector(click_btnEye:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)click_btnEye:(id)sender{
    self.selected = !self.selected;
    UIView *superView = self.superview;
    for (UIView *aSubView in superView.subviews) {
        if ([aSubView isKindOfClass:UITextField.class]) {
            UITextField *txtValue = (UITextField*)aSubView;
            txtValue.secureTextEntry = !self.selected;
        }
    }   
}

@end
