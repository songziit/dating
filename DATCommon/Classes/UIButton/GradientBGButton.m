//
//  GradientBGButton.m
//  Social
//
//  Created by neil on 2019/8/30.
//  Copyright © 2019 songzi. All rights reserved.
//

#import "GradientBGButton.h"

@implementation GradientBGButton

- (void)awakeFromNib {
    [super awakeFromNib];
    if (self.beginColor != nil && self.endColor != nil) {
        if (CGPointEqualToPoint(self.endPoint, CGPointZero)) {
            self.endPoint = CGPointMake(1, 0);
        }
        [self setBackgroundImage:[UIImage imageGradientWithSize:self.size
                                                     beginColor:self.beginColor
                                                       endColor:self.endColor
                                                     beginPoint:self.beginPoint
                                                       endPoint:self.endPoint]
                        forState:UIControlStateNormal];
    }
    if (self.disableColor) {
        [self setBackgroundImage:[UIImage imageWithColor:self.disableColor] forState:UIControlStateDisabled];
    }
}

@end
