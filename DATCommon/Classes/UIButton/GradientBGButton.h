//
//  GradientBGButton.h
//  Social
//
//  Created by neil on 2019/8/30.
//  Copyright © 2019 songzi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GradientBGButton : UIButton

@property (strong,nonatomic) IBInspectable UIColor *beginColor;
@property (strong,nonatomic) IBInspectable UIColor *endColor;
@property (strong,nonatomic) IBInspectable UIColor *disableColor;
@property (assign,nonatomic) IBInspectable CGPoint beginPoint;
@property (assign,nonatomic) IBInspectable CGPoint endPoint;
@end

NS_ASSUME_NONNULL_END
