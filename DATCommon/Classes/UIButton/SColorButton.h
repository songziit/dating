//
//  SColorButton.h
//  Social
//
//  Created by neil on 2019/9/2.
//  Copyright © 2019 songzi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SColorButton : UIButton

@property (strong,nonatomic) IBInspectable UIColor *normalColor;
@property (strong,nonatomic) IBInspectable UIColor *selectedColor;
@property (strong,nonatomic) IBInspectable UIColor *highlightedColor;
@property (strong,nonatomic) IBInspectable UIColor *disableColor;

@end

NS_ASSUME_NONNULL_END
