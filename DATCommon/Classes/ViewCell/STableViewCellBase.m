//
//  STableViewCellBase.m
//  SuperRunErrandsUser
//
//  Created by neil on 2018/5/23.
//  Copyright © 2018年 Sly. All rights reserved.
//

#import "STableViewCellBase.h"

@interface STableViewCellBase()

@property (nonatomic,strong) RACSignal *disposeSignal;

@end

@implementation STableViewCellBase

- (void)awakeFromNib {
    [super awakeFromNib];
    self.disposeSignal = [RACSubject subject];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dataWillChange{    
    [(RACSubject*)self.disposeSignal sendNext:nil];
}

- (void)removeFromSuperview{
    [super removeFromSuperview];    
    [(RACSubject*)self.disposeSignal sendNext:nil];
}

@end
