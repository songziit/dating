//
//  STableViewCellBase.h
//  SuperRunErrandsUser
//
//  Created by neil on 2018/5/23.
//  Copyright © 2018年 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STableViewCellBase : UITableViewCell

@property (nonatomic,readonly) RACSignal *disposeSignal;

@end
