//
//  ImageCollectionViewCell.m
//  Loan
//
//  Created by neil on 2017/3/13.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@interface ImageCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imgvCover;


@end

@implementation ImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dataDidChange
{
    NSString *image = self.data;
    if([image isKindOfClass:[NSString class]])
    {
        [ImageShow showImageInImageView:self.imgvCover withPath:image];
    }
}

@end
