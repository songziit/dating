//
//  BannerView.m
//  Loan
//
//  Created by neil on 2017/3/13.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import "BannerView.h"
#import "ImageCollectionViewCell.h"

@interface BannerView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property(nonatomic, strong) NSTimer *bannerTimer;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;


@end

@implementation BannerView
@synthesize pageNumber=_pageNumber;

+ (instancetype)createInstance {
    return [self loadFromNib];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView registerNib:[ImageCollectionViewCell nib] forCellWithReuseIdentifier:@"ImageCollectionViewCell"];
}

#pragma mark - 计算页数

- (NSInteger)maxPageNumber
{
    NSInteger maxPageNumber = 0;
    if(self.delegate && [self.delegate respondsToSelector:@selector(numberOfBannerView:)])
    {
        maxPageNumber = [self.delegate numberOfBannerView:self];
    }
    return maxPageNumber;
}

- (void)setPageNumber:(NSInteger)pageNumber
{
    [self.collectionView reloadData];
    [self setPageNumber:pageNumber animated:NO];
}

- (void)setPageNumber:(NSInteger)pageNumber animated:(BOOL)animated
{
    [self willChangeValueForKey:@"pageNumber"];
    _pageNumber = pageNumber;
    [self.collectionView reloadData];
    CGPoint contentOffset = self.collectionView.contentOffset;
    contentOffset.x = 0;
    [self.collectionView setContentOffset:contentOffset];
    contentOffset.x = self.collectionView.width;
    if(animated)
    {
        [self.collectionView setContentOffset:contentOffset animated:animated];
       
    }
    else{
        [self.collectionView setContentOffset:contentOffset];
    }    
    [self didChangeValueForKey:@"pageNumber"];
    self.pageControl.numberOfPages = [self maxPageNumber];
    self.pageControl.currentPage = self.pageNumber;
}

- (NSInteger)pageNumberWithNumber:(NSInteger)pageNumber
{
    if(pageNumber<0)
    {
        pageNumber = self.maxPageNumber-1;
    }
    else if (pageNumber>=self.maxPageNumber)
    {
        pageNumber = 0;
    }
    return  pageNumber;
}

#pragma mark - 轮播


- (void)start {
    [self stop];
    self.bannerTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(imageTimerOperation) userInfo:nil repeats:YES];
}

- (void)stop {
    if (self.bannerTimer) {
        [self.bannerTimer invalidate];
        self.bannerTimer = nil;
    }
}

- (void)imageTimerOperation {
    if (self.maxPageNumber>0) {
        self.userInteractionEnabled = NO;
        @weakify(self);
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            @strongify(self);
            self.userInteractionEnabled = YES;
            NSInteger pageNumber = self.pageNumber+1;
            pageNumber = [self pageNumberWithNumber:pageNumber];
            [self setPageNumber:pageNumber animated:YES];
        });
    }
}

- (void)removeFromSuperview {
    [self stop];
    [super removeFromSuperview];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger number = 0;
    if(self.maxPageNumber>0)
    {
        number = 3;
    }
    return number;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCollectionViewCell" forIndexPath:indexPath];
    NSInteger pageNumber = self.pageNumber;
    NSInteger offset = indexPath.row-1;
    pageNumber+=offset;    
    pageNumber = [self pageNumberWithNumber:pageNumber];
    id<BannerViewImage> image = [self.delegate bannerView:self imageOfPageNumber:pageNumber];
    cell.data = [image image];
    return cell;
}


#pragma mark - UICollectionViewDelegate


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self.delegate bannerView:self didSelected:self.pageNumber];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.bounds.size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0.f;
}

#pragma mark - UIScrollViewDelegate

- (void)updatePageEvent:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        NSInteger pageNumber = fabs(scrollView.contentOffset.x) / scrollView.width;
        pageNumber = pageNumber-1;
        if(pageNumber!=0)
        {
            pageNumber = self.pageNumber+pageNumber;
            pageNumber = [self pageNumberWithNumber:pageNumber];
            [self setPageNumber:pageNumber animated:NO];
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        [self stop];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.collectionView) {
        [self updatePageEvent:scrollView];
        [self start];
    }
}

@end
