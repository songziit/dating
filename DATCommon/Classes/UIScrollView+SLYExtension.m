//
//  UIScrollView+SLYExtension.m
//  MJRefreshExample
//
//  Created by neil on 15/5/13.
//  Copyright (c) 2015年 itcast. All rights reserved.
//

#import "UIScrollView+SLYExtension.h"
#import <MJRefresh/MJRefresh.h>
#import <objc/runtime.h>
#import "UIImage+GIF.h"

static char SupportRefresh, SupportPage, RefreshingBlock, PageingBlock;
@implementation UIScrollView (SLYExtension)

#pragma mark - 刷新

- (void)setSupportRefresh:(BOOL)supportRefresh {
    objc_setAssociatedObject(self, &SupportRefresh, @(supportRefresh), OBJC_ASSOCIATION_COPY_NONATOMIC);
    if (supportRefresh) {
        [self customizeRefresh];
    } else {
        [self.mj_header removeFromSuperview];
    }
}

- (BOOL)supportRefresh {
    return [objc_getAssociatedObject(self, &SupportRefresh) boolValue];
}

- (void)setRefreshingBlock:(void (^)(void))refreshingBlock {
    __weak typeof(self) weakself = self;
    void (^tempBlock)(void) = ^{
        [weakself endPageing];
        refreshingBlock();
    };
    objc_setAssociatedObject(self, &RefreshingBlock, tempBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.mj_header.refreshingBlock = tempBlock;
}

- (void (^)(void))refreshingBlock {
    void (^refreshingBlock)(void) = objc_getAssociatedObject(self, &RefreshingBlock);
    return refreshingBlock;
}

- (void)customizeRefresh {
    if (self.mj_header) {
        return;
    }
    self.mj_header = [MJRefreshNormalHeader createInstance];
    self.mj_footer = [MJRefreshBackNormalFooter createInstance];
    
    return;
    /*
    // 添加动画图片的下拉刷新
    // 设置回调（一旦进入刷新状态，就调用target的action，也就是调用self的loadNewData方法）
    self.mj_header = [MJRefreshGifHeader headerWithRefreshingBlock:self.refreshingBlock];

    // 设置普通状态的动画图片
    NSMutableArray *idleImages = [NSMutableArray array];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"refreshWillLoad" ofType:@"gif"];
    NSData *data = [NSData dataWithContentsOfFile:path];
    UIImage *temp = [UIImage sd_animatedGIFWithData:data];
    if(temp)
        [idleImages addObjectsFromArray:temp.images];

    [(MJRefreshGifHeader *)self.mj_header setImages:idleImages forState:MJRefreshStateIdle];

    // 设置即将刷新状态的动画图片（一松开就会刷新的状态）
    NSMutableArray *refreshingImages = [NSMutableArray array];
    path = [[NSBundle mainBundle] pathForResource:@"refreshLoading" ofType:@"gif"];
    data = [NSData dataWithContentsOfFile:path];
    temp = [UIImage sd_animatedGIFWithData:data];
    if(temp)
        [refreshingImages addObjectsFromArray:temp.images];
    [(MJRefreshGifHeader *)self.mj_header setImages:@[ idleImages.lastObject ] forState:MJRefreshStatePulling];

    // 设置正在刷新状态的动画图片
    [(MJRefreshGifHeader *)self.mj_header setImages:refreshingImages
                                        duration:refreshingImages.count * (1.f / 24.f)
                                        forState:MJRefreshStateRefreshing];

    // 马上进入刷新状态
    //    [self.gifHeader beginRefreshing];
     */
}

#pragma mark - 分页

- (void)setSupportPage:(BOOL)supportPage {
    objc_setAssociatedObject(self, &SupportPage, @(supportPage), OBJC_ASSOCIATION_COPY_NONATOMIC);
    if (supportPage) {
        if (self.mj_footer) {
            [self.mj_footer resetNoMoreData];
        } else {
            [self customizePage];
        }
    } else {
        [self.mj_footer setState:MJRefreshStateNoMoreData];
    }
}

- (BOOL)supportPage {
    return [objc_getAssociatedObject(self, &SupportPage) boolValue];
}

- (void)setPageingBlock:(void (^)(void))pageingBlock {
    __weak typeof(self) weakself = self;
    void (^tempBlock)(void) = ^{
        [weakself endRefreshing];
        pageingBlock();
    };

    objc_setAssociatedObject(self, &PageingBlock, tempBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.mj_footer.refreshingBlock = tempBlock;
}

- (void (^)(void))pageingBlock {
    return objc_getAssociatedObject(self, &PageingBlock);
}

- (void)customizePage {
    if (self.mj_footer) {
        return;
    }
    self.mj_footer = [MJRefreshAutoGifFooter footerWithRefreshingBlock:self.pageingBlock];
    // 设置正在刷新状态的动画图片
    NSMutableArray *refreshingImages = [NSMutableArray array];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"refreshLoading" ofType:@"gif"];
    NSData *data = [NSData dataWithContentsOfFile:path];    
    UIImage *temp = [UIImage sd_imageWithGIFData:data];
    
    [refreshingImages addObjectsFromArray:temp.images];
    [(MJRefreshAutoGifFooter *)self.mj_footer setImages:refreshingImages
                                            duration:refreshingImages.count * (1.f / 24.f)
                                            forState:MJRefreshStateRefreshing];
}

#pragma mark - 结束

- (void)endRefreshing {
    if (self.mj_header.isRefreshing) {
        [self.mj_header endRefreshing];
    }
}
- (void)endPageing {
    if (self.mj_footer.isRefreshing) {
        [self.mj_footer endRefreshing];
    }
}

- (void)endRefreshOrPage {
    [self endRefreshing];
    [self endPageing];
}

-(void)hiddenFooter:(BOOL)hidden{
    self.mj_footer.hidden = hidden;
}
@end
