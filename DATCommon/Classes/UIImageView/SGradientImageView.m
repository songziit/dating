//
//  SGradientImageView.m
//  Social
//
//  Created by neil on 2019/9/2.
//  Copyright © 2019 songzi. All rights reserved.
//

#import "SGradientImageView.h"

@implementation SGradientImageView

- (void)awakeFromNib{
    [super awakeFromNib];
    if (self.beginColor!=nil && self.endColor != nil){
        if (CGPointEqualToPoint(self.endPoint, CGPointZero)) {
            self.endPoint = CGPointMake(1, 0);
        }        
        UIImage *image = [UIImage imageGradientWithSize:self.size
                                             beginColor:self.beginColor
                                               endColor:self.endColor
                                             beginPoint:self.beginPoint
                                               endPoint:self.endPoint];
        self.image = image;
    }
}

@end
