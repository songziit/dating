//
//  UIImageView+SLYExtension.m
//  SuperRunErrands
//
//  Created by neil on 2018/12/21.
//  Copyright © 2018 Sly. All rights reserved.
//

#import "UIImageView+SLYExtension.h"
#import <objc/runtime.h>

static const char kUIHttpImage;
static const char kUILocalImageName;
static const char kUILocalImagePath;

@implementation UIImageView (SLYExtension)

- (void)setHttpImage:(NSString *)httpImage{
    if ([NSString isEmpty:httpImage]) {
        return;
    }
    if (![httpImage hasPrefix:@"http"]) {        
        httpImage = [NSString stringWithFormat:@"%@/%@",ImageHttpHost,httpImage];
    }
    objc_setAssociatedObject(self, &kUIHttpImage, httpImage, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [ImageShow showImageInImageView:self withPath:httpImage];
}

- (void)setLocalImageName:(NSString *)localImageName{
    if ([NSString isEmpty:localImageName]) {
        return;
    }
    objc_setAssociatedObject(self, &kUILocalImageName, localImageName, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    UIImage *image = [UIImage imageNamed:localImageName];
    self.image = image;
}

- (void)setLocalImagePath:(NSString *)localImagePath{
    if ([NSString isEmpty:localImagePath]) {
        return;
    }
    objc_setAssociatedObject(self, &kUILocalImagePath, localImagePath, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    UIImage *image = [UIImage imageWithContentsOfFile:localImagePath];
    self.image = image;
}

- (NSString *)httpImage{
    
    return objc_getAssociatedObject(self, &kUIHttpImage);
}

- (NSString *)localImageName{
    return objc_getAssociatedObject(self, &kUILocalImageName);    
}

- (NSString *)localImagePath{
    return objc_getAssociatedObject(self, &kUILocalImagePath);
}

@end
