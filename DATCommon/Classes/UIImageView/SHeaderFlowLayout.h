//
//  SHeaderFlowLayout.h
//  Social
//
//  Created by neil on 2019/8/30.
//  Copyright © 2019 songzi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SHeaderFlowLayout : UICollectionViewFlowLayout

@property (nonatomic, assign) IBInspectable CGFloat navHeight;
@property (nonatomic, assign) BOOL isFloat;

@end

NS_ASSUME_NONNULL_END
