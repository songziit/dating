//
//  UIImageView+SLYExtension.h
//  SuperRunErrands
//
//  Created by neil on 2018/12/21.
//  Copyright © 2018 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (SLYExtension)

@property (nonatomic,strong) NSString *httpImage;
@property (nonatomic,strong) NSString *localImageName;
@property (nonatomic,strong) NSString *localImagePath;

@end

NS_ASSUME_NONNULL_END
