//
//  ImageShow.h
//  Loan
//
//  Created by neil on 2017/3/3.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  @brief 图片展示样式
 */
typedef NS_ENUM(NSUInteger, ImageShowType) {
    /**
     *  @brief 默认图片，正常图片
     */
    DefaultImageType,
    /**
     *  @brief 圆形图片
     */
    CircleImageType,
    /**
     *  @brief 圆角
     */
    RectImageType,
};


@interface ImageShow : NSObject

+ (UIImage*_Nullable)placeholderImage;

+ (UIImage*_Nullable)errorImage;

+ (void)showImageInImageView:(UIImageView *_Nullable)aImageView
                    withPath:(NSString *_Nullable)aImagePath
            placeholderImage:(UIImage *_Nullable)placeholderImage;

+ (void)showImageInButton:(UIButton *_Nullable)aImageView
                 withPath:(NSString *_Nullable)aImagePath;

+ (void)showImageInButton:(UIButton *_Nullable)aImageView
                 withPath:(NSString *_Nullable)aImagePath
         placeholderImage:(nullable UIImage *)placeholder;

+ (void)showImageInButton:(UIButton *_Nullable)aImageView
                 withPath:(NSString *_Nullable)aImagePath
                 forState:(UIControlState)state;


/**
 *  @brief 展示图片
 *
 *  @param aImageView       需要展示的UIImageView
 *  @param aImagePath       图片路径，图片名，或图片url
 */
+ (void)showImageInImageView:(UIImageView *_Nullable)aImageView
                    withPath:(NSString *_Nullable)aImagePath;

+ (void)showImageInImageView:(__weak UIImageView *_Nullable)aImageView
                    withPath:(NSString *_Nullable)aImagePath
            placeholderImage:(UIImage *_Nullable)placeholderImage
        withOriginalImageURL:(NSString *_Nullable)aOriginalImageURL
               withImageType:(ImageShowType)aImageType
              withImageBlock:(void (^_Nullable)(UIImage * _Nullable imgData))aBlock;

+ (void)showImageInImageView:(UIImageView *_Nullable)aImageView
                    withPath:(NSString *_Nullable)aImagePath
               withImageType:(ImageShowType)aImageType;

+ (NSString*_Nullable)urlPathWithUrl:(NSString*_Nullable)url;

@end
