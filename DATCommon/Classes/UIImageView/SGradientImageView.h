//
//  SGradientImageView.h
//  Social
//
//  Created by neil on 2019/9/2.
//  Copyright © 2019 songzi. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SGradientImageView : UIImageView


@property (strong,nonatomic) IBInspectable UIColor *beginColor;
@property (strong,nonatomic) IBInspectable UIColor *endColor;
@property (assign,nonatomic) IBInspectable CGPoint beginPoint;
@property (assign,nonatomic) IBInspectable CGPoint endPoint;

@end

NS_ASSUME_NONNULL_END
