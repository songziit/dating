//
//  ImageShow.m
//  Loan
//
//  Created by neil on 2017/3/3.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import "ImageShow.h"
#import <SDWebImage/UIButton+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDImageCache.h>

@implementation ImageShow

+ (UIImage*)placeholderImage
{
    return [UIImage imageNamed:@"Placeholder"];
}

+ (UIImage*)errorImage
{
    return nil;
}

+ (void)showImageInImageView:(UIImageView *)aImageView
                    withPath:(NSString *)aImagePath
            placeholderImage:(UIImage *)placeholderImage
{
    [self showImageInImageView:aImageView
                      withPath:aImagePath
              placeholderImage:placeholderImage
          withOriginalImageURL:nil
                 withImageType:DefaultImageType
                withImageBlock:nil];
}

+ (void)showImageInImageView:(UIImageView *)aImageView
                    withPath:(NSString *)aImagePath {
    UIImage *placeholder = aImageView.image?:self.placeholderImage;
    [self showImageInImageView:aImageView
                      withPath:aImagePath
              placeholderImage:placeholder];
}

+ (void)showImageInImageView:(UIImageView *)aImageView
                    withPath:(NSString *)aImagePath
               withImageType:(ImageShowType)aImageType {
    [self showImageInImageView:aImageView
                      withPath:aImagePath
              placeholderImage:nil
          withOriginalImageURL:nil
                 withImageType:aImageType
                withImageBlock:nil];
}


+ (void)showImageInImageView:(__weak UIImageView *)aImageView
                    withPath:(NSString *)aImagePath
            placeholderImage:(UIImage *)placeholderImage
        withOriginalImageURL:(NSString *)aOriginalImageURL
               withImageType:(ImageShowType)aImageType
              withImageBlock:(void (^)(UIImage *imgData))aBlock {
    if ([NSString isEmpty:aImagePath]) {
        aImageView.image = placeholderImage;
        return;
    }    
    
    //网络图片处理
    if ([aImagePath hasPrefix:@"http"] && ![aImagePath.lastPathComponent isEqualToString:aImagePath]) {
        void (^completionBlock)(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) = ^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (error && error.code != NSURLErrorCancelled) {
                aImageView.image = self.errorImage;
                if (aBlock) {
                    aBlock(nil);
                }
            } else {
                [self showImageView:aImageView
                              image:[self originalImage:image forImageType:aImageType forPath:aImagePath]
                     withImageBlock:aBlock];
            }
        };
        NSURL *url = [NSURL URLWithString:aImagePath];
        [aImageView sd_setImageWithURL:url
                      placeholderImage:placeholderImage
                               options:SDWebImageRetryFailed
                             completed:completionBlock];
    } else {  //本地图片处理
        UIImage *imgValue = nil;
        //应用内的一张图片，直接是图片名字
        if ([aImagePath.lastPathComponent isEqualToString:aImagePath]) {
            imgValue = [UIImage imageNamed:aImagePath];
        }
        //本地文件路径，获取图片
        else if ([aImagePath isAbsolutePath]) {
            imgValue = [UIImage imageWithContentsOfFile:aImagePath];
        }
        
        imgValue = imgValue ? imgValue : placeholderImage;
        if (imgValue) {
            imgValue = [self originalImage:imgValue forImageType:aImageType forPath:aImagePath];
            [self showImageView:aImageView
                          image:imgValue
                 withImageBlock:aBlock];
        }
    }
}

+ (void)showImageView:(UIImageView *)aImageView
                image:(UIImage *)image
       withImageBlock:(void (^)(UIImage *imgData))aBlock {
    if (aBlock) {
        aBlock(image);
    } else {
        if (image) {
            aImageView.image = image;
        }
    }
}

+ (UIImage *)originalImage:(UIImage *)originalImage forImageType:(ImageShowType)aImageType forPath:(NSString *)aImagePath {
    if (aImagePath == nil) {
        return nil;
    }
    //    SDImageCache
    UIImage *imgProcess = originalImage;
    NSString *cacheKey = [NSString stringWithFormat:@"%@%lu", aImagePath, (unsigned long)aImageType];
    switch (aImageType) {
        case CircleImageType:
            
            imgProcess = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:cacheKey];
            if (imgProcess == nil) {
                imgProcess = [originalImage circularImageWithDiamter:originalImage.size.height / 2.f];
                [[SDImageCache sharedImageCache] storeImage:imgProcess forKey:cacheKey completion:nil];
            }
            break;
        case RectImageType:
            imgProcess = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:cacheKey];
            if (imgProcess == nil) {
                imgProcess = [originalImage roundedRectImageWithSize:originalImage.size withCornerRadius:originalImage.size.height / 7.f];
                [[SDImageCache sharedImageCache] storeImage:imgProcess forKey:cacheKey completion:nil];
            }
            break;
        default:
            break;
    }
    return imgProcess;
}

+ (void)showImageInButton:(UIButton *)aImageView
                 withPath:(NSString *)aImagePath{
    [ImageShow showImageInButton:aImageView withPath:aImagePath forState:UIControlStateNormal];
}

+ (void)showImageInButton:(UIButton *)aImageView
                 withPath:(NSString *)aImagePath
         placeholderImage:(nullable UIImage *)placeholder{
    [ImageShow showImageInButton:aImageView withPath:aImagePath forState:UIControlStateNormal placeholderImage:placeholder];
}

+ (void)showImageInButton:(UIButton *)aImageView
                 withPath:(NSString *)aImagePath
                 forState:(UIControlState)state{
    
    [ImageShow showImageInButton:aImageView withPath:aImagePath forState:state placeholderImage:nil];
}

+ (void)showImageInButton:(UIButton *)aImageView
                 withPath:(NSString *)aImagePath
                 forState:(UIControlState)state
         placeholderImage:(nullable UIImage *)placeholder{
    if (aImageView==nil) {
        return;
    }
    if ([NSString isEmpty:aImagePath]) {
        return;
    }
    NSURL *url = [NSURL URLWithString:aImagePath];
    UIImage *image = placeholder?:[aImageView imageForState:state];
    [aImageView sd_setBackgroundImageWithURL:url forState:state placeholderImage:image];
    
}

+ (NSString*)urlPathWithUrl:(NSString*)url{
    NSString *ret = url;
    if (![NSString isEmpty:ret] && ![ret hasPrefix:@"http"]) {
        ret = [NSString stringWithFormat:@"%@/%@",ImageHttpHost,ret];
    }
    return ret;
}

@end
