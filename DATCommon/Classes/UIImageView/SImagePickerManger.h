//
//  ImageUploadManger.h
//  SuperRunErrands
//
//  Created by neil on 2019/1/4.
//  Copyright © 2019 Sly. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^SImageUploadMangerBlock)(UIImage *image);

@interface SImagePickerManger : NSObject

+ (instancetype)sharedInstance;
@property (nonatomic,assign) CGFloat defaultWidth;
- (void)getImageWithEditType:(BOOL)editType withBlock:(SImageUploadMangerBlock)block;

@end

NS_ASSUME_NONNULL_END
