//
//  ImageUploadManger.m
//  SuperRunErrands
//
//  Created by neil on 2019/1/4.
//  Copyright © 2019 Sly. All rights reserved.
//

#import "SImagePickerManger.h"
#import "MQActionSheet.h"
#import <MQKit/UIImage+FishKit.h>

@interface SImagePickerManger()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@end

@implementation SImagePickerManger
+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.defaultWidth = 500;
    }
    return self;
}

- (UIViewController*)rootVC{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIViewController *rootController = window.rootViewController;
    if (rootController.presentedViewController) {
        rootController = rootController.presentedViewController;
    }
    return rootController;
}

- (void)getImageWithEditType:(BOOL)editType withBlock:(SImageUploadMangerBlock)block{
    @weakify(self);
    MQAlertAction *cameraAction = [MQAlertAction actionWithTitle:LOCALIZATION(@"Use Camera")
                                                           style:MQAlertActionStyleDefault
                                                         handler:^(MQAlertAction *action) {
        @strongify(self);
        [self showImageToolWithCamera:YES withEditType:editType withBlock:block];
    }];
    MQAlertAction *imagesAction = [MQAlertAction actionWithTitle:LOCALIZATION(@"Use Photo Album")
                                                           style:MQAlertActionStyleDefault
                                                         handler:^(MQAlertAction *action) {
        @strongify(self);
        [self showImageToolWithCamera:NO withEditType:editType withBlock:block];
    }];
    MQAlertAction *cancelAction = [MQAlertAction actionWithTitle:LOCALIZATION(@"Cancel")
                                                           style:MQAlertActionStyleCancel
                                                         handler:^(MQAlertAction *action) {
        [MQActionSheet dismiss];
    }];
    id actions = @[cameraAction,imagesAction,cancelAction];
    [MQActionSheet showWithTitle:LOCALIZATION(@"Choose Photo") message:nil withAlertActions:actions];
}

#pragma mark - 相机相册处理 UIImagePickerControllerDelegate

- (void)showImageToolWithCamera:(bool)isCamera withEditType:(BOOL)editType withBlock:(SImageUploadMangerBlock)block{
    if(isCamera){
        if(![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            [MessageBox showMessage:LOCALIZATION(@"No Cameras")];
            return;
        }
    }else{
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
            [MessageBox showMessage:LOCALIZATION(@"Failed to obtain album")];
            return;
        }
    }
    UIImagePickerController *picker = [UIImagePickerController new];
    NSMutableDictionary *tempData = [NSMutableDictionary dictionary];
    [tempData setObj:block forKey:@"block"];
    picker.data = tempData;
    
    picker.delegate = self;
    picker.sourceType = isCamera ? UIImagePickerControllerSourceTypeCamera:UIImagePickerControllerSourceTypePhotoLibrary;
    picker.allowsEditing = editType;
    [self.rootVC presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image = nil;
    if (picker.allowsEditing) {
        image = [info objectForKey:UIImagePickerControllerEditedImage];
    }else{
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }    
    CGSize size = image.size;
    CGFloat width = self.defaultWidth;
    CGFloat height = (self.defaultWidth/size.width)*size.height;
    
    image = [image scaleToSize:CGSizeMake(width, height)];
    
    NSDictionary *tempData = picker.data;
    SImageUploadMangerBlock block = [tempData objectForKey:@"block"];
    picker.data = nil;
    if (block) {
        block(image);
    }    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
