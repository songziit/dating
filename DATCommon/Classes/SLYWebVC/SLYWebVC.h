//
//  SLYWebVC.h
//  SuperRunErrandsUser
//
//  Created by neil on 2018/6/17.
//  Copyright © 2018年 Sly. All rights reserved.
//

typedef void(^SLYWebVCEventHandel)(NSString *eventName,NSDictionary *parameter);
@interface SLYWebVC : UIViewController

+ (instancetype)createInstanceWithURL:(NSString*)url;
@property(nonatomic, strong) NSString *navigationTitle;
@property (nonatomic,strong) SLYWebVCEventHandel eventHandel;

@end
