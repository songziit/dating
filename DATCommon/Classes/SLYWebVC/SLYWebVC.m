//
//  SLYWebVC.m
//  SuperRunErrandsUser
//
//  Created by neil on 2018/6/17.
//  Copyright © 2018年 Sly. All rights reserved.
//

#import "SLYWebVC.h"
#import "WeakWebViewScriptMessageDelegate.h"
#import <WebKit/WebKit.h>

@interface SLYWebVC ()<WKNavigationDelegate,WKUIDelegate,WKScriptMessageHandler>

@property (strong, nonatomic) WKWebView *webView;
@property (nonatomic, strong) IBOutlet UIProgressView *progressView;
@property(nonatomic, strong) NSString *urlString;

@end

@implementation SLYWebVC

+ (instancetype)createInstanceWithURL:(NSString*)url {    
    SLYWebVC *vc = [self loadFromStoryBoard:@"SLYWebVC"];
    vc.urlString = url;
    return vc;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self customizeView];
    [self customize];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.progressView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self.webView removeObserver:self forKeyPath:@"estimatedProgress"];
    [self.webView removeObserver:self forKeyPath:@"title"];
}

- (void)customizeView{
    WKWebViewConfiguration *config = [self createConfig];
    self.webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:config];
    self.webView.navigationDelegate = self;
    self.webView.UIDelegate = self;
    [self.view addSubview:self.webView withEdge:UIEdgeInsetsMake(self.progressView.y, 0, 0, 0)];
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
}

- (WKWebViewConfiguration*)createConfig{
    //创建网页配置对象
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    
    // 创建设置对象
    WKPreferences *preference = [[WKPreferences alloc]init];
    //最小字体大小 当将javaScriptEnabled属性设置为NO时，可以看到明显的效果
    preference.minimumFontSize = 0;
    //设置是否支持javaScript 默认是支持的
    preference.javaScriptEnabled = YES;
    // 在iOS上默认为NO，表示是否允许不经过用户交互由javaScript自动打开窗口
    preference.javaScriptCanOpenWindowsAutomatically = YES;
    config.preferences = preference;
    
    // 是使用h5的视频播放器在线播放, 还是使用原生播放器全屏播放
    config.allowsInlineMediaPlayback = YES;
    //设置视频是否需要用户手动播放  设置为NO则会允许自动播放
    if (@available(iOS 9.0, *)) {
        config.requiresUserActionForMediaPlayback = YES;
    } else {
        // Fallback on earlier versions
    }
    //设置是否允许画中画技术 在特定设备上有效
    if (@available(iOS 9.0, *)) {
        config.allowsPictureInPictureMediaPlayback = YES;
    } else {
        // Fallback on earlier versions
    }
    //设置请求的User-Agent信息中应用程序名称 iOS9后可用
    if (@available(iOS 9.0, *)) {
        config.applicationNameForUserAgent = @"ChinaDailyForiPad";
    } else {
        // Fallback on earlier versions
    }
    //自定义的WKScriptMessageHandler 是为了解决内存不释放的问题
    WeakWebViewScriptMessageDelegate *weakScriptMessageDelegate = [[WeakWebViewScriptMessageDelegate alloc] initWithDelegate:self];
    //这个类主要用来做native与JavaScript的交互管理
    WKUserContentController * wkUController = [[WKUserContentController alloc] init];
    //注册一个name为jsToOcNoPrams的js方法
    [wkUController addScriptMessageHandler:weakScriptMessageDelegate  name:@"CallbackEvent"];
    config.userContentController = wkUController;
    return config;
}

- (void)customize{
    self.title = self.navigationTitle;
    if (self.urlString) {
        NSURL *url = [NSURL URLWithString:self.urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [self.webView loadRequest:request];
    }
}

#pragma mark - KVO监听
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if (object==self.webView && [@"estimatedProgress" isEqualToString:keyPath]) {
        self.progressView.progress = self.webView.estimatedProgress;
        self.progressView.hidden = self.progressView.progress == 1;
    }
    else if (object==self.webView && [@"title" isEqualToString:keyPath]){
        if ([NSString isEmpty:self.navigationTitle]) {
            self.navigationItem.title = self.webView.title;
        }
    }
    else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

#pragma mark - WKNavigationDelegate
//开始加载
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    self.progressView.hidden = NO;
    [self.view bringSubviewToFront:self.progressView];
}

//加载完成
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    self.progressView.hidden = YES;
}

//加载失败
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    self.progressView.hidden = YES;
}

#pragma mark - WKUIDelegate

/**
 *  web界面中有弹出警告框时调用
 *
 *  @param webView           实现该代理的webview
 *  @param message           警告框中的内容
 *  @param completionHandler 警告框消失调用
 */
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    [MessageBox showMessage:message?:@""];
    completionHandler();
}

// 确认框
//JavaScript调用confirm方法后回调的方法 confirm是js中的确定框，需要在block中把用户选择的情况传递进去
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    [[ConfirmMessageBox confirmMessage:message?:@""] subscribeNext:^(id x) {
        completionHandler([x boolValue]);
    }];
}
// 输入框
//JavaScript调用prompt方法后回调的方法 prompt是js中的输入框 需要在block中把用户输入的信息传入
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = defaultText;
    }];
    [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(alertController.textFields[0].text?:@"");
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}

// 页面是弹出窗口 _blank 处理
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

#pragma mark WKScriptMessageHandler

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    NSString *callbackName = message.name;
    if (![@"CallbackEvent" isEqualToString:callbackName]) {
        return;
    }
    NSDictionary *parameter = message.body;
    if (![parameter isKindOfClass:NSDictionary.class]) {
        return;
    }
    NSString *eventName = [parameter stringForKey:@"eventName"];
    if (self.eventHandel) {
        self.eventHandel(eventName, parameter);
    }
}

@end
