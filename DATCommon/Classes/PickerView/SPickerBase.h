//
//  SPickerBase.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/7.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^SPickerBlock)(id aValue);

@interface SPickerBase : UIView

@property(nonatomic, readonly) BOOL showing;

#pragma mark - event
@property(nonatomic, copy) SPickerBlock okBlock;
- (void)setOkBlock:(SPickerBlock)okBlock;

@property(nonatomic, copy) dispatch_block_t cancelBlock;

//protect方法

- (void)click_btnCancel:(id)sender;
- (void)click_btnOK:(id)sender;

#pragma mark - 展示消失处理

- (void)show;
- (void)hidden;

@end
