//
//  SexDataPicker.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/29.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "SexDataPicker.h"

@implementation SexDataPicker

+ (instancetype)sharedInstance {
    static SexDataPicker *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self createInstance];
        instance.data = @(SexTypeMale);
    });
    return instance;
}

- (void)show {
    [super show];
    [self.pkvData selectRow:[self.data integerValue] inComponent:0 animated:NO];
}

#pragma mark - UIPickerViewDataSource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSInteger numberOfRows = 3;
    return numberOfRows;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [EnumHelper sexType:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.data = @(row);
    if (self.didSelectedBlock) {
        self.didSelectedBlock(self.data);
    }
}

@end
