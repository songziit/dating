//
//  SDatePicker.m
//  UserCar
//
//  Created by 舒联勇 on 14/11/15.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import "SDatePicker.h"

@interface SDatePicker ()

@property(nonatomic, weak) IBOutlet UIDatePicker *datePicker;

@end

@implementation SDatePicker

+ (instancetype)createInstance {
    return [SDatePicker loadFromNib];
}

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self createInstance];
    });
    return instance;
}

- (void)awakeFromNib {
    if (!self.data) {
        NSDate *now = [[NSDate alloc] init];
        [self.datePicker setMaximumDate:now];
        self.data = now;
    }
    [super awakeFromNib];
}

#pragma mark - event

- (IBAction)click_ChangeTime:(id)sender {
    self.data = self.datePicker.date;
}

@end
