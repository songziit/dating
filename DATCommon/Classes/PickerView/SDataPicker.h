//
//  SDataPicker.h
//  UserCar
//
//  Created by 舒联勇 on 14/10/7.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPickerBase.h"

@class SDataPicker;

@protocol SDataPickerDataSource

@optional
- (id)dataPicker:(SDataPicker*)aDataPicker dataForRow:(NSInteger)row forComponent:(NSInteger)component;
- (NSIndexPath*)dataPicker:(SDataPicker*)aDataPicker indexPathForData:(id)data;

@end

@interface SDataPicker : SPickerBase<UIPickerViewDataSource, UIPickerViewDelegate>

+ (instancetype)sharedInstance;

@property(weak, nonatomic, readonly) UIPickerView *pkvData;
@property(weak, nonatomic, readonly) UILabel *lblSubTitle;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,weak) id<UIPickerViewDataSource, UIPickerViewDelegate,SDataPickerDataSource> pickerDelegate;

- (void)reloadData;

@end
