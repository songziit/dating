//
//  SexDataPicker.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/29.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "SDataPicker.h"

@interface SexDataPicker : SDataPicker

@property(nonatomic, copy) SPickerBlock didSelectedBlock;
- (void)setDidSelectedBlock:(SPickerBlock)didSelectedBlock;

+ (instancetype)sharedInstance;

@end
