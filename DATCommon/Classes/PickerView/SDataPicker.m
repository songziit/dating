//
//  SDataPicker.m
//  UserCar
//
//  Created by 舒联勇 on 14/10/7.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import "SDataPicker.h"

@interface SDataPicker ()

@property(weak, nonatomic) IBOutlet UIPickerView *pkvData;
@property(strong, nonatomic) IBOutlet UIView *view;
@property(weak, nonatomic,) IBOutlet UILabel *lblSubTitle;
@property(weak, nonatomic,) IBOutlet UILabel *lblTitle;

@end

@implementation SDataPicker

#pragma mark - 生命周期

+ (instancetype)createInstance {
    SDataPicker *picker = [[[self class] alloc] init];
    [[NSBundle mainBundle] loadNibNamed:@"SDataPicker" owner:picker options:nil];
    picker.frame = picker.view.bounds;
    [picker addSubview:picker.view withEdge:UIEdgeInsetsZero];
    return picker;
}

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self createInstance];
    });
    return instance;
}

#pragma mark - event

- (void)reloadData {
    [self.pkvData reloadAllComponents];
}

- (void)setTitle:(NSString *)title{
    self.lblTitle.text = title;
}

- (NSString *)title{
    return self.lblTitle.text;
}

- (void)show{
    [super show];
    [self reloadData];
    self.lblTitle.text = nil;
    self.lblSubTitle.text = nil;
    
    NSIndexPath *indexPath = nil;
    if(self.pickerDelegate && [self.pickerDelegate respondsToSelector:@selector(dataPicker:indexPathForData:)]){
        indexPath = [self.pickerDelegate dataPicker:self indexPathForData:self.data];
    }
    if(indexPath)
        [self.pkvData selectRow:indexPath.row inComponent:indexPath.section animated:YES];
}

- (void)hidden{
    [super hidden];
    self.pickerDelegate = nil;
    self.lblTitle.text = nil;
    self.lblSubTitle.text = nil;
}

#pragma mark - UIPickerViewDataSource

// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    NSInteger result = 0;
    if (self.pickerDelegate && [self.pickerDelegate respondsToSelector:@selector(numberOfComponentsInPickerView:)]) {
        result = [self.pickerDelegate numberOfComponentsInPickerView:pickerView];
    }
    return result;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSInteger result = 0;
    if (self.pickerDelegate && [self.pickerDelegate respondsToSelector:@selector(pickerView:numberOfRowsInComponent:)]) {
        result = [self.pickerDelegate pickerView:pickerView numberOfRowsInComponent:component];
    }
    return result;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *result = @"";
    if (self.pickerDelegate && [self.pickerDelegate respondsToSelector:@selector(pickerView:titleForRow:forComponent:)]) {
        result = [self.pickerDelegate pickerView:pickerView titleForRow:row forComponent:component];
    }
    return result;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if(self.pickerDelegate && [self.pickerDelegate respondsToSelector:@selector(dataPicker:dataForRow:forComponent:)]){
        id data = [self.pickerDelegate dataPicker:self dataForRow:row forComponent:component];
        self.data = data;
    }
    
    if (self.pickerDelegate && [self.pickerDelegate respondsToSelector:@selector(pickerView:didSelectRow:inComponent:)]) {
        [self.pickerDelegate pickerView:pickerView didSelectRow:row inComponent:component];
    }
}


@end
