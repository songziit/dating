//
//  SDatePicker.h
//  UserCar
//
//  Created by 舒联勇 on 14/11/15.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SPickerBase.h"

@interface SDatePicker : SPickerBase

+ (instancetype)sharedInstance;

@end
