//
//  SPickerBase.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/7.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "SPickerBase.h"

@interface SPickerBase () {
    id _originalData;
}

@property(nonatomic, strong) UIView *maskView;

@end

@implementation SPickerBase

#pragma mark - 生命周期

- (void)awakeFromNib {
    [super awakeFromNib];
    self.size = CGSizeMake(SCREEN_BOUND_SIZE.width, 260);
}

#pragma mark - event

- (IBAction)click_btnCancel:(id)sender {
    self.data = _originalData;
    [self hidden];
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}

- (IBAction)click_btnOK:(id)sender {
    [self hidden];
    if (self.okBlock) {
        self.okBlock(self.data);
    }
}

#pragma mark - mask view

- (UIView *)maskView {
    if (_maskView == nil) {
        _maskView = [[UIView alloc] initWithFrame:CGRectZero];
        _maskView.backgroundColor = [UIColor colorWithHexString:@"000000" withAlpha:0.5];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
        [_maskView addGestureRecognizer:tap];
    }
    return _maskView;
}

- (void)tap:(id)sender {
    [self hidden];
}

#pragma mark - pro

- (BOOL)showing {
    return (self.superview != nil);
}

- (void)show {
    if ([self showing]) {
        return;
    }
    _originalData = self.data;

    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    self.width = SCREEN_BOUND_SIZE.width;
    self.top = SCREEN_BOUND_SIZE.height;

    [window addSubview:self.maskView withEdge:UIEdgeInsetsZero];
    [window addSubview:self];

    self.maskView.alpha = 0;
    [UIView animateWithDuration:0.25
        delay:0
        options:UIViewAnimationOptionCurveEaseIn
        animations:^{
            self.top = self.top - self.height;
            self.maskView.alpha = 1;
        }
        completion:^(BOOL finished) {
            if (finished) {
                window.rootViewController.view.userInteractionEnabled = NO;
            }
        }];
}

- (void)hidden {
    UIWindow *window = [[UIApplication sharedApplication].delegate window];
    [UIView animateWithDuration:0.25
        delay:0
        options:UIViewAnimationOptionCurveEaseOut
        animations:^{
            self.top = self.top + self.height;
            self.maskView.alpha = 0;
        }
        completion:^(BOOL finished) {
            if (finished) {
                [self removeFromSuperview];
                [self.maskView removeFromSuperview];
                window.rootViewController.view.userInteractionEnabled = YES;
            }
        }];
}
@end
