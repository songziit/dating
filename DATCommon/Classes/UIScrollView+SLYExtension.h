//
//  UIScrollView+SLYExtension.h
//  MJRefreshExample
//
//  Created by neil on 15/5/13.
//  Copyright (c) 2015年 itcast. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (SLYExtension)

/**
 *   @brief  刷新block
 */
@property(copy, nonatomic) void (^refreshingBlock)(void);
/**
 *   @brief  是否刷新
 */
@property(nonatomic, assign) BOOL supportRefresh;

/**
 *   @brief  分布block
 */
@property(copy, nonatomic) void (^pageingBlock)(void);
/**
 *   @brief  是否分页
 */
@property(nonatomic, assign) BOOL supportPage;

#pragma mark - 结束刷新与分页

/**
 *   @brief  结束刷新
 */
- (void)endRefreshing;

/**
 *   @brief  结束分页
 */
- (void)endPageing;

/**
 *   @brief  结束刷新或分页
 */
- (void)endRefreshOrPage;

-(void)hiddenFooter:(BOOL)hidden;

@end
