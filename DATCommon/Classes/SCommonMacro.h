//
//  SCommonMacro
//  SCommon
//
//  Created by neil on 2017/3/12.
//  Copyright © 2017年 Sly. All rights reserved.
//

#ifndef LoanMacro_h
#define LoanMacro_h

#define SCREEN_BOUND_SIZE [UIView screenSize]
#define SCREEN_BOUND [UIScreen mainScreen].bounds

#define UIColorFromRGB(rgbValue) [UIColor  colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0  green:((float)((rgbValue & 0xFF00) >> 8))/255.0  blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define UIColorFromRGBA(rgbValue) [UIColor  colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0  green:((float)((rgbValue & 0xFF00) >> 8))/255.0  blue:((float)(rgbValue & 0xFF))/255.0 alpha:0.7]

static NSString *const kChannelTest = @"Test";
static NSString *const kChannelAppStore = @"AppStore";
static NSString *const kUMAppChannelKey = @"kUMAppChannelKey";
static NSString *const DidSelectedSystemPhoneNotification = @"DidSelectedSystemPhoneNotification";
static NSString *ImageHttpHost = @"";

typedef void(^SLYResultBlock)(id);
typedef void(^SLYResponseBlock)(id,NSError*);

#endif /* LoanMacro_h */
