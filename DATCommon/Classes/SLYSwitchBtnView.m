//
//  SLYSwitchBtnView.m
//  SuperRunErrandsUser
//
//  Created by neil on 2018/7/21.
//  Copyright © 2018年 Sly. All rights reserved.
//

#import "SLYSwitchBtnView.h"

@interface SLYSwitchBtnView()
    
@property (nonatomic,strong) UIButton *btnSelected;
    
@end

@implementation SLYSwitchBtnView

- (void)awakeFromNib{
    [super awakeFromNib];
    [self customize];
}

- (UIButton*)btnSelected{
    if (_btnSelected==nil) {
        NSArray *allButton = self.subviews;
        if(allButton!=nil && allButton.count>0){
            for (UIButton *btnTemp in allButton) {
                if(![btnTemp isKindOfClass:[UIButton class]])
                    continue;
                if(btnTemp.selected){
                    _btnSelected = btnTemp;
                    continue;
                }                
            }
        }
    }
    return _btnSelected;
}

- (void)customize{
    NSArray *allButton = self.subviews;
    if(allButton==nil || allButton.count<=0)
        return;
    
    for (UIButton *btnTemp in allButton) {
        if(![btnTemp isKindOfClass:[UIButton class]])
            continue;
        [btnTemp addTarget:self action:@selector(click_btnSwith:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}
    
- (void)click_btnSwith:(UIButton*)btnSwith
{
    NSArray *allButton = self.subviews;
    for (UIButton *btnTemp in allButton) {
        if (![btnTemp isKindOfClass:[UIButton class]]) {
            continue;
        }        
        btnTemp.selected = NO;
    }
    btnSwith.selected = YES;
    self.btnSelected = btnSwith;
}

- (void)setSelectedTag:(NSInteger)tag{
    UIButton *btnSwith = nil;
    NSArray *allButton = self.subviews;
    for (UIButton *btnTemp in allButton) {
        if (btnTemp.tag == tag) {
            btnSwith = btnTemp;
        }
        btnTemp.selected = NO;
    }
    if (btnSwith) {        
        btnSwith.selected = YES;
        self.btnSelected = btnSwith;
    }
}
    
@end
