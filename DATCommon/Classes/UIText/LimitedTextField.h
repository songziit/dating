//
//  LimitedTextField.h
//  UserCar
//
//  Created by 舒联勇 on 14/11/12.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, LimitedTextType) {
    LimitedTextTypeDefault=0,
    LimitedTextTypePhoneFormat=1,
    LimitedTextTypeUppercaseFormat=2,
    LimitedTextTypeInt=3,
    LimitedTextTypeDecimal=4,
};

@interface LimitedTextField : UITextField

@property(nonatomic, assign) IBInspectable NSInteger maxLength;


/**
 约束类型
 0:默认 1:电话样式 2:大写样式 3:整形样式 4:金额样式
 */
@property(nonatomic, assign) IBInspectable int limitedType;
/**
 *  电话号码，真实电话号码，没有其他格式
 */
@property(nonatomic, readonly) NSString *phoneNumber;

@end
