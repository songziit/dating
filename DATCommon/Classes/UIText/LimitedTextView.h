//
//  LimitedTextView.h
//  SuperRunErrands
//
//  Created by neil on 2018/10/22.
//  Copyright © 2018 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IQTextView.h"

NS_ASSUME_NONNULL_BEGIN

@interface LimitedTextView : IQTextView

@property(nonatomic, assign) IBInspectable NSInteger maxLength;

@end

NS_ASSUME_NONNULL_END
