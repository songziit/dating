//
//  LimitedTextView.m
//  SuperRunErrands
//
//  Created by neil on 2018/10/22.
//  Copyright © 2018 Sly. All rights reserved.
//

#import "LimitedTextView.h"

@implementation LimitedTextView

#pragma mark - 生命周期

- (void)awakeFromNib {
    [super awakeFromNib];
    @weakify(self);
    [self.rac_textSignal subscribeNext:^(id x) {
        @strongify(self);
        [self textField_ChangeText:self];
    }];
}

- (void)textField_ChangeText:(id)sender {
    if (self.maxLength > 0 && self.text.length >= self.maxLength) {
        self.text = [self.text substringToIndex:self.maxLength];
    }
}

- (void)dataDidChange{
    self.text = self.data;
    [self textField_ChangeText:self];
}

@end
