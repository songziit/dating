//
//  LimitedTextField.m
//  UserCar
//
//  Created by 舒联勇 on 14/11/12.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import "LimitedTextField.h"

@interface LimitedTextFieldDelegate : NSObject<UITextFieldDelegate>

@end

@interface LimitedTextField ()

- (LimitedTextType)limitedTextType;

@end

@implementation LimitedTextFieldDelegate

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

- (BOOL)textField:(LimitedTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    BOOL ret = YES;
    // 判断是否为删除字符，如果为删除则让执行
    char c = [string UTF8String][0];
    if (c == '\000') {
        if (textField.limitedTextType == LimitedTextTypePhoneFormat) {
            if ([textField.text hasSuffix:@"-"]) {
                textField.text = [textField.text substringToIndex:textField.text.length - 2];
                ret = NO;
            }
        }
    }
    return ret;
}

@end

@implementation LimitedTextField

#pragma mark - 生命周期

- (void)awakeFromNib {
    [super awakeFromNib];
    if (self.limitedTextType == LimitedTextTypePhoneFormat) {
        [self setKeyboardType:UIKeyboardTypePhonePad];
    }
    else if (self.limitedTextType == LimitedTextTypeInt) {
        [self setKeyboardType:UIKeyboardTypeNumberPad];
    }
    else if (self.limitedTextType == LimitedTextTypeDecimal) {
        [self setKeyboardType:UIKeyboardTypeDecimalPad];
    }
    
    @weakify(self);
    [self.rac_textSignal subscribeNext:^(id x) {
        @strongify(self);
        [self textField_ChangeText:self];
    }];
    self.delegate = [LimitedTextFieldDelegate sharedInstance];
    if (self.background) {
        [self setBackground:self.background];
    }
}

#pragma mark - 属性处理

- (void)setPhoneFormatEnable:(BOOL)phoneFormatEnable{
    if (phoneFormatEnable) {
        _limitedType = LimitedTextTypePhoneFormat;
    }    
}

- (void)setUppercaseFormatEnable:(BOOL)uppercaseFormatEnable{
    if (uppercaseFormatEnable) {
        _limitedType = LimitedTextTypeUppercaseFormat;
    }
}

- (NSString *)phoneNumber {
    return [NSString phoneNumFormat:self.text];
}

- (LimitedTextType)limitedTextType{
    return self.limitedType;
}

#pragma mark - 输入监听

- (void)textField_ChangeText:(id)sender {
    if (self.limitedTextType == LimitedTextTypePhoneFormat) {
        self.maxLength = 13;
        if (self.text.length != 8 && self.text.length != 3) {
            self.text = [self iPhoneNumberFormat:self.phoneNumber];
        }
    }
    else if (self.limitedTextType == LimitedTextTypeUppercaseFormat) {
        self.text = self.text.uppercaseString;
    }
    else if (self.limitedTextType == LimitedTextTypeInt) {
        if ([@"0" isEqualToString:self.text]) {
            self.text = @"";
        }
        self.text = [NSString NumberString:self.text];
    }
    else if (self.limitedTextType == LimitedTextTypeDecimal) {
        if ([@"0" isEqualToString:self.text]) {
            self.text = @"0.";
        }else if ([self.text hasSuffix:@"."]){
            self.text = [self.text substringToIndex:self.text.length - 1];
        }
        self.text = [NSString NumberString:self.text andSpecialChar:'.'];
    }
    if (self.maxLength > 0 && self.text.length >= self.maxLength) {
        self.text = [self.text substringToIndex:self.maxLength];
    }
}

#pragma mark - 电话号码处理

- (NSString *)iPhoneNumberFormat:(NSString *)aValue {
    if ([NSString isEmpty:aValue]) {
        return @"";
    }
    NSString *tenDigitNumber = aValue;

    NSString *iPhoneFormat = @"(\\d{3})(\\d{4})(\\d{0,4})";
    NSString *lessiPhoneFormat = @"(\\d{3})(\\d{0,4})";

    NSString *iPhoneReplaceFormat = @"$1-$2-$3";
    NSString *lessiPhoneReplaceFormat = @"$1-$2";

    NSString *format = nil;
    NSString *replaceFormat = nil;
    if (aValue.length < 7) {
        format = lessiPhoneFormat;
        replaceFormat = lessiPhoneReplaceFormat;
    } else {
        format = iPhoneFormat;
        replaceFormat = iPhoneReplaceFormat;
    }
    tenDigitNumber = [tenDigitNumber stringByReplacingOccurrencesOfString:format
                                                               withString:replaceFormat
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [tenDigitNumber length])];
    return tenDigitNumber;
}

- (void)setBackground:(UIImage *)image{
    CGSize imageSize = CGSizeZero;
    if (CGSizeEqualToSize(imageSize, CGSizeZero))
    {
        imageSize = CGSizeMake(image.size.width/2, image.size.height/2);
    }
    UIImage *temp = [image stretchableImageWithLeftCapWidth:imageSize.width topCapHeight:imageSize.height];
    [super setBackground:temp];
}

@end
