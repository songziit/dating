//
//  SLYSwitchBtnView.h
//  SuperRunErrandsUser
//
//  Created by neil on 2018/7/21.
//  Copyright © 2018年 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SLYSwitchBtnView : UIView
   
@property (nonatomic,readonly) UIButton *btnSelected;
- (void)setSelectedTag:(NSInteger)tag;
    
@end
