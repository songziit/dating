//
//  SLYEnum.m
//  SuperRunErrandsUser
//
//  Created by neil on 2018/5/19.
//  Copyright © 2018年 Sly. All rights reserved.
//

#import "EnumHelper.h"

@implementation EnumHelper

+ (NSString *)sexType:(SexType)type {
    switch (type) {
        case SexTypeMale:
            return LOCALIZATION(@"Male");
        case SexTypeFemale:
            return LOCALIZATION(@"Female");
        default:
            return LOCALIZATION(@"Secret");
    }
}

@end
