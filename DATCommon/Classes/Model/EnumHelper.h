//
//  SLYEnum.h
//  SuperRunErrandsUser
//
//  Created by neil on 2018/5/19.
//  Copyright © 2018年 Sly. All rights reserved.
//

/**
 *   @brief  性别类型
 */
typedef NS_ENUM(NSUInteger, SexType) {
    /**
     *   @brief  未知
     */
    SexTypeDefault = 0,
    /**
     *   @brief  女
     */
    SexTypeFemale = 1,
    /**
     *   @brief  男
     */
    SexTypeMale=2,
};

#import <Foundation/Foundation.h>

@interface EnumHelper : NSObject

+ (NSString *)sexType:(SexType)type;

@end
