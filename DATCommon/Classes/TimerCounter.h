//
//  TimerCounter.h
//  Loan
//
//  Created by neil on 2017/3/12.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimerCounter : NSObject

+ (instancetype)sharedInstance;

@property(nonatomic, readonly) NSInteger codeCount;
@property(nonatomic, readonly) BOOL countEnd;
- (void)beginCount;
- (void)endCount;

@end
