//
//  RegularExpressionResource.m
//  SuperRunErrands
//
//  Created by neil on 2018/12/13.
//  Copyright © 2018 Sly. All rights reserved.
//

#import "RegularExpressionResource.h"

@implementation RegularExpressionResource

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

+ (NSDictionary *)resource{
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [RegularExpressionResource customize];
    });
    return instance;
}

+ (NSDictionary*)customize{
    NSMutableDictionary *ret = [NSMutableDictionary dictionary];
    ret[@"PhoneReg"] = @"(\\d{3})-(\\d{4})-(\\d{4})";
    ret[@"PasswordReg"] = @"\\w{6,16}";
    ret[@"CodeReg"] = @"\\w{6}";
    ret[@"PasswordValidReg"] = @"(?![0-9]+$)(?![a-zA-Z]+$)[\\w!@#$%^&*?\\(\\)]{6,16}";
    ret[@"EmailReg"] = @"[a-zA-Z0-9_\\.-]+@[a-zA-Z0-9_-]+\\.[a-zA-Z0-9_-]+";
    return ret;
}


@end
