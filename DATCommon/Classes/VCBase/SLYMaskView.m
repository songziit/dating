//
//  SLYMaskView.m
//  SuperRunErrands
//
//  Created by neil on 2019/1/11.
//  Copyright © 2019 Sly. All rights reserved.
//

#import "SLYMaskView.h"

@implementation SLYMaskView

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor colorWithHexString:@"000000" withAlpha:0.65];
        self.frame = SCREEN_BOUND;
    }
    return self;
}

@end
