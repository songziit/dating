//
//  UINavigationController+RootController.h
//  Social
//
//  Created by neil on 2019/8/28.
//  Copyright © 2019 songzi. All rights reserved.
//



NS_ASSUME_NONNULL_BEGIN

@interface UINavigationController (RootController)

@property (strong,nonatomic) IBInspectable NSString *rootClass;

@end

NS_ASSUME_NONNULL_END
