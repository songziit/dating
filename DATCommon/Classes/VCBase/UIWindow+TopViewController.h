//
//  UIWindow+TopViewController.h
//  SuperRunErrands
//
//  Created by neil on 2019/1/17.
//  Copyright © 2019 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIWindow (TopViewController)

- (UIViewController *)topViewController;

@end

NS_ASSUME_NONNULL_END
