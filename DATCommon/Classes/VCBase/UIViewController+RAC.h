//
//  UIViewController+SLYViewModel.h
//  Loan
//
//  Created by neil on 2017/3/1.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewModelBase;
@interface UIViewController (RAC)

@property(nonatomic, readonly) RACSignal *rac_viewWillDisappear;
@property(nonatomic, readonly) RACSignal *rac_viewWillAppear;
- (void)presentError:(NSError *)error;

@end
