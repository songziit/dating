//
//  UINavigationController+RootController.m
//  Social
//
//  Created by neil on 2019/8/28.
//  Copyright © 2019 songzi. All rights reserved.
//

#import "UINavigationController+RootController.h"

@implementation UINavigationController (RootController)

- (void)setRootClass:(NSString *)rootClass{
    Class aClass = NSClassFromString(rootClass);
    UIViewController *nextVC = [aClass new];
    [self setViewControllers:@[nextVC]];
}

- (NSString *)rootClass{
    return nil;
}

@end
