//
//  UIViewController+SLYViewModel.m
//  Loan
//
//  Created by neil on 2017/3/1.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import "UIViewController+RAC.h"
#import <objc/runtime.h>
#import "MessageBox.h"

@implementation UIViewController (RAC)
static const char kUIViewWillDisappearKey;
static const char kUIViewWillAppearKey;

- (RACSignal*)rac_viewWillDisappear
{
    id result = objc_getAssociatedObject(self, &kUIViewWillDisappearKey);
    if(result==nil)
    {
        result = [RACSubject subject];
        objc_setAssociatedObject(self, &kUIViewWillDisappearKey, result, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return result;
}

- (RACSignal*)rac_viewWillAppear
{
    id result = objc_getAssociatedObject(self, &kUIViewWillAppearKey);
    if(result==nil)
    {
        result = [RACSubject subject];
        objc_setAssociatedObject(self, &kUIViewWillAppearKey, result, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return result;
}


- (void)presentError:(NSError *)error {
    if(!self.isViewLoaded || self.view.window==nil){
        return;
    }
    ENTER(@"presentError:%@", error);
    NSParameterAssert([error isKindOfClass:[NSError class]]);
    NSError *newError = [self filterError:error];
    [MessageBox showTitle:newError.localizedDescription
              withMessage:newError.localizedFailureReason
     withCancelButonTitle:NSLocalizedString(@"OK", nil)];
}

- (NSError *)filterError:(NSError *)error {
    if (error.code != NSURLErrorNotConnectedToInternet) {
        return error;
    } else {
        return [NSError errorWithTitle:LOCALIZATION(@"Alert")
                                reason:LOCALIZATION(@"Network error, please check the network connection.")];
    }
}

@end
