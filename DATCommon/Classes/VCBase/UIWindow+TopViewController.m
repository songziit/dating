//
//  UIWindow+TopViewController.m
//  SuperRunErrands
//
//  Created by neil on 2019/1/17.
//  Copyright © 2019 Sly. All rights reserved.
//

#import "UIWindow+TopViewController.h"

@implementation UIWindow (TopViewController)

- (UIViewController *)topViewController {
    UIViewController *resultVC = nil;
    resultVC = [self _topViewController:self.rootViewController];
    while (resultVC.presentedViewController) {
        resultVC = [self _topViewController:resultVC.presentedViewController];
    }
    return resultVC;
}

- (UIViewController *)_topViewController:(UIViewController *)vc {
    if ([vc isKindOfClass:[UINavigationController class]]) {
        return [self _topViewController:[(UINavigationController *)vc topViewController]];
    } else if ([vc isKindOfClass:[UITabBarController class]]) {
        return [self _topViewController:[(UITabBarController *)vc selectedViewController]];
    } else {
        return vc;
    }
    return nil;
}

@end
