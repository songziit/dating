//
//  UIVerifyManager.h
//  SuperRunErrands
//
//  Created by neil on 2018/12/13.
//  Copyright © 2018 Sly. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIVerifyManager : NSObject

@property (nonatomic,weak) IBOutlet UIViewController *parentVC;
@property (nonatomic,strong) IBOutletCollection(UITextField) NSMutableArray *verifyFrom;
@property (nonatomic,strong) IBOutlet UIView *verifyFromView;
@property (nonatomic,strong) IBOutletCollection(UIButton) NSMutableArray *verifyTo;

+ (NSError*)checkRequiredWithAllView:(NSArray*)allViews;

@end

NS_ASSUME_NONNULL_END
