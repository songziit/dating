//
//  UIVerifyManager.m
//  SuperRunErrands
//
//  Created by neil on 2018/12/13.
//  Copyright © 2018 Sly. All rights reserved.
//

#import "UIVerifyManager.h"
#import "SLYBindView.h"

@interface UIVerifyManager()

@property (nonatomic,strong) NSTimer *timer;

@end

@implementation UIVerifyManager

- (void)awakeFromNib{
    [super awakeFromNib];
    if (self.parentVC==nil) {
        return;
    }
    @weakify(self);
    [self.parentVC.rac_viewWillAppear subscribeNext:^(id x) {
        @strongify(self);
        [self customize];
    }];
    [self.parentVC.rac_viewWillDisappear subscribeNext:^(id x) {
        @strongify(self);
        if(self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    }];
}

- (void)customize{
    if (self.parentVC==nil) {
        return;
    }
    if ((self.verifyFrom==nil || self.verifyFrom.count<=0) && self.verifyFromView==nil) {
        return;
    }
    if (self.verifyTo == nil || self.verifyTo.count<=0) {
        return;
    }
    
    @weakify(self);
    for (UITextField *txtItem in self.verifyFrom) {
        [[[txtItem.rac_textSignal skip:1] takeUntil:self.parentVC.rac_viewWillDisappear] subscribeNext:^(id x) {
            @strongify(self);
            [self checkInput];
        }];
    }
    if (self.verifyFromView) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(checkInput) userInfo:nil repeats:YES];
    }
    [self checkInput];
}

- (void)checkInput{
    if (self.parentVC==nil) {
        return;
    }
    if ((self.verifyFrom==nil || self.verifyFrom.count<=0) && self.verifyFromView==nil) {
        return;
    }
    if (self.verifyTo == nil || self.verifyTo.count<=0) {
        return;
    }
    @weakify(self);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        @strongify(self);
        NSMutableArray *checkList = [NSMutableArray array];
        if (self.verifyFromView) {
            NSArray *tempList = [UIVerifyManager allSubviews:self.verifyFromView withResult:nil];
            [checkList addObjectsFromArray:tempList];
        }
        if (self.verifyFrom) {
            for (UIView *tempView in self.verifyFrom) {
                if (![checkList containsObject:tempView]) {
                    [checkList addObject:tempView];
                }
            }
        }
        NSError *error = [UIVerifyManager checkRequiredWithAllView:checkList];
        BOOL enable = error==nil;
        for (UIButton *btnItem in self.verifyTo) {
            btnItem.enabled = enable;
        }
    });
}

+ (NSMutableArray*)allSubviews:(UIView*)currentView withResult:(NSMutableArray*)result{
    if (result==nil) {
        result = [NSMutableArray array];
    }
    if ([currentView isKindOfClass:UITableViewCell.class]) {
        if (currentView.hidden) {
            return result;
        }        
    }
    
    [result addObj:currentView];
    if (currentView.subviews) {
        for (UIView *item in currentView.subviews) {
            [self allSubviews:item withResult:result];
        }
    }
    return result;
}

+ (NSError*)checkRequiredWithAllView:(NSArray*)allViews{
    NSError *result = nil;
    NSString *enterString = LOCALIZATION(@"Enter");
    NSString *pleaseEnterString = LOCALIZATION(@"Please enter");
    NSString *pleaseEnterFormatString = LOCALIZATION(@"Please enter %@");
    
    for (UIView *item in allViews) {
        if (!item.required) {
            continue;
        }
        NSString *value = nil;
        NSString *errorValue = nil;
        if ([item respondsToSelector:@selector(text)]) {
            value = [item valueForKey:@"text"];
        }
        if ([item respondsToSelector:@selector(placeholder)]) {
            errorValue = [item valueForKey:@"placeholder"];
        }
        
        if ([NSString isEmpty:value]) {
            if (![NSString isEmpty:errorValue]) {
                if (![errorValue containsString:enterString]) {
                    errorValue = [NSString stringWithFormat:pleaseEnterFormatString,errorValue];
                }
            }
            result = [NSError errorWithTitle:LOCALIZATION(@"Alert") reason:errorValue];
            break;
        }
        else if(![NSString isEmpty:item.verifyReg]){
            NSString *regValue = [[RegularExpressionResource resource] stringForKey:item.verifyReg];
            if ([NSString isEmpty:regValue]) {
                regValue = item.verifyReg;
            }
            if (![NSString regexWithFormat:regValue ValueString:value]) {
                errorValue = [errorValue stringByReplacingOccurrencesOfString:pleaseEnterString withString:@""];
                errorValue = [errorValue stringByReplacingOccurrencesOfString:enterString withString:@""];
                errorValue = [NSString stringWithFormat:LOCALIZATION(@"%@ format error"),errorValue];
                result = [NSError errorWithTitle:LOCALIZATION(@"Alert") reason:errorValue];
                break;
            }
        }
    }
    if (result == nil) {
        result = [self checkGroupNameWithAllView:allViews];
    }    
    return result;
}

+ (NSError*)checkGroupNameWithAllView:(NSArray*)allViews{
    NSError *ret = nil;
    NSString *enterString = LOCALIZATION(@"Enter");
    NSString *pleaseEnterString = LOCALIZATION(@"Please enter");
    NSString *pleaseEnterFormatString = LOCALIZATION(@"Please enter %@");
    NSMutableDictionary *groupList = [NSMutableDictionary dictionary];
    for (UIView *item in allViews) {
        if ([NSString isEmpty:item.groupName]) {
            continue;
        }
        NSMutableArray *values = [groupList objectForKey:item.groupName];
        if (values==nil) {
            values = [NSMutableArray array];
            [groupList setObj:values forKey:item.groupName];
        }
        if ([values containsObject:item]) {
            continue;
        }
        [values addObj:item];
    }
    
    if (groupList.count>0) {
        for (NSString *key in groupList.allKeys) {
            NSMutableArray *values = [groupList objectForKey:key];
            
            NSString *firstValue = nil;
            NSString *firstError = nil;
            for (UIView *item in values) {
                NSString *value = nil;
                NSString *errorValue = nil;
                if ([item respondsToSelector:@selector(text)]) {
                    value = [item valueForKey:@"text"];
                }
                if ([item respondsToSelector:@selector(placeholder)]) {
                    errorValue = [item valueForKey:@"placeholder"];
                }
                
                if (firstValue==nil) {
                    firstValue = value?:@"";
                    firstError = errorValue?:@"";
                }else{
                    if (![firstValue isEqualToString:value]) {
                        
                        errorValue = [errorValue stringByReplacingOccurrencesOfString:pleaseEnterString withString:@""];
                        errorValue = [errorValue stringByReplacingOccurrencesOfString:enterString withString:@""];
                        
                        firstError = [firstError stringByReplacingOccurrencesOfString:pleaseEnterString withString:@""];
                        firstError = [firstError stringByReplacingOccurrencesOfString:enterString withString:@""];
                        
                        NSString *errorString = [NSString stringWithFormat:LOCALIZATION(@"%@ is not equar to %@"),firstError,errorValue];
                        return [NSError errorWithTitle:LOCALIZATION(@"Alert") reason:errorString];
                        
                    }
                }
            }
        }
    }
    
    return ret;
}


@end
