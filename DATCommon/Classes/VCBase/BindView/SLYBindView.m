//
//  SLYBindView.m
//  SuperRunErrands
//
//  Created by neil on 2018/10/5.
//  Copyright © 2018 Sly. All rights reserved.
//

#import "SLYBindView.h"
#import "UIVerifyManager.h"
#import <objc/runtime.h>

static const char kUIBindDataKey;
static const char kUIBindRequiredKey;
static const char kUIVerifyRegKey;
static const char kUISegueNameKey;
static const char kUIGroupNameKey;
static const char kUIViewNameKey;

@implementation UIView (SLYBindView)
@dynamic bindDataKey,verifyReg;

- (void)setBindDataKey:(id)data
{
    objc_setAssociatedObject(self, &kUIBindDataKey, data, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (id)bindDataKey
{
    return objc_getAssociatedObject(self, &kUIBindDataKey);
}

- (NSString *)verifyReg{
    return objc_getAssociatedObject(self, &kUIVerifyRegKey);
}

- (void)setVerifyReg:(NSString *)verifyReg{
    objc_setAssociatedObject(self, &kUIVerifyRegKey, verifyReg, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setRequired:(BOOL)required{
    objc_setAssociatedObject(self, &kUIBindRequiredKey, @(required), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (BOOL)required{
    NSNumber *temp = objc_getAssociatedObject(self, &kUIBindRequiredKey);
    return temp.boolValue;
}

- (NSString *)segueName{
    return objc_getAssociatedObject(self, &kUISegueNameKey);
}

- (void)setSegueName:(NSString *)segueName{
    objc_setAssociatedObject(self, &kUISegueNameKey, segueName, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)groupName{
    return objc_getAssociatedObject(self, &kUIGroupNameKey);
}

- (void)setGroupName:(NSString *)componentName{
    objc_setAssociatedObject(self, &kUIGroupNameKey, componentName, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)viewName{
    return objc_getAssociatedObject(self, &kUIViewNameKey);
}

- (void)setViewName:(NSString *)viewName{
    objc_setAssociatedObject(self, &kUIViewNameKey, viewName, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)bindData:(id)data{
    if (![NSString isEmpty:self.bindDataKey]) {
        NSStringEncoding encoding = NSUTF8StringEncoding;
        NSData *currentData = [self.bindDataKey dataUsingEncoding:encoding];
        NSDictionary *allKeys = [NSJSONSerialization JSONObjectWithData:currentData
                                                                options:kNilOptions
                                                                  error:nil];
        for (NSString *viewKey in allKeys.allKeys) {
            NSString *dataKey = allKeys[viewKey];
            id dataValue = [data valueForKeyPath:dataKey];
            if (dataValue) {
                if ([@"required" isEqualToString:viewKey]) {
                }
                else if ([@"selected" isEqualToString:viewKey]){
                    
                }
                else if ([dataValue isKindOfClass:[NSNumber class]]) {
                    dataValue = [NSString stringWithFormat:@"%@",dataValue];
                }                
                [self setValue:dataValue forKeyPath:viewKey];
            }
        }
    }
    if (self.subviews) {
        for (UIView *viewItem in self.subviews) {
            [viewItem bindData:data];
        }
    }
}

- (id)objectItem:(id)lastObject forKey:(NSString*)key{
    return [self objectItem:lastObject forKey:key lastIndex:1];
}

- (id)objectItem:(id)lastObject forKey:(NSString*)key lastIndex:(NSInteger)index{
    if (lastObject==nil || [NSString isEmpty:key]) {
        return nil;
    }
    NSArray *temp = [key componentsSeparatedByString:@"."];
    NSMutableArray *allKeys = [NSMutableArray arrayWithArray:temp];
    if (allKeys.count>index) {
        NSString *firstKey = allKeys.firstObject;
        id data = [lastObject valueForKey:firstKey];
        
        [allKeys removeObjectAtIndex:0];
        key = [allKeys componentsJoinedByString:@"."];
        return [self objectItem:data forKey:key lastIndex:index];
    }else{
        return lastObject;
    }
}

- (BOOL)existBindDataKey{
    NSMutableArray *allSubviews = [self allSubviews:self withResult:nil];
    BOOL result = NO;
    for (UIView *item in allSubviews) {
        if (![NSString isEmpty:item.bindDataKey]) {
            result = YES;
            break;
        }
    }
    return result;
}


- (NSArray*)allSubviews{
    return [self allSubviews:self withResult:nil];
}

- (NSMutableArray*)allSubviews:(UIView*)currentView withResult:(NSMutableArray*)result{
    if (result==nil) {
        result = [NSMutableArray array];
    }
    [result addObj:currentView];
    if (currentView.subviews) {
        for (UIView *item in currentView.subviews) {
            [self allSubviews:item withResult:result];
        }
    }
    return result;
}

- (NSError*)checkRequired{
    NSError *result = nil;
    NSArray *allViews = [self allSubviews:self withResult:nil];
    result = [UIVerifyManager checkRequiredWithAllView:allViews];
    return result;
}

@end
