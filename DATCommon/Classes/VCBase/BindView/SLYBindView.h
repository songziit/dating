//
//  SLYBindView.h
//  SuperRunErrands
//
//  Created by neil on 2018/10/5.
//  Copyright © 2018 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SLYBindView)

@property (nonatomic,strong) IBInspectable NSString *bindDataKey;
@property (nonatomic,assign) IBInspectable BOOL required;
@property (nonatomic,strong) IBInspectable NSString *verifyReg;
@property (nonatomic,strong) IBInspectable NSString *segueName;
@property (nonatomic,strong) IBInspectable NSString *groupName;
@property (nonatomic,strong) IBInspectable NSString *viewName;

- (void)bindData:(id)data;
- (BOOL)existBindDataKey;
- (NSArray*)allSubviews;
- (NSError*)checkRequired;

@end
