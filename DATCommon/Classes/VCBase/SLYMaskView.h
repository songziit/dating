//
//  SLYMaskView.h
//  SuperRunErrands
//
//  Created by neil on 2019/1/11.
//  Copyright © 2019 Sly. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SLYMaskView : UIView

+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
