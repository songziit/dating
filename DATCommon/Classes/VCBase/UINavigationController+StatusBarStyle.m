//
//  UINavigationController+StatusBarStyle.m
//  SuperRunErrandsUser
//
//  Created by neil on 2018/12/12.
//  Copyright © 2018 Sly. All rights reserved.
//

#import "UINavigationController+StatusBarStyle.h"

@implementation UINavigationController (StatusBarStyle)

- (UIViewController*)childViewControllerForStatusBarStyle{
    return self.visibleViewController;
}

- (UIViewController *)childViewControllerForStatusBarHidden{
    return self.visibleViewController;
}

@end
