//
//  ViewModelBase.m
//  Loan
//
//  Created by neil on 2017/3/1.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import "ViewModelBase.h"

@implementation ViewModelBase

- (instancetype)init
{
    self = [super init];
    if (self) {
        _errorSignal = [RACSubject subject];
        _loadingSignal = [RACSubject subject];
    }
    return self;
}

- (void)dealloc
{
    [self.errorSignal sendCompleted];
    [self.loadingSignal sendCompleted];
    _errorSignal = nil;
    _loadingSignal = nil;
}

- (instancetype)initWithModel:(NSObject*)aModel
{
    self = [self init];
    if (self) {
        
    }
    return self;
}

- (void)customize{
    
}

@end
