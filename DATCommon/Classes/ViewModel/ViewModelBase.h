//
//  ViewModelBase.h
//  Loan
//
//  Created by neil on 2017/3/1.
//  Copyright © 2017年 Sly. All rights reserved.
//

@protocol IPageingLoad<NSObject>

@property(nonatomic, readonly) NSArray *items;
@property(nonatomic,readonly) BOOL supportPage;
- (void)loadData;
- (void)loadNext;

@optional
@property (nonatomic) NSInteger pageSize;


@end

@interface ViewModelBase : NSObject

- (instancetype)initWithModel:(NSObject*)aModel;
- (void)customize;

@property(nonatomic, strong) RACSubject *errorSignal;
@property(nonatomic, strong) RACSubject *loadingSignal;

@end
