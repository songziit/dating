//
//  MQActionSheet.h
//  MeiQiReferrer
//
//  Created by neil on 15/5/22.
//  Copyright (c) 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MQAlertActionStyle) {
    MQAlertActionStyleDefault = 0,
    MQAlertActionStyleCancel,
    MQAlertActionStyleDestructive
};

@protocol MQAlertAction;
@interface MQAlertAction : NSObject

+ (instancetype)actionWithTitle:(NSString *)title style:(MQAlertActionStyle)style handler:(void (^)(MQAlertAction *action))handler;

@property(nonatomic, readonly) NSString *title;
@property(nonatomic, readonly) MQAlertActionStyle style;
@property(nonatomic, getter=isEnabled) BOOL enabled;

@end

@interface MQActionSheet : NSObject

+ (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions;
+ (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions inVC:(UIViewController*)aVC inView:(UIView*)aView;
+ (void)dismiss;


@end
