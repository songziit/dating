//
//  ComfirmMessageBox.m
//  UserCar
//
//  Created by 舒联勇 on 14-4-30.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import "ConfirmMessageBox.h"
#import "MQAlertView.h"

@interface ConfirmMessageBox ()

@property(nonatomic, strong) RACSubject *confirmSignal;

@end

@implementation ConfirmMessageBox

- (RACSubject *)confirmSignal {
    if (_confirmSignal == nil) {
        _confirmSignal = [RACSubject subject];
    }
    return _confirmSignal;
}

- (void)confirmTitle:(NSString *)aTitle withMessage:(NSString *)aMessage {
    [self confirmTitle:aTitle withMessage:aMessage
        withCanelTitle:LOCALIZATION(@"Cancel")
        withOtherTitle:LOCALIZATION(@"OK")];
}

- (void)confirmTitle:(NSString *)aTitle
         withMessage:(NSString *)aMessage
      withCanelTitle:(NSString*)cancelTitle
         withOtherTitle:(NSString*)otherTitle {
    @weakify(self);
    UIAlertController *alertBox = [UIAlertController alertControllerWithTitle:aTitle message:aMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [alertBox dismissViewControllerAnimated:YES completion:nil];
        [self.confirmSignal sendNext:@NO];
    }];
    UIAlertAction *OKAction = [UIAlertAction actionWithTitle:otherTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @strongify(self);
        [alertBox dismissViewControllerAnimated:YES completion:nil];
        [self.confirmSignal sendNext:@YES];
    }];
    [alertBox addAction:cancelAction];
    [alertBox addAction:OKAction];
    UIViewController *currentVC = UIApplication.sharedApplication.keyWindow.topViewController;
    [currentVC presentViewController:alertBox animated:YES completion:nil];
}

#pragma mark--------------- 确认提示

+ (RACSignal *)confirmMessage:(NSString *)aMessage {
    return [self confirmTitle:LOCALIZATION(@"Alert")
                  withMessage:aMessage];
}

+ (RACSignal *)confirmTitle:(NSString *)aTitle withMessage:(NSString *)aMessage {
    return [self confirmTitle:aTitle
                  withMessage:aMessage
               withCanelTitle:LOCALIZATION(@"Cancel")
               withOtherTitle:LOCALIZATION(@"OK")];
}

+ (RACSignal *)confirmTitle:(NSString *)aTitle
                withMessage:(NSString *)aMessage
             withCanelTitle:(NSString*)cancelTitle
             withOtherTitle:(NSString*)otherTitle {
    static ConfirmMessageBox *confirmObj;
    confirmObj = [ConfirmMessageBox createInstance];
    [confirmObj confirmTitle:aTitle withMessage:aMessage withCanelTitle:cancelTitle withOtherTitle:otherTitle];
    return confirmObj.confirmSignal;
}

@end
