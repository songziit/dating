//
//  AlertView.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/22.
//  Copyright (c) 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import "MQAlertView.h"

@interface MQAlertAction ()

@property(nonatomic, readonly) void (^handler)(MQAlertAction *action);

@end

@interface MQAlertView ()

@property(nonatomic, strong) UIViewController *sheetController;
@property(nonatomic, strong) NSArray<MQAlertAction> *actions;

@end

@implementation MQAlertView

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions {
    [[self sharedInstance] showWithTitle:title message:message withAlertActions:actions];
}
+ (void)dismiss {
    [[self sharedInstance] dismiss];
}

#pragma mark - 私有方法

- (void)dismiss {
    [self dismissAlertController];
}

- (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions;
{
    self.actions = actions;
    [self showAlertControllerWithTitle:title message:message withAlertActions:actions];
}

- (void)dismissAlertController {
    [self.sheetController dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAlertControllerWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions;
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    [actions enumerateObjectsUsingBlock:^(MQAlertAction *obj, NSUInteger idx, BOOL *stop) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:obj.title style:(UIAlertActionStyle)obj.style handler:(void (^)(UIAlertAction *action))obj.handler];
        [alertController addAction:action];
    }];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIViewController *rootController = window.rootViewController;
    [rootController presentViewController:alertController animated:YES completion:nil];
    self.sheetController = alertController;
}

@end
