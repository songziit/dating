//
//  AlertView.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/22.
//  Copyright (c) 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MQActionSheet.h"

@interface MQAlertView : NSObject

+ (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions;
+ (void)dismiss;

- (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions;
- (void)dismiss;

@end
