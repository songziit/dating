//
//  YJGLoadingView.h
//  YJGCommon
//
//  Created by neil on 12/7/15.
//  Copyright © 2015 MeiQi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YJGLoadingView : UIView;


@property(nonatomic) CFTimeInterval animationTimeDuration;
/**
 *  @brief 开始旋转动画
 */
- (void)startRotateAnimation;

/**
 *  @brief 结束旋转动画
 */
- (void)stopRotateAnimation;

@end
