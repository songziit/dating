//
//  MessageBox.h
//  iNoknok
//
//  Created by 舒联勇 on 14-4-23.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  @brief 消息提示框
 */
@interface MessageBox : NSObject

/**
 *  @brief 消息提示
 *
 *  @param aMessage 提示内容
 */
+ (void)showMessage:(NSString *)aMessage;

/**
 *  @brief 完整的消息提示框
 *
 *  @param aTitle       标题
 *  @param aMessage     提示内容
 *  @param aCancelTitle 按钮名称
 */
+ (void)showTitle:(NSString *)aTitle
             withMessage:(NSString *)aMessage
    withCancelButonTitle:(NSString *)aCancelTitle;

+ (void)showError:(NSError *)error;

@end
