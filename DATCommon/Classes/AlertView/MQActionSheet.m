//
//  MQActionSheet.m
//  MeiQiReferrer
//
//  Created by neil on 15/5/22.
//  Copyright (c) 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import "MQActionSheet.h"

@interface MQAlertAction ()

@property(nonatomic, copy) void (^handler)(MQAlertAction *action);
@property(nonatomic, strong) NSString *title;
@property(nonatomic, assign) MQAlertActionStyle style;

@end
@implementation MQAlertAction

+ (instancetype)actionWithTitle:(NSString *)title style:(MQAlertActionStyle)style handler:(void (^)(MQAlertAction *action))handler {
    MQAlertAction *action = [[MQAlertAction alloc] init];
    if (action) {
        action.title = title;
        action.style = style;
        action.handler = handler;
        action.enabled = YES;
    }
    return action;
}

@end

@interface MQActionSheet ()<UIActionSheetDelegate>

@property(nonatomic, strong) NSArray *actions;
- (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions;

@property(nonatomic, strong) UIActionSheet *sheet;
@property(nonatomic, strong) UIViewController *sheetController;
@property(nonatomic, weak) UIViewController *rootVC;
@property(nonatomic, weak) UIView *rootView;

@end

@implementation MQActionSheet

#pragma mark - 生命周期

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

#pragma mark - 公有方法

+ (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions {
    [self showWithTitle:title message:message withAlertActions:actions inVC:nil inView:nil];
}

+ (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions inVC:(UIViewController *)aVC inView:(UIView *)aView;
{
    [[self sharedInstance] showWithTitle:title message:message withAlertActions:actions inVC:aVC inView:aView];
}

+ (void)dismiss {
    [[self sharedInstance] dismiss];
}

#pragma mark - 私有方法

- (void)dismiss {
    [self dismissAlertController];
}

- (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions inVC:(UIViewController *)aVC inView:(UIView *)aView;
{
    self.rootVC = aVC;
    if (aVC == nil) {
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        UIViewController *rootController = window.rootViewController;
        if (rootController.presentedViewController) {
            rootController = rootController.presentedViewController;
        }
        self.rootVC = rootController;
    }
    self.rootView = aView ?: self.rootVC.view;
    self.actions = actions;
    [self showAlertControllerWithTitle:title message:message withAlertActions:actions];
}

- (void)showWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions;
{
    [self showWithTitle:title message:message withAlertActions:actions inVC:nil inView:nil];
}

- (void)dismissAlertController {
    [self.sheetController dismissViewControllerAnimated:YES completion:nil];
}

- (void)showAlertControllerWithTitle:(NSString *)title message:(NSString *)message withAlertActions:(NSArray<MQAlertAction> *)actions;
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleActionSheet];
    [actions enumerateObjectsUsingBlock:^(MQAlertAction *obj, NSUInteger idx, BOOL *stop) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:obj.title style:(UIAlertActionStyle)obj.style handler:(void (^)(UIAlertAction *action))obj.handler];
        action.data = obj.data;
        action.enabled = obj.enabled;
        [alertController addAction:action];
    }];
    alertController.popoverPresentationController.sourceView = self.rootView;
    alertController.popoverPresentationController.sourceRect = self.rootView.frame;
    
    [self.rootVC presentViewController:alertController animated:YES completion:nil];
    self.sheetController = alertController;
}

@end

