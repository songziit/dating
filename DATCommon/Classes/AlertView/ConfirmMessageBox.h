//
//  ComfirmMessageBox.h
//  UserCar
//
//  Created by 舒联勇 on 14-4-30.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfirmMessageBox : NSObject

/**
 *  @brief 确定消息提示
 *
 *  @param aMessage        消息提示信息
 *
 */
+ (RACSignal*)confirmMessage:(NSString*)aMessage;

/**
 *  @brief 确定消息提示
 *
 *  @param aTitle          标题
 *  @param aMessage        提示内容
 */
+ (RACSignal*)confirmTitle:(NSString*)aTitle withMessage:(NSString*)aMessage;

+ (RACSignal *)confirmTitle:(NSString *)aTitle
                withMessage:(NSString *)aMessage
             withCanelTitle:(NSString*)cancelTitle
             withOtherTitle:(NSString*)otherTitle;

@end
