//
//  NSObject+Tips.m
//  Foomoo
//
//  Created by QFish on 6/6/14.
//  Copyright (c) 2014 QFish.inc. All rights reserved.
//

#import <MBProgressHUD/MBProgressHUD.h>
#import <objc/runtime.h>
#import "UIView+Tips.h"
#import "YJGLoadingView.h"

@interface UIView()

@property(nonatomic, strong) YJGLoadingView *loadView;

@end

__weak MBProgressHUD *_sharedHud;
@implementation UIView (Tips)
@dynamic loading;

#pragma mark - life cycle

#pragma mark - public method
+ (void)presentMessageTips:(NSString *)message {
    [self showTips:message autoHide:YES withModel:MBProgressHUDModeText];
}

+ (void)presentMessageTips:(NSString *)message toView:(UIView*)view{    
    [self showTipsTo:view message:message autoHide:YES withModel:MBProgressHUDModeText];
}


+ (void)presentSuccessTips:(NSString *)message {
    [self showTips:message autoHide:YES withModel:MBProgressHUDModeText];
}

+ (void)presentSuccessTips:(NSString *)message  showTime:(CGFloat)showTime{
    [self showTips:message autoHide:YES withModel:MBProgressHUDModeText withSeconds:showTime];
}

+ (void)presentFailureTips:(NSString *)message {
    [self showTips:message autoHide:YES withModel:MBProgressHUDModeText];
}

+ (void)presentLoadingTips:(NSString *)message {
    [self showTips:message autoHide:NO withModel:MBProgressHUDModeIndeterminate];
}

+ (void)presentLoadingTipsTo:(UIView *)view message:(NSString *)message {
    [self showTipsTo:view message:message autoHide:NO withModel:MBProgressHUDModeIndeterminate];
}

+ (void)presentLoading {
    [self presentLoadingTips:LOCALIZATION(@"Loading...")];
}

+ (void)presentLoading:(NSString *)message {
    [self presentLoadingTips:message];
}

+ (void)presentLoadingTo:(UIView *)view {
    [self presentLoadingTipsTo:view message:LOCALIZATION(@"Loading...")];
}

+ (void)presentProgressTips:(NSString *)message {
    [self showTips:message autoHide:NO withModel:MBProgressHUDModeText];
}

+ (void)presentSuccessTipsWithPicture:(NSString *)message {
    UIImage *image = [UIImage imageNamed:@"success"];
    [self showTipsWithImage:image message:message autoHide:YES withModel:MBProgressHUDModeCustomView];
}

+ (void)dismissTips {
    [_sharedHud hideAnimated:YES];
}

#pragma mark - private method
+ (void)showTips:(NSString *)message autoHide:(BOOL)autoHide withModel:(MBProgressHUDMode)model {
    [self showTipsTo:[UIApplication sharedApplication].keyWindow message:message autoHide:autoHide withModel:model];
}

+ (void)showTips:(NSString *)message autoHide:(BOOL)autoHide withModel:(MBProgressHUDMode)model withSeconds:(CGFloat)seconds {
    [self showTipsTo:[UIApplication sharedApplication].keyWindow message:message autoHide:autoHide withModel:model withSeconds:seconds];
}

+ (void)showTipsTo:(UIView *)view message:(NSString *)message autoHide:(BOOL)autoHide withModel:(MBProgressHUDMode)model {
    NSAssert(view, @"View must not be nil.");
    if (nil != _sharedHud) {
        [_sharedHud hideAnimated:NO];
    }

    _sharedHud = [self buildHUDTo:view statusImage:nil message:message autoHide:autoHide withModel:model];
    if (autoHide) {
        [_sharedHud hideAnimated:YES afterDelay:1.0f];
    }
}

+ (void)showTipsTo:(UIView *)view message:(NSString *)message
          autoHide:(BOOL)autoHide withModel:(MBProgressHUDMode)model withSeconds:(CGFloat)showSeconds{
    NSAssert(view, @"View must not be nil.");
    if (nil != _sharedHud) {
        [_sharedHud hideAnimated:NO];
    }
    
    _sharedHud = [self buildHUDTo:view statusImage:nil message:message autoHide:autoHide withModel:model];
    if (autoHide) {
        [_sharedHud hideAnimated:YES afterDelay:showSeconds];
    }
}

+ (void)showTipsWithImage:(UIImage *)statusImage message:(NSString *)message autoHide:(BOOL)autoHide withModel:(MBProgressHUDMode)model {
    if (nil != _sharedHud) {
        [_sharedHud hideAnimated:NO];
    }
    _sharedHud = [self buildHUDTo:[UIApplication sharedApplication].keyWindow statusImage:statusImage message:message autoHide:autoHide withModel:model];
    if (autoHide) {
        [_sharedHud hideAnimated:YES afterDelay:1.0f];
    }
}

+ (MBProgressHUD *)buildHUDTo:(UIView *)view statusImage:(UIImage *)statusImage message:(NSString *)message autoHide:(BOOL)autoHide withModel:(MBProgressHUDMode)model {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.mode = model;
    hud.label.text = message;
    if (statusImage) {
        hud.customView = [[UIImageView alloc] initWithImage:statusImage];
        hud.alpha = 0.6f;
    }
    return hud;
}

#pragma mark - 对象方法

- (YJGLoadingView *)loadView {
    static char loadViewKey;
    YJGLoadingView *loadView = objc_getAssociatedObject(self, &loadViewKey);
    if (loadView == nil) {
        loadView = [YJGLoadingView createInstance];
        objc_setAssociatedObject(self, &loadViewKey, loadView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return loadView;
}

- (void)presentLoading {
    [self.loadView removeFromSuperview];
    self.loadView.frame = self.bounds;
    [self addSubview:self.loadView];
    [self.loadView startRotateAnimation];
}
- (void)dismissLoading {
    [self.loadView stopRotateAnimation];
    [self.loadView removeFromSuperview];
}

- (void)setLoading:(BOOL)loading
{
    if(loading)
    {
        if(!self.loading)
            [self presentLoading];
    }
    else
    {
        [self dismissLoading];
    }
}

- (BOOL)loading{
    return self.loadView.superview!=nil;
}

@end
