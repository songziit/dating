//
//  MessageBox.m
//  iNoknok
//
//  Created by 舒联勇 on 14-4-23.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import "MessageBox.h"

@implementation MessageBox

+ (void)showMessage:(NSString *)aMessage {
    [self showTitle:LOCALIZATION(@"Alert")
                 withMessage:aMessage
        withCancelButonTitle:LOCALIZATION(@"OK")];
}

+ (void)showTitle:(NSString *)aTitle
             withMessage:(NSString *)aMessage
    withCancelButonTitle:(NSString *)aCancelTitle {
//    UIAlertView *box = [[UIAlertView alloc] initWithTitle:aTitle
//                                                  message:aMessage
//                                                 delegate:nil
//                                        cancelButtonTitle:aCancelTitle
//                                        otherButtonTitles:nil];
//    [box show];    
    UIAlertController *alertBox = [UIAlertController alertControllerWithTitle:aTitle message:aMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:aCancelTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertBox dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertBox addAction:cancelAction];
    UIViewController *currentVC = UIApplication.sharedApplication.keyWindow.topViewController;
    [currentVC presentViewController:alertBox animated:YES completion:nil];
}

+ (void)showError:(NSError *)error {
    ENTER(@"presentError:%@", error);
    NSParameterAssert([error isKindOfClass:[NSError class]]);
    NSError *newError = [self filterError:error];
    [MessageBox showTitle:newError.localizedDescription
              withMessage:newError.localizedFailureReason
     withCancelButonTitle:NSLocalizedString(@"OK", nil)];
}

+ (NSError *)filterError:(NSError *)error {
    if (error.code != NSURLErrorNotConnectedToInternet) {
        return error;
    } else {
        return [NSError errorWithTitle:LOCALIZATION(@"Alert")
                                reason:LOCALIZATION(@"Network error, please check the network connection.")];
    }
}
@end
