//
//  YJGLoadingView.m
//  YJGCommon
//
//  Created by neil on 12/7/15.
//  Copyright © 2015 MeiQi. All rights reserved.
//

#import "YJGLoadingView.h"

@interface YJGLoadingView ()

@property(weak, nonatomic) IBOutlet UIImageView *imgLoading;

@end

@implementation YJGLoadingView
@synthesize animationTimeDuration = _animationTimeDuration;

#pragma mark - life cycle
+ (instancetype)createInstance {
    return [self loadFromNib];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
    }
    return self;
}

- (void)dealloc {
}

- (void)setAnimationTimeDuration:(CFTimeInterval)duration {
    _animationTimeDuration = duration;
    [self startRotateAnimation];
}

- (CFTimeInterval)animationTimeDuration {
    if (_animationTimeDuration == 0) {
        _animationTimeDuration = 1.0f;
    }
    return _animationTimeDuration;
}

- (void)startRotateAnimation {
    if (self.imgLoading.layer.animationKeys.count > 0) {
        return;
    }
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = @(0);
    animation.toValue = @(2 * M_PI);
    animation.duration = self.animationTimeDuration;
    animation.repeatCount = INT_MAX;

    [self.imgLoading.layer addAnimation:animation forKey:@"keyFrameAnimation"];
}

- (void)stopRotateAnimation {
    [self.imgLoading.layer removeAllAnimations];
}

@end
