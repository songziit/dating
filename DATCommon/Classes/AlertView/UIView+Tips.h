//
//  NSObject+Tips.h
//  Foomoo
//
//  Created by QFish on 6/6/14.
//  Copyright (c) 2014 QFish.inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView (Tips)

+ (void)presentMessageTips:(NSString *)message;
+ (void)presentMessageTips:(NSString *)message toView:(UIView*)view;
+ (void)presentSuccessTips:(NSString *)message;
+ (void)presentFailureTips:(NSString *)message;
+ (void)presentLoadingTips:(NSString *)message;
+ (void)presentLoadingTipsTo:(UIView *)view message:(NSString *)message;
+ (void)presentLoading;
+ (void)presentLoadingTo:(UIView *)view;
+ (void)presentProgressTips:(NSString *)message;
+ (void)presentSuccessTipsWithPicture:(NSString *)message;

+ (void)presentLoading:(NSString *)message;

+ (void)presentSuccessTips:(NSString *)message  showTime:(CGFloat)showTime;

#pragma mark - dismiss
+ (void)dismissTips;

- (void)presentLoading;
- (void)dismissLoading;
@property(nonatomic) BOOL loading;

@end
