//
//  TimerCounter.m
//  Loan
//
//  Created by neil on 2017/3/12.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import "TimerCounter.h"

/**
 *   @brief  最大验证码计数
 */
static NSInteger MaxCodeCount = 60;

@interface TimerCounter ()

@property(nonatomic, weak) NSTimer *codeTimer;
@property(nonatomic) BOOL countEnd;
@property(nonatomic) NSInteger codeCount;

@end

@implementation TimerCounter

+ (instancetype)sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self.class alloc] init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.countEnd = YES;
        self.codeCount = 0;
    }
    return self;
}

- (void)counterSelector:(id)sender {
    if (self.codeCount <= 0) {
        [self endCount];
    } else {
        self.codeCount--;
    }
}

- (void)beginCount {
    self.countEnd = NO;
    if (self.codeTimer) {
        [self endCount];
    }
    self.codeCount = MaxCodeCount;
    self.codeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(counterSelector:) userInfo:nil repeats:YES];
}

- (void)endCount {
    self.codeCount = 0;
    self.countEnd = YES;
    [self.codeTimer invalidate];
    self.codeTimer = nil;
}


@end
