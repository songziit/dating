//
//  RegularExpressionResource.h
//  SuperRunErrands
//
//  Created by neil on 2018/12/13.
//  Copyright © 2018 Sly. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegularExpressionResource : NSObject

+ (NSDictionary*)resource;

@end

NS_ASSUME_NONNULL_END
