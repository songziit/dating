//
//  main.m
//  MQKit
//
//  Created by cailu on 16/2/20.
//  Copyright © 2016年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
