//
//  AppDelegate.h
//  MQKit
//
//  Created by cailu on 16/2/20.
//  Copyright © 2016年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

