#
# Be sure to run `pod lib lint MQKit.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "MQKit"
  s.version          = "0.3.1"
  s.summary          = "MeiQi custom MQKit."
  s.description      = "MeiQi UI and Coustom kits "
  s.homepage         = "http://git.oschina.net/louis.cai/MQKit"
  s.license          = 'MIT'
  s.author           = { "cailu" => "louis.cai.cn@gmail.com" }
  s.source           = { :git => "git@git.oschina.net:louis.cai/MQKit.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.resource_bundles = {'MQKit' => ['Assets/**/*']}

  s.default_subspec = 'Core','UI'
  s.subspec 'Core' do |core|
    core.source_files = 'Classes/Core/*.{h,m}'
    core.frameworks   = 'Foundation'
  end

  s.subspec 'UI' do |ui|
    ui.source_files = 'Classes/UI/*.{h,m}'
    ui.frameworks   = 'UIKit'
  end

  s.dependency 'MQLogger', '~> 0.1'
end
