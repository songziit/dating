//
//  NSString+MIMETests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+MIME.h"

@interface NSString_MIMETests : XCTestCase

@end

@implementation NSString_MIMETests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testsMIMEType {
    NSString* string = @"sakdhfjdsahfkjas'";
    [string MIMEType];
}
- (void)testsMIMETypeForExtension {
    NSString* string = @"sakdhfjdsahfkjas'";
    [NSString MIMETypeForExtension:string];
}
- (void)testsMIMEDict {
    NSDictionary* dic = [NSString MIMEDict];
    XCTAssertTrue(dic != nil, @"file is not zero length");
}

@end
