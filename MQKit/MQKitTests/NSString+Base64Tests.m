//
//  NSString+Base64Tests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSData+Base64.h"
#import "NSString+Base64.h"

@interface NSString_Base64Tests : XCTestCase

@end

@implementation NSString_Base64Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testsstringWithBase64EncodedString {
    NSString *string = [NSString stringWithBase64EncodedString:@"fasdfsagas"];
    XCTAssertNil(string, @"stringWithBase64EncodedString is not work");
}

- (void)testsbase64DecodedData {
    NSString *string = @"fasdfsagas";
    NSData *data = [string base64DecodedData];
    XCTAssertNil(data, @"base64DecodedData is not work");
}

@end
