//
//  NSDictionary+JSONStringTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDictionary+JSONString.h"

@interface NSDictionary_JSONStringTests : XCTestCase

@end

@implementation NSDictionary_JSONStringTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
-(void)testsJSONString{
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
    NSString * string =[dic JSONString];
     XCTAssert(string != nil, @"JSONString is not work");
}

@end
