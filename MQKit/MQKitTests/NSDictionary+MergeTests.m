//
//  NSDictionary+MergeTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDictionary+Merge.h"
#import "NSDictionary+Block.h"

@interface NSDictionary_MergeTests : XCTestCase

@end

@implementation NSDictionary_MergeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.        NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
    [super tearDown];
}
-(void)testsdictionaryByMerging{
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
    NSDictionary *dic1=[NSDictionary dictionaryWithObjectsAndKeys:@"vone",@"kone",@"vtwo",@"ktwo",@"vthree",@"kthree",@"vfour",@"kfour",@"vfive",@"kfive",nil];
    dic = [NSDictionary dictionaryByMerging:dic with:dic1];
    [dic each:^(id k, id v) {
        NSLog(@"%@",k);
    }];
    [dic  dictionaryByMergingWith:dic1];
    [dic each:^(id k, id v) {
        NSLog(@"%@",k);
    }];
    }
@end
