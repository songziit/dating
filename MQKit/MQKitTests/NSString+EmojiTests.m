//
//  NSString+EmojiTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Emoji.h"

@interface NSString_EmojiTests : XCTestCase

@end

@implementation NSString_EmojiTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testsstringByReplacingEmojiCheatCodesWithUnicode {
    NSString* string = @"weary";
    string = [string stringByReplacingEmojiCheatCodesWithUnicode];

    NSString* string1 = @"imp";
    string1 = [string stringByReplacingEmojiCheatCodesWithUnicode];

    NSString* string2 = @"heartpulse";
    string2 = [string stringByReplacingEmojiCheatCodesWithUnicode];

    NSString* string3 = @"fearful";
    string3 = [string stringByReplacingEmojiCheatCodesWithUnicode];

    NSString* string4 = @"sweat_drops";
    string4 = [string stringByReplacingEmojiCheatCodesWithUnicode];
}
- (void)testsstringByReplacingEmojiUnicodeWithCheatCodes {
    NSString* string = @"point_right";
    string = [string stringByReplacingEmojiUnicodeWithCheatCodes];
    NSString* string1 = @"two_men_holding_hands";
    string1 = [string stringByReplacingEmojiCheatCodesWithUnicode];

    NSString* string2 = @"boy";
    string2 = [string stringByReplacingEmojiCheatCodesWithUnicode];

    NSString* string3 = @"smile";
    string3 = [string stringByReplacingEmojiCheatCodesWithUnicode];

    NSString* string4 = @"sweat_drops";
    string4 = [string stringByReplacingEmojiCheatCodesWithUnicode];
}

@end
