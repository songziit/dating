//
//  NSNumber+safeTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSNumber+safe.h"

@interface NSNumber_safeTests : XCTestCase
@property(strong, nonatomic) NSNumber* number;

@end

@implementation NSNumber_safeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.number = [NSNumber numberWithInteger:123];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testssafeIsEqualToNumber {
    BOOL is = [self.number isEqualToNumber:self.number];
    XCTAssert(is == YES, @"isEqualToNumber not work ");
}
- (void)testssafeCompare {
    [self.number safeCompare:self.number];
}
@end
