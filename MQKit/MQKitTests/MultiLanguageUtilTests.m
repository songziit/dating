//
//  MultiLanguageUtilTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/23.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MultiLanguageUtil.h"
#import "NSString+Util.h"

@interface MultiLanguageUtilTests : XCTestCase

@end

@implementation MultiLanguageUtilTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testshareInstance {
    id lang = [MultiLanguageUtil shareInstance];
    NSString* string = [lang currentLanguage];
    [lang setCurrentLanguage:string];

    XCTAssertEqual(@"clear...", LOCALIZATION(@"clear..."), @"多语言错误");

    XCTAssertEqual(@"付款失败", LOCALIZATION(@"付款失败"), @"多语言错误");
}
@end
