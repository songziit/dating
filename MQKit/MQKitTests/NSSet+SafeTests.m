//
//  NSSet+SafeTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSSet+Safe.h"

@interface NSSet_SafeTests : XCTestCase

@end

@implementation NSSet_SafeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testsaddObj {
    NSMutableSet* set = [NSMutableSet set];
    [set addObj:@"1"];
    [set addObj:@"2"];
    XCTAssert(set.count == 2, @"NSMutableSet add success.");

    [set addObj:nil];
    XCTAssert(set.count == 2, @"NSMutableSet add success.");

    [set addObj:NULL];
    XCTAssert(set.count == 2, @"NSMutableSet add success.");
}

@end
