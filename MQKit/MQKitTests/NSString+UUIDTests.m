//
//  NSString+UUIDTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface NSString_UUIDTests : XCTestCase

@end

@implementation NSString_UUIDTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    NSString* str = @"hello, world!";
    NSData* data = [str dataUsingEncoding:NSUTF8StringEncoding];  //NSString转换成NSData类型
    NSLog(@"%s", data.bytes);
    NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", newStr);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

@end
