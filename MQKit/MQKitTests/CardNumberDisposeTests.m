//
//  CardNumberDisposeTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/23.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CardNumberDispose.h"

@interface CardNumberDisposeTests : XCTestCase

@end

@implementation CardNumberDisposeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
     XCTAssert(@"num should less than 10");
}
- (void)testreturnCardLastNumber
{
   NSString * number= @"56823642894723542";
    number = [CardNumberDispose returnCardLastNumber:number withCount:4];
    XCTAssert(number.length<=4,@"num should less than 4");
    XCTAssert([number isEqualToString:@"3542"],@"num is wrong");
    
    NSString * number1= @" ";
    number1 = [CardNumberDispose returnCardLastNumber:number1 withCount:4];
    XCTAssert(number1.length<=4,@"num should less than 4");
    XCTAssert([number1 isEqualToString:@" "],@"num is wrong");
    
  }

@end
