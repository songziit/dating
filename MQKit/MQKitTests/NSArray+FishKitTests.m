//
//  NSArray+FishKitTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSArray+FishKit.h"
@interface NSArray_FishKitTests : XCTestCase

@end

@implementation NSArray_FishKitTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
-(void)testsnonRetainingArray{
   NSMutableArray * array= [NSMutableArray nonRetainingArray];
    XCTAssert(array!= nil,@"nonRetainingArray not work");
    
}

@end
