//
//  NSString+UtilTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Expecta/Expecta.h"
#import "NSString+Util.h"

@interface NSString_UtilTests : XCTestCase

@end

@implementation NSString_UtilTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testIsEmpty {
    XCTAssertTrue([NSString isEmpty:@""], @"对比空的字符串");
    XCTAssertFalse([NSString isEmpty:@"1"], @"对比字符串");
    XCTAssertTrue([NSString isEmpty:nil], @"对比nil");
}

- (void)testUrlEncode {
    expect([NSString urlEncode:@""]).to.equal(@"");
}

- (void)testUrlDecode {
    expect([NSString urlDecode:@""]).to.equal(@"");
}

- (void)testStringWithInteger {
    expect([NSString stringWithInteger:0]).to.equal(@"0");
    expect([NSString stringWithInteger:1]).to.equal(@"1");
    expect([NSString stringWithInteger:-2]).to.equal(@"-2");
    expect([NSString stringWithInteger:-3]).notTo.equal(@"3");
    expect([NSString stringWithInteger:2]).notTo.equal(@"1");
}

- (void)testMD5 {
    expect([NSString MD5:@"1"]).to.equal(@"C4CA4238A0B923820DCC509A6F75849B");
    expect([NSString MD5:@"%"]).to.equal(@"0BCEF9C45BD8A48EDA1B26EB0C61C869");
    expect([NSString MD5:@""]).to.equal(@"D41D8CD98F00B204E9800998ECF8427E");
    expect([NSString MD5:nil]).to.equal(nil);
}
@end
