//
//  NSData+EncryptTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <CommonCrypto/CommonCryptor.h>
#import <XCTest/XCTest.h>
#import "NSData+Encrypt.h"

@interface NSData_EncryptTests : XCTestCase

@end

@implementation NSData_EncryptTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

@end
