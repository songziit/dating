//
//  NSString+HashTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+hash.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>

@interface NSString_HashTests : XCTestCase

@end

@implementation NSString_HashTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testshmacMD5StringWithKey {
    NSString* string = @"ashfjsadf;";
    string = [string hmacMD5StringWithKey:string];
}
- (void)testshmacSHA1StringWithKey {
    NSString* string = @"ashfjsadf;";
    string = [string hmacSHA1StringWithKey:string];
}
- (void)testshmacSHA256StringWithKey {
    NSString* string = @"ashfjsadf;";
    string = [string hmacSHA256StringWithKey:string];
}
@end
