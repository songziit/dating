//
//  NSDictionary+BlockTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDictionary+Block.h"

@interface NSDictionary_BlockTests : XCTestCase

@end

@implementation NSDictionary_BlockTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
-(void)testsdictionaryByAddingEntriesFromDictionary{
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
    dic = [dic dictionaryByAddingEntriesFromDictionary:dic];
    XCTAssert(dic != nil, @"tomorrow0 is not work");
}
-(void)testsdictionaryByRemovingEntriesWithKeys{
     NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
    NSSet * set = [[NSSet alloc] initWithObjects:@"1",@"2",@"3",@"4", nil];
    dic =  [dic dictionaryByRemovingEntriesWithKeys:set];
    XCTAssert(dic != nil, @"tomorrow0 is not work");
}
-(void)testseach{
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
    [dic each:^(id k, id v) {
        NSLog(@"%@",k);
    }];
    
}
-(void)testseachKey{
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
    [dic eachKey:^(id k) {
        NSLog(@"%@",k);
    }];
}
-(void)testeachValue{
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
    [dic eachValue:^(id v) {
        NSLog(@"%@",v);
    }];
    
}
-(void)testspick{
    NSArray *array = [NSArray arrayWithObjects:@"wendy", @"andy", @"tom", @"test", nil];
    NSDictionary *dic=[NSDictionary dictionaryWithObjectsAndKeys:@"v1",@"k1",@"v2",@"k2",@"v3",@"k3",@"v4",@"k4",@"v5",@"k5",nil];
   dic =  [dic pick:array];
   XCTAssert(dic != nil, @"tomorrow0 is not work");
    
    
}
@end
