//
//  NSArray+SafeAccessTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSArray+SafeAccess.h"

@interface NSArray_SafeAccessTests : XCTestCase

@end

@implementation NSArray_SafeAccessTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testNumberWithIndex {
    NSArray *array = @[ @"wendy", @(1), @"tom", @"test" ];
    NSNumber *number = [array numberWithIndex:3];
    XCTAssertNil(number, @"numberWithIndex not work");

    XCTAssertNotNil([array numberWithIndex:1], @"numberWithIndex not work");

    XCTAssertEqual(@(1), [array numberWithIndex:1], @"numberWithIndex not work");
}

- (void)testsdictionaryWithIndex {
    NSArray *array = @[ @"wendy", @{ @"key" : @"value" }, @"tom", @"test" ];
    NSDictionary *dic = [array dictionaryWithIndex:0];
    XCTAssertNil(dic, @" dictionaryWithIndex not work");

    XCTAssertNotNil([array dictionaryWithIndex:1], @"dictionaryWithIndex not work");

    dic = [array dictionaryWithIndex:1];
    XCTAssertEqual(@"value", [dic objectForKey:@"key"], @"numberWithIndex not work");
}

//- (void)testscharWithIndex {
//    NSArray *array = @[ @"wendy", @"andy", @"tom", @"test" ];
//    [array charWithIndex:2];
//}

//- (void)testsunsignedIntegerWithIndex {
//    NSArray *array = @[ @"wendy", @"andy", @"tom", @"test" ];
//    [array unsignedIntegerWithIndex:1];
//}

//- (void)testsfloatWithIndex {
//    NSArray *array = @[ @"wendy", @"andy", @"tom", @"test" ];
//    [array floatWithIndex:0];
//}
@end
