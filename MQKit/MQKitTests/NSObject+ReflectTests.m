//
//  NSObject+ReflectTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <XCTest/XCTest.h>
#import <objc/runtime.h>
#import "NSObject+Reflect.h"

@interface NSObject_ReflectTests : XCTestCase

@end

@implementation NSObject_ReflectTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInitWithJsonString {
    NSString *jsonString = @"";
    id obj = [[NSObject alloc] initWithJsonString:jsonString];
    XCTAssertNil(obj, @"initWithJsonString not work ");

    jsonString = @"[]";
    obj = [[NSObject alloc] initWithJsonString:jsonString];
    XCTAssertNotNil(obj, @"initWithJsonString not work ");
}

- (void)testsinitWithDictionary {
    //    id obj = [NSObject createInstance];
    //    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@"v1", @"k1", @"v2", @"k2", @"v3", @"k3", @"v4", @"k4", @"v5", @"k5", nil];
    //    obj = [obj initWithDictionary:dic];
}

- (void)testsaddMethod {
    //    id obj = [NSObject createInstance];
    //    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SolutionDetail" ofType:@"json"];
    //    NSError *error = nil;
    //    NSString *jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    //    [obj addObject:jsonString];
}

- (void)testsencodeWithCoder {
    //    id obj = [NSObject createInstance];
    //    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SolutionDetail" ofType:@"json"];
    //    NSError *error = nil;
    //    NSString *jsonString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
    //    NSString *filePath1 = [[NSBundle mainBundle] pathForResource:@"goodsDetails" ofType:@"json"];
    //    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"YJGGoodsSort" ofType:@"json"]];
    //    [obj encodeObject:data forKey:filePath];
}

@end
