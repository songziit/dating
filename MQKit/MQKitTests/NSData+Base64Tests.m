//
//  NSData+Base64Tests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <Availability.h>
#import <XCTest/XCTest.h>
#import "NSData+Base64.h"
@interface NSData_Base64Tests : XCTestCase

@end

@implementation NSData_Base64Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testsdataWithBase64EncodedString {
    NSString *string = @"sagdfjsd/dsa';f[/s'a;f[s/f'[dsf[s'd//";
    NSData *date = [NSData dataWithBase64EncodedString:string];
    XCTAssertNil(date, @"dataWithBase64EncodedString is not work");
    NSString *string1 = @"sagdfjsd1223424324324";
    NSData *data1 = [NSData dataWithBase64EncodedString:string1];
    XCTAssertNil(data1, @"dataWithBase64EncodedString is not work");
}

//- (void)testsbase64EncodedStringWithWrapWidth {
//    NSString *string = [jdata base64EncodedStringWithWrapWidth:4];
//    XCTAssertNil(string != nil, @"base64EncodedStringWithWrapWidth is not work");
//}
//
//- (void)testsbase64EncodedString {
//    NSString *string = [jdata base64EncodedString];
//    XCTAssertNil(string != nil, @"base64EncodedString is not work");
//}
@end
