//
//  NSArray+BlockTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSArray+Block.h"
@interface NSArray_BlockTests : XCTestCase

@end

@implementation NSArray_BlockTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testEach {
    NSArray *array = @[ @"wendy", @"andy", @"tom", @"test" ];
    [array enumerateObjectsUsingBlock:^(id str, NSUInteger index, BOOL *te) {
        NSLog(@"%@,%lu", str, (unsigned long)index);
    }];

    [array enumerateObjectsWithOptions:NSEnumerationReverse
                            usingBlock:^(id str, NSUInteger index, BOOL *te) {
                                NSLog(@"%@,%lu", str, (unsigned long)index);
                            }];

    [array enumerateObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, 3)]
                             options:NSEnumerationReverse
                          usingBlock:^(id str, NSUInteger index, BOOL *te) {
                              NSLog(@"%@,%lu", str, (unsigned long)index);
                          }];

    NSInteger index = [array indexOfObjectPassingTest:^BOOL(id tr, NSUInteger index, BOOL *te) {
        NSString *s = (NSString *)tr;
        if ([@"wendy" isEqualToString:s]) {
            return YES;
        }
        return NO;
    }];

    NSLog(@"index==%ld=.", (long)index);
}

- (void)testEachWithIndex {
    NSArray *array = @[ @"wendy", @"andy", @"tom", @"test" ];
    [array each:^(id object) {
        id string = object;
        NSLog(@"%@", string);
    }];
}

@end
