//
//  NSString+PinyinTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Pinyin.h"

@interface NSString_PinyinTests : XCTestCase

@end

@implementation NSString_PinyinTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testspinyinWithPhoneticSymbol {
    NSString *string = @"伤风败绝大部分就俗把手机发布会介绍大包";
    [string pinyinWithPhoneticSymbol];
}
- (void)testspinyin {
    NSString *string = @"把手机发";
    [string pinyin];
}
- (void)testspinyinArray {
    NSString *string = @"把手机发";
    NSArray *array = [string pinyinArray];
    XCTAssert(array != nil, @"JSONString is not work");
}
- (void)testspinyinWithoutBlank {
    NSString *string = @"safasfas";
    [string pinyinWithoutBlank];
}

@end
