//
//  NSArray+UtilTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSArray+Util.h"

@interface NSArray_UtilTests : XCTestCase

@end

@implementation NSArray_UtilTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testsisArray {
    XCTAssertFalse([NSArray isArray:nil], @"nil is Array");

    XCTAssertFalse([NSArray isArray:@"string"], @"string is Array");

    XCTAssertFalse([NSArray isArray:@(1)], @"number is Array");

    XCTAssertTrue([NSArray isArray:@[]], @"array is Array");
}

- (void)testIsNotArray {
    XCTAssertTrue([NSArray isNotArray:nil], @"nil is Not Array");

    XCTAssertTrue([NSArray isNotArray:@"string"], @"string is Not Array");

    XCTAssertTrue([NSArray isNotArray:@(1)], @"number is Not Array");

    XCTAssertFalse([NSArray isNotArray:@[]], @"array is Not Array");
}

- (void)testIsNotEmpty {
    XCTAssertFalse([NSArray isNotEmpty:@"sjfskafjasjkf"], @"string isNotEmpty is Empty");

    XCTAssertFalse([NSArray isNotEmpty:nil], @"nil isNotEmpty is Empty");

    XCTAssertFalse([NSArray isNotEmpty:@[]], @"@[] isNotEmpty is Empty");

    NSArray *array = @[ @"1" ];
    XCTAssertTrue([NSArray isNotEmpty:array], @"@[] isNotEmpty is Empty");

    array = @[ @"1", @"2" ];
    XCTAssertTrue([NSArray isNotEmpty:array], @"@[] isNotEmpty is Empty");

    array = NULL;
    XCTAssertFalse([NSArray isNotEmpty:array], @"@[] isNotEmpty is Empty");
}

- (void)testIsEmpty {
    XCTAssertTrue([NSArray isEmpty:@"sjfskafjasjkf"], @"string isNotEmpty is Empty");

    XCTAssertTrue([NSArray isEmpty:nil], @"nil isNotEmpty is Empty");

    XCTAssertTrue([NSArray isEmpty:@[]], @"@[] isNotEmpty is Empty");

    NSArray *array = @[ @"1" ];
    XCTAssertFalse([NSArray isEmpty:array], @"@[] isNotEmpty is Empty");

    array = @[ @"1", @"2" ];
    XCTAssertFalse([NSArray isEmpty:array], @"@[] isNotEmpty is Empty");

    array = NULL;
    XCTAssertTrue([NSArray isEmpty:array], @"@[] isNotEmpty is Empty");
}

@end
