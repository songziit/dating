//
//  NSString+ContainsTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Contains.h"

@interface NSString_ContainsTests : XCTestCase

@end

@implementation NSString_ContainsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIsContainChinese {
    NSString* string = @"sdf是不会地方";
    BOOL is = [string isContainChinese];
    XCTAssert(is == YES, @"base64DecodedData is not work");
}

- (void)testIsContainBlank {
    NSString* string = @"sdf是不  会地方";
    BOOL is = [string isContainBlank];
    XCTAssert(is == YES, @"isContainBlank is not work");
}

- (void)testMakeUnicodeToString {
    NSString* string = @"sdf是不  会地方";
    string = [string makeUnicodeToString];
}

- (void)testContainsString {
    NSString* string = @"sdf是不  会地方";
    NSString* string1 = @"safas";
    [string containsString:string1];
}

@end
