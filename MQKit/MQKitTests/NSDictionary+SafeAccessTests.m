//
//  NSDictionary+SafeAccessTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSDictionary+SafeAccess.h"

@interface NSDictionary_SafeAccessTests : XCTestCase
@property(strong, nonatomic) NSDictionary *dic;
@end

@implementation NSDictionary_SafeAccessTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.dic = [NSDictionary dictionaryWithObjectsAndKeys:@"v1", @"k1", @"v2", @"k2", @"v3", @"k3", @"v4", @"k4", @"v5", @"k5", nil];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testshasKey {
    NSDictionary *dic = @{ @"k1" : @"v1",
                           @"k2" : @"v2" };
    BOOL has = [dic hasKey:@"k1"];
    XCTAssert(has == YES, @"hasKey not work ");
}

- (void)testsstringForKey {
    NSDictionary *dic = @{ @"k1" : @"1",
                           @"k3" : @"v3" };
    NSString *string = [dic stringForKey:@"k3"];
    XCTAssert([string isEqualToString:@"v3"], @"stringForKey not work ");
}

- (void)testsnumberForKey {
    NSDictionary *dic = @{ @"k1" : @"1",
                           @"k4" : @(1) };
    NSNumber *number = [dic numberForKey:@"k4"];
    XCTAssert([number isEqual:@(1)], @"numberForKey not work ");
}

- (void)testsdecimalNumberForKey {
    [self.dic decimalNumberForKey:@"k1"];
}

- (void)testsarrayForKey {
    NSDictionary *dic = @{ @"k2" : @[ @"1", @"2" ],
                           @"k4" : @(1) };
    NSArray *array = [dic arrayForKey:@"k2"];
    XCTAssert(array != nil, @"arrayForKey not work ");
}

- (void)testsintegerForKey {
    NSDictionary *dic = @{ @"k2" : @"1",
                           @"k4" : @(1) };
    NSInteger number = [dic integerForKey:@"k2"];
    XCTAssert(number == 1, @"integerForKey not work ");
}

- (void)testsboolForKey {
    NSDictionary *dic = @{ @"k2" : @"1" };
    BOOL is = [dic boolForKey:@"k2"];
    XCTAssert(is, @"boolForKey not work ");
}

- (void)testscharForKey {
    NSDictionary *dic = @{ @"k1" : @"x" };
    char v = [dic charForKey:@"k1"];
    XCTAssert(v == 0, @"boolForKey not work ");
}

- (void)testsshortForKey {
    [self.dic charForKey:@"k2"];
}

@end
