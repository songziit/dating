//
//  NSString+RemoveEmojiTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+RemoveEmoji.h"

@interface NSString_RemoveEmojiTests : XCTestCase

@end

@implementation NSString_RemoveEmojiTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testsisIncludingEmoji {
    NSString *string = @"dsajfhashfjldksag上的华国锋一个哈萨克疯狂激动撒";
    BOOL is = [string isIncludingEmoji];
    XCTAssertTrue(is == NO, @" isIncludingEmoji not work");
}
- (void)testsremovedEmojiString {
    NSString *string = @"sfsfassfas";
    [string removedEmojiString];
}

@end
