//
//  NSDate+UtilTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "MultiLanguageUtil.h"
#import "NSDate+Util.h"

@interface NSDate_UtilTests : XCTestCase

@end

@implementation NSDate_UtilTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testsdateFromRFC2822 {
    NSString *string = @"rfc2822";
    NSDate *date = [NSDate dateFromRFC2822:string];
    XCTAssertNil(date);
}

- (void)testsdescriptionAsFormat {
    NSString *date = @"234576";
    NSDate *time = [NSDate dateFromRFC2822:date];
    XCTAssertNil(time);
}

- (void)teststomorrow0 {
    NSDate *date = [NSDate tomorrow0];
    XCTAssertNotNil(date, @"tomorrow0 is not work");
}

@end
