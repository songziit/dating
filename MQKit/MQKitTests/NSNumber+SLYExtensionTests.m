//
//  NSNumber+SLYExtensionTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSNumber+SLYExtension.h"
#import <CoreText/CoreText.h>

@interface NSNumber_SLYExtensionTests : XCTestCase
@property(strong, nonatomic) NSNumber *number;

@end

@implementation NSNumber_SLYExtensionTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.number = [NSNumber numberWithInteger:99];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testsattributeStringAtPriceForFont {
    UIFont *font = [UIFont boldSystemFontOfSize:20];
    [self.number attributeStringAtPriceForFont:font];
}
- (void)testssimplePrice {
    NSString *string = [self.number simplePrice];
    XCTAssert(string != nil, @"simplePrice not work ");
}
- (void)testsattributeStringAtPriceInGoodsList {
    NSString *string = [self.number simplePrice];
    UIFont *font = [UIFont boldSystemFontOfSize:20];
    [self.number attributeStringAtPriceInGoodsList:string AtFont:font];
}
- (void)testsattributeStringAtPrice {
    UIFont *font = [UIFont boldSystemFontOfSize:20];
    [self.number attributeStringAtPriceForFont:font];
}

@end




