//
//  NSString+EncryptTests.m
//  YouJiaGou
//
//  Created by Edison on 15/11/26.
//  Copyright © 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "NSString+Encrypt.h"
#import "NSData+Encrypt.h"
#import "NSData+Base64.h"

@interface NSString_EncryptTests : XCTestCase

@end

@implementation NSString_EncryptTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
- (void)testsencryptedWithAESUsingKey {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FeedbackType" ofType:@"json"];
    NSData *jdata = [[NSData alloc] initWithContentsOfFile:path];
    NSString *string = [jdata base64EncodedStringWithWrapWidth:4];
    [string encryptedWith3DESUsingKey:string andIV:jdata];
}
- (void)testsdecryptedWithAESUsingKey {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FeedbackType" ofType:@"json"];
    NSData *jdata = [[NSData alloc] initWithContentsOfFile:path];
    NSString *string = [jdata base64EncodedStringWithWrapWidth:4];
    [string decryptedWith3DESUsingKey:string andIV:jdata];
}

@end
