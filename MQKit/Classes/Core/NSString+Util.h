//
//  NSString+Util.h
//  VVM
//
//  Created by shulianyong on 12/03/30.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    PhoneOrEmailVerifyTypeEmpty = 0,   /**<为空 */
    PhoneOrEmailVerifyTypePhone = 1,   /**< 电话号码 */
    PhoneOrEmailVerifyTypeEmail = 2,   /**< 邮件箱 */
    PhoneOrEmailVerifyTypeNoPhone = 3, /**< 不是正常的电话号码 */
    PhoneOrEmailVerifyTypeNoEmail = 4, /**< 不是正常的邮件箱 */
} PhoneOrEmailVerifyType;

@interface NSString (Util)

/**
 *  @brief 是否为空
 *
 *  @param aString 需要判断的字符串
 *
 *  @return 是否为空
 */
+ (BOOL)isEmpty:(nullable NSString *)aString;

/**
 *  @brief URL编码
 *
 *  @param aSource 需要编码的字符串
 *
 *  @return 编码后的字符串
 */
+ (nullable NSString *)urlEncode:(nullable NSString *)aSource;

/**
 *  @brief URL解码
 *
 *  @param aSource 需要解码字符串
 *
 *  @return 解决后的字符串
 */
+ (nullable NSString *)urlDecode:(nullable NSString *)aSource;

/**
 *  @brief 构建HTTP URL
 *
 *  @return URL
 */
- (nullable NSURL *)URLForHTTP;
/**
 *  @brief 将16进制字符串转换成uint型
 *
 *  @param aHex 16进制字符串
 *
 *  @return 转换后的uint值
 */
+ (unsigned int)convertHexString:(nullable NSString *)aHex;

/**
 *  @brief base64编码
 *
 *  @param aData 需要编码数据
 *
 *  @return 编码后的值
 */
+ (nullable NSString *)base64Encode:(nullable NSData *)aData;

/**
 *  @brief base64编码
 *
 *  @param aString   需要编码数据
 *  @param aEncoding NSStringEncoding值
 *
 *  @return 编码后的值
 */
+ (nullable NSString *)base64Encode:(nullable NSString *)aString encoding:(NSStringEncoding)aEncoding;

/**
 *  @brief base64解码
 *
 *  @param aBase64   需要解码的数据
 *  @param aEncoding NSStringEncoding值
 *
 *  @return 解码后的值
 */
+ (nullable NSString *)base64Decode:(nullable NSString *)aBase64 encoding:(NSStringEncoding)aEncoding;

/**
 *  @brief base64解码
 *
 *  @param aBase64 需要解码的数据
 *
 *  @return 解码后的值
 */
+ (nullable NSString *)base64Decode:(nullable NSString *)aBase64;

/**
 *  @brief 字符串长度
 *
 *  @param aString 需要计算的字符串
 *
 *  @return 字符串长度
 */
+ (NSUInteger)length:(nullable NSString *)aString;

/**
 *  @brief 是否只是Ascii
 *
 *  @param aString 判断的字符串
 *
 *  @return 是否只是Ascii
 */
+ (BOOL)isAsciiOnly:(nullable NSString *)aString;

/**
 *  @brief 正则表达式
 *
 *  @param aFormat      表达式
 *  @param aValueString 判断的字符串
 *
 *  @return 是否满足正则表达式
 */
+ (BOOL)regexWithFormat:(nullable NSString *)aFormat ValueString:(nullable NSString *)aValueString;

/**
 *  @brief MD5计算
 *
 *  @param aValue 需要MD5的字符串
 *
 *  @return md5值
 */
+ (nullable NSString *)MD5:(nullable NSString *)aValue;

/**
 *  @brief 获取新的UUID
 *
 *  @return 新uuid
 */
+ (nullable NSString *)UUID;

/**
 *  @brief 电话号码转换
 *
 *  @param aValue 需要转换的字符串
 *
 *  @return 转换后的11位的电话号码
 */
+ (nullable NSString *)phoneNumFormat:(nullable NSString *)aValue;

/**
 *  @brief 苹果电话号码格式
 *
 *  @param aValue 需要转换的手机号
 *
 *  @return iPhone电话号码
 */
+ (nullable NSString *)iPhoneNumberFormat:(nullable NSString *)aValue;

/**
 *   @brief  加密的电话号码格式
 *
 *   @param phone 电话号码
 *
 *   @return 解决完的加密电话号码
 */
+ (nullable NSString *)secretPhoneNumberFormart:(nullable NSString *)phone;

/**
 *  @brief 获取一个字符串内的，所有邮件地址
 *
 *  @param aText 需要提取的字符串
 *
 *  @return 提取结果
 */
+ (nullable NSArray *)emails:(nullable NSString *)aText;

/**
 *  @brief 过滤正则表达式的数据
 *
 *  @param aText   需要过滤的字符串
 *  @param aFormat 正则表达式
 *
 *  @return 结果
 */
+ (nullable NSArray *)filteString:(nullable NSString *)aText forFormat:(nullable NSString *)aFormat;

/**
 *  @brief 将json的下划线key,转换成驼峰命名
 *
 *  @param aText 需要转换的数据
 *
 *  @return 转换之后的数据
 */
+ (nullable NSString *)convertHTTPJsonToModel:(nullable NSString *)aText;

/**
 *  @brief 将驼峰命名，转换成下划线全名
 *
 *  @param aText 需要转换的数据
 *
 *  @return 转换之后的数据
 */
+ (nullable NSString *)convertModelToHttpJson:(nullable NSString *)aText;

/**
 *  @brief 让字符串不为空
 *
 *  @param aValue 需要处理的字符串
 *
 *  @return 不为空的字符串
 */
+ (nullable NSString *)valueNotNull:(nullable NSString *)aValue;

/**
 *  @brief 获取资源名的字符串
 *
 *  @param aName 资源名字
 *  @param aType 资源类型
 *
 *  @return 不为空的字符串
 */
+ (NSString *)stringWithName:(NSString *)aName ofType:(NSString*)aType;


#pragma mark------------------------- 搜索与索引符

/**
 *  @brief 对比字符串大小
 *
 *  @param aBaseString  当前字符串
 *  @param aMarchString 对比字符串
 *
 *  @return 是否当前字符串大
 */
+ (BOOL)marchStringForSearch:(nullable NSString *)aBaseString withMarch:(nullable NSString *)aMarchString;

/**
 *  @brief 中文字符串的首字母
 *
 *  @param aString 字文字符串
 *
 *  @return 首字母
 */
+ (nullable NSString *)firstLetter:(nullable NSString *)aString;

/**
 *  @brief 所有索引字符串
 *
 *  @return 索引字符串
 */
+ (nullable NSArray *)indexLetters;

#pragma mark------------------------ 字符串验证
/**
 *  @brief 判断输入数据是否是邮箱
 *
 *  @param aEmailAddress 判断字符串
 *
 *  @return 是否是邮箱
 */
+ (BOOL)isEmail:(nullable NSString *)aEmailAddress;


/**
 是否是电话号码

 @param phone 电话号码
 @return 是否是电话号码
 */
+ (BOOL)isPhone:(nullable NSString*)phone;

/**
 *  @brief 验证电话号码是否正确
 *
 *  @return 是否正确
 */
+ (PhoneOrEmailVerifyType)verifyPhoneOrEmail:(nullable NSString *)aString;

#pragma mark 文件路径管理

/**
 *  @brief Document文件夹详细路径
 *
 *  @return Document文件夹详细路径
 */
+ (nullable NSString *)documentFolderPath;

/**
 *  @brief Document文件夹中的一个文件夹，不存在就添加
 *
 *  @param aFolderName 文件夹名
 *
 *  @return 路径
 */
+ (nullable NSString *)pathInDocument:(nullable NSString *)aFolderName;

/**
 *  @brief cache文件夹路径
 *
 *  @return cache文件夹路径
 */
+ (nullable NSString *)cacheFolderPath;

/**
 *  @brief cache文件夹中的一个文件夹，不存在就添加
 *
 *  @return 文件夹路径
 */
+ (nullable NSString *)folderPathInCache:(nullable NSString *)aFolderName;

/**
 *  @brief 获取文件名
 *
 *  @param aPath 文件路径
 *
 *  @return 文件名
 */
+ (nullable NSString *)fileNameInPath:(nullable NSString *)aPath;

/**
 *  @brief 去前后空格
 *
 *  @return 去空格后的值
 */
- (nullable NSString *)trim;

/**
 *  @brief 判断是否是数字字符串
 *
 *  @param aString 需要判断的字符串
 *
 *  @return 是否是数字
 */
+ (BOOL)isNumberString:(nullable NSString *)aString;


/**
 数字字符串

 @param aString 传入字符串
 @return 数字字符串
 */
+ (NSString*)NumberString:(nullable NSString *)aString;

/**
 数字字符串和特殊符号

 @param aString 传入字符串
 @param aSpecialChar 特殊符号
 @return 数字字符串和特殊符号
 */
+ (NSString*)NumberString:(nullable NSString *)aString andSpecialChar:(char)aSpecialChar;

/**
 *  @brief URL的参数
 *
 *  @return 参数值
 */
- (nullable NSDictionary *)urlParameters;

/**
 *  @brief UDID
 *
 *  @return 唯一标识
 */
+ (nullable NSString *)UDID;

/**
 *  @brief 4位数随机数
 *
 *  @return 随机数值
 */
//+ (NSString *)Rand4;

#pragma mark - 样式
- (nullable NSMutableAttributedString *)attributedStringAtLineSpace:(float)sapce;

- (nullable NSMutableAttributedString *)attributedStringAtLeftInset:(float)inset;

+ (nullable NSString *)jsonDecode:(nullable NSString *)jsonString;

#pragma mark - format
+ (nullable NSString *)stringWithInteger:(NSInteger)integer;

- (NSString*)money;

-(NSUInteger)unsignedLongLongValue;

@end
