//
//  CardNumberDispose.h
//  Pods
//
//  Created by mazo on 6/4/15.
//
//

#import <Foundation/Foundation.h>

@interface CardNumberDispose : NSObject


/**
 *  @brief 返回银行卡最后N位
 *
 *  @param orginalString 原始字符串
 *
 *  @param count 需要返回的尾数
 *
 *  @return 首字母
 */
+(NSString*)returnCardLastNumber:(NSString*)orginalString withCount:(NSInteger)count;

@end
