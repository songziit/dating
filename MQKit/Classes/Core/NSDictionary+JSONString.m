//
//  NSDictionary+JSONString.m
//  iOS-Categories (https://github.com/shaojiankui/iOS-Categories)
//
//  Created by Jakey on 15/4/25.
//  Copyright (c) 2015年 www.skyfox.org. All rights reserved.
//

#import "NSDictionary+JSONString.h"

@implementation NSDictionary (JSONString)
-(NSString *)JSONString{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if (jsonData == nil) {
#ifdef DEBUG
        NSLog(@"fail to get JSON from dictionary: %@, error: %@", self, error);
#endif
        return nil;
    }
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}


+ (NSDictionary*)sortFromDictinary:(NSDictionary *)fromValue
{
    NSMutableDictionary *retDic = [NSMutableDictionary dictionary];
    NSArray *keys = [fromValue allKeys];
    keys = [keys sortedArrayUsingSelector:@selector(compare:)];
    for (NSString *temp in keys) {
        id value = fromValue[temp];
        if ([value isKindOfClass:[NSDictionary class]]) {
            value = [self sortFromDictinary:value];
        } else if ([value isKindOfClass:NSNumber.class]) {
            value = ((NSNumber *)value).stringValue;
        }
        retDic[temp] = value;
    }
    return retDic;
}

@end
