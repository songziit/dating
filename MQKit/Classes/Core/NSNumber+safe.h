//
//  NSNumber+safe.h
//  ttttt
//
//  Created by cailu on 15/10/30.
//  Copyright © 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (safe)
- (BOOL)safeIsEqualToNumber:(NSNumber *)number;
- (NSComparisonResult)safeCompare:(NSNumber *)otherNumber;
@end
