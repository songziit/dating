//
//  NSString+Util.m
//  VVM
//
//  Created by shulianyong on 12/03/30.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import "KeychainItemWrapper.h"
#import "NSData+Util.h"
#import "NSString+Util.h"
#import "spelling.h"
#import "MQLogger.h"

@interface NSString (PrivateDelegateHandling)
+ (BOOL)isSigleByte:(NSString *)character;
@end

@implementation NSString (Util)

#pragma mark - Private Methods

+ (BOOL)isSigleByte:(NSString *)character {
    NSString *const singlePattern = @"[\\x20-\\x7E\\xA1-\\xDF]";  // Multi=Byte character's include 8bit-Kana, accents.
    NSRange match = [character rangeOfString:singlePattern options:NSRegularExpressionSearch];
    if (match.location != NSNotFound) {
        return YES;
    }
    return NO;
}

#pragma mark - Public Methods

+ (BOOL)isEmpty:(NSString *)aString {
    BOOL ret = NO;
    if ((aString == nil) || ([[aString trim] length] == 0) || [aString isKindOfClass:[NSNull class]])
        ret = YES;
    return ret;
}

+ (NSString *)urlEncode:(NSString *)aSource {
    CFStringRef result = CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)aSource, NULL, CFSTR(";,/?:@&=+$#"), kCFStringEncodingUTF8);
    NSString *resultString = (__bridge NSString *)(result);
    CFRelease(result);
    return resultString;
}

+ (NSString *)urlDecode:(NSString *)aSource {
    CFStringRef result = CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (CFStringRef)aSource, CFSTR(""), kCFStringEncodingUTF8);
    NSString *resultString = (__bridge NSString *)result;
    CFRelease(result);
    return resultString;
}

- (NSURL *)URLForHTTP {
    NSString *urlStr = nil;
    if ([self hasPrefix:@"http"]) {
        urlStr = self;
    } else {
        urlStr = [@"http://" stringByAppendingString:self];
    }
    return [NSURL URLWithString:urlStr];
}

+ (unsigned int)convertHexString:(NSString *)aHex {
    NSScanner *scanner = [NSScanner scannerWithString:aHex];
    unsigned int value;
    [scanner scanHexInt:&value];
    return value;
}

static char encodingTable[64] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
    'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
    'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

+ (NSString *)base64Encode:(NSData *)aData {
    unsigned long ixtext = 0;
    unsigned long lentext = [aData length];
    long ctremaining = 0;
    unsigned char inbuf[3], outbuf[4];
    unsigned short i = 0;
    unsigned short charsonline = 0, ctcopy = 4;
    unsigned long ix = 0;
    const unsigned char *bytes = [aData bytes];
    NSMutableString *result = [NSMutableString stringWithCapacity:lentext];

    while (1) {
        ctremaining = lentext - ixtext;
        if (ctremaining <= 0) break;

        for (i = 0; i < 3; i++) {
            ix = ixtext + i;
            if (ix < lentext)
                inbuf[i] = bytes[ix];
            else
                inbuf[i] = 0;
        }

        outbuf[0] = (inbuf[0] & 0xFC) >> 2;
        outbuf[1] = ((inbuf[0] & 0x03) << 4) | ((inbuf[1] & 0xF0) >> 4);
        outbuf[2] = ((inbuf[1] & 0x0F) << 2) | ((inbuf[2] & 0xC0) >> 6);
        outbuf[3] = inbuf[2] & 0x3F;

        switch (ctremaining) {
            case 1:
                ctcopy = 2;
                break;
            case 2:
                ctcopy = 3;
                break;
            default:
                ctcopy = 4;
                break;
        }

        for (i = 0; i < ctcopy; i++) [result appendFormat:@"%c", encodingTable[outbuf[i]]];

        for (i = ctcopy; i < 4; i++) [result appendString:@"="];

        ixtext += 3;
        charsonline += 4;
    }
    return [NSString stringWithString:result];
}

+ (NSString *)base64Encode:(NSString *)aString encoding:(NSStringEncoding)aEncoding {
    return [self base64Encode:[aString dataUsingEncoding:aEncoding]];
}

+ (NSString *)base64Decode:(NSString *)aBase64 {
    return [self base64Decode:aBase64 encoding:NSUTF8StringEncoding];
}

+ (NSString *)base64Decode:(NSString *)aBase64 encoding:(NSStringEncoding)aEncoding {
    NSData *decodeData = [[NSData alloc] initWithBase64EncodedString:aBase64 options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [[NSString alloc] initWithData:decodeData encoding:aEncoding];
}

+ (NSUInteger)length:(NSString *)aString {
    NSUInteger length = 0;
    NSUInteger i;
    for (i = 0; i < [aString length]; ++i) {
        NSString *character = [aString substringWithRange:NSMakeRange(i, 1)];
        ++length;
        if (![self isSigleByte:character]) {
            ++length;
        }
    }
    return length;
}

+ (BOOL)isAsciiOnly:(NSString *)aString {
    NSUInteger i;
    for (i = 0; i < [aString length]; ++i) {
        NSString *character = [aString substringWithRange:NSMakeRange(i, 1)];
        NSString *encoded = [character stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if (!encoded || [encoded length] > 3) return NO;
    }
    return YES;
}

+ (BOOL)regexWithFormat:(NSString *)aFormat ValueString:(NSString *)aValueString {
    NSPredicate *_Predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", aFormat];
    return [_Predicate evaluateWithObject:aValueString];
}

+ (NSString *)MD5:(NSString *)aValue {
    if (aValue == nil) {
        return nil;
    }
    const char *cStr = [aValue UTF8String];

    unsigned char result[CC_MD5_DIGEST_LENGTH];

    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);

    return [NSString

        stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",

                         result[0], result[1],

                         result[2], result[3],

                         result[4], result[5],

                         result[6], result[7],

                         result[8], result[9],

                         result[10], result[11],

                         result[12], result[13],

                         result[14], result[15]

    ];
}

//UUID
+ (NSString *)UUID {
    CFUUIDRef uuidObject = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidStr = (NSString *)CFBridgingRelease(CFUUIDCreateString(kCFAllocatorDefault, uuidObject));
    CFRelease(uuidObject);
    return uuidStr;
}

//将苹果的电话号码格式，转换为正常的格式
+ (NSString *)phoneNumFormat:(NSString *)aValue;
{
    if(aValue==nil){
        aValue = @"";
    }
    NSMutableString *tmp = [[NSMutableString alloc] init];
    for (int j = 0; j < [aValue length]; j++) {
        if (([aValue characterAtIndex:j] >= '0' && [aValue characterAtIndex:j] <= '9') || [aValue characterAtIndex:j] == '+') {
            //					NSString *str = [tmpText substringWithRange:NSMakeRange(j,1)];
            [tmp appendFormat:@"%c", [aValue characterAtIndex:j]];
        }
    }
    return tmp;
}

+ (NSString *)iPhoneNumberFormat:(NSString *)aValue {
    if ([NSString isEmpty:aValue]) {
        return @"";
    }
    NSString *tenDigitNumber = aValue;
    tenDigitNumber = [tenDigitNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{4})(\\d{4})"
                                                               withString:@"$1-$2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [tenDigitNumber length])];
    return tenDigitNumber;
}

+ (NSString *)secretPhoneNumberFormart:(NSString *)phone {
    if ([NSString isEmpty:phone]) {
        return @"";
    }
    NSString *tenDigitNumber = phone;
    tenDigitNumber = [tenDigitNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{4})(\\d{4})"
                                                               withString:@"$1****$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [tenDigitNumber length])];
    return tenDigitNumber;
}

#define EMAILFORMAT @"(?:\\w+\\.?)*\\w+@(?:\\w+\\.?)*\\w+"

+ (NSArray *)emails:(NSString *)aText {
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:EMAILFORMAT options:NSRegularExpressionAnchorsMatchLines error:&error];
    NSArray *matches = [regex matchesInString:aText options:0 range:NSMakeRange(0, [aText length])];
    NSMutableArray *values = [NSMutableArray array];
    for (NSTextCheckingResult *result in matches) {
        [values addObject:[aText substringWithRange:result.range]];
    }
    return values;
}

+ (NSArray *)filteString:(NSString *)aText forFormat:(NSString *)aFormat {
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:aFormat options:NSRegularExpressionAnchorsMatchLines error:&error];
    NSArray *matches = [regex matchesInString:aText options:0 range:NSMakeRange(0, [aText length])];
    NSMutableArray *values = [NSMutableArray array];
    for (NSTextCheckingResult *result in matches) {
        [values addObject:[aText substringWithRange:result.range]];
    }
    return values;
}

+ (NSString *)convertHTTPJsonToModel:(NSString *)aText {
    NSString *responseString = aText;
    NSArray *list = [NSString filteString:responseString forFormat:@"\"(\\w+)_(\\w+)\"\\s*:"];

    for (NSString *oldKey in list) {
        NSString *newkey = nil;

        NSArray *oldKeyComponse = [oldKey componentsSeparatedByString:@"_"];
        NSMutableArray *newKeyComponse = [NSMutableArray arrayWithCapacity:oldKeyComponse.count];
        for (int i = 0; i < oldKeyComponse.count; i++) {
            NSString *aKey = oldKeyComponse[i];
            if (i > 0) {
                aKey = [aKey capitalizedString];
            }
            [newKeyComponse addObject:aKey];
        }
        newkey = [newKeyComponse componentsJoinedByString:@""];
        responseString = [responseString stringByReplacingOccurrencesOfString:oldKey withString:newkey];
    }
    return responseString;
}

+ (NSString *)convertModelToHttpJson:(NSString *)aText {
    NSString *requestString = aText;
    NSArray *list = [NSString filteString:requestString forFormat:@"\"(\\w+)([A-Z]+)(\\w*)\"\\s*:"];

    for (NSString *oldKey in list) {
        NSString *newkey = oldKey;
        NSArray *uppercaseStrings = [NSString filteString:oldKey forFormat:@"[A-Z]+[a-z]+"];
        for (int i = 0; i < uppercaseStrings.count; i++) {
            NSString *aKey = uppercaseStrings[i];
            newkey = [newkey stringByReplacingOccurrencesOfString:aKey
                                                       withString:[NSString stringWithFormat:@"_%@", aKey.lowercaseString]];
        }

        if ([newkey hasPrefix:@"_"]) {
            newkey = [newkey substringFromIndex:1];
        }
        requestString = [requestString stringByReplacingOccurrencesOfString:oldKey withString:newkey];
    }
    return requestString;
}

+ (NSString *)valueNotNull:(NSString *)aValue {
    NSString *temp = nil;
    temp = [NSString isEmpty:aValue] ? @"" : aValue;
    return temp;
}

+ (NSString *)stringWithName:(NSString *)aName ofType:(NSString*)aType{
    NSString *contentPath = [[NSBundle mainBundle] pathForResource:aName ofType:aType];
    NSString *result = @"";
    if (contentPath) {
        result  = [NSString stringWithContentsOfFile:contentPath encoding:NSUTF8StringEncoding error:nil];
    }
    return result;
}

#pragma mark------------------------- 搜索与索引符

+ (BOOL)marchStringForSearch:(NSString *)aBaseString withMarch:(NSString *)aMarchString {
    if (aBaseString == nil || aMarchString == nil) {
        return NO;
    }
    NSComparisonResult result = [aBaseString compare:aMarchString options:NSCaseInsensitiveSearch range:NSMakeRange(0, aMarchString.length)];
    if (result == NSOrderedSame) {
        return YES;
    }
    NSRange range = [aBaseString rangeOfString:aMarchString];
    if (range.length > 0) {
        return YES;
    }
    return NO;
}

+ (NSString *)firstLetter:(NSString *)aString {
    NSString *firstLetter = @"#";
    if (![NSString isEmpty:aString]) {
        char firstLetterChar = pinyinFirstLetter([aString characterAtIndex:0]);
        if (firstLetterChar >= 'a' && firstLetterChar <= 'z') {
            firstLetterChar = firstLetterChar - 32;
        }
        if (firstLetterChar >= 'A' && firstLetterChar <= 'Z') {
            firstLetter = [NSString stringWithFormat:@"%c", firstLetterChar];
        }
    }
    return firstLetter;
}

+ (NSArray *)indexLetters {
    static NSMutableArray *indexTitleArray = nil;
    if (!indexTitleArray) {
        indexTitleArray = [[NSMutableArray alloc] init];
        //        [indexTitleArray addObject:UITableViewIndexSearch];
        for (char c = 'A'; c <= 'Z'; c++) {
            [indexTitleArray addObject:[NSString stringWithFormat:@"%c", c]];
        }
        [indexTitleArray addObject:@"#"];
    }
    return indexTitleArray;
}

#pragma mark------------------------ 字符串验证

/**
 *  @brief 判断输入数据是否是邮箱
 *
 *  @param aEmailAddress 判断字符串
 *
 *  @return 是否是邮箱
 */
+ (BOOL)isEmail:(NSString *)aEmailAddress {
    BOOL ret = YES;

    NSRange rangeEmail = [aEmailAddress rangeOfString:@"@"];
    if (rangeEmail.location == NSIntegerMax) {
        ret = NO;
    }

    return ret;
}

+ (BOOL)isPhone:(NSString *)phone{
    BOOL ret = NO;
    if (phone) {
        static NSString *phoneFormat = @"(13[0-9]|14[57]|15[012356789]|17[0-9]|18[0-9]|19[0-9])[0-9]{8}";
        ret = [NSString regexWithFormat:phoneFormat ValueString:phone];
    }
    return ret;
}

/**
 *  @brief 验证字符串是是电话号码还是邮件箱
 *
 *  @param aString 输入字符串
 *
 *  @return 结果
 */
+ (PhoneOrEmailVerifyType)verifyPhoneOrEmail:(NSString *)aString {
    PhoneOrEmailVerifyType ret = PhoneOrEmailVerifyTypeEmpty;
    if ([NSString isEmpty:aString]) {
        ret = PhoneOrEmailVerifyTypeEmpty;
    } else {
        static NSString *phoneFormat = @"[0-9]{8,20}";  //@"(13[0-9]|15[012356789]|18[0-9]|14[57])[0-9]{8}";
        static NSString *emailFormat = @"[a-zA-Z0-9_\\.-]+@[a-zA-Z0-9_-]+\\.[a-zA-Z0-9_-]+";

        if ([NSString isEmail:aString]) {
            if ([NSString regexWithFormat:emailFormat ValueString:aString])
                ret = PhoneOrEmailVerifyTypeEmail;
            else
                ret = PhoneOrEmailVerifyTypeNoEmail;
        } else {
            if ([NSString regexWithFormat:phoneFormat ValueString:aString])
                ret = PhoneOrEmailVerifyTypePhone;
            else
                ret = PhoneOrEmailVerifyTypeNoPhone;
        }
    }
    return ret;
}

#pragma mark 文件路径管理

+ (NSString *)documentFolderPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    return path;
}

+ (NSString *)pathInDocument:(NSString *)aFolderName {
    NSString *folderPath = [NSString stringWithFormat:@"%@/%@", [self documentFolderPath], aFolderName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        NSError *err = nil;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:folderPath
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:&err]) {
            ERROR(@"Error creating logsDirectory: %@", err);
        }
    }
    return folderPath;
}

+ (NSString *)cacheFolderPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    return path;
}

+ (NSString *)folderPathInCache:(NSString *)aFolderName {
    NSString *folderPath = [[self cacheFolderPath] stringByAppendingPathComponent:aFolderName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:folderPath]) {
        NSError *err = nil;
        if (![[NSFileManager defaultManager] createDirectoryAtPath:folderPath
                                       withIntermediateDirectories:YES
                                                        attributes:nil
                                                             error:&err]) {
            ERROR(@"Error creating imageFolderInCache: %@", err);
        }
    }
    return folderPath;
}

//获取文件名
+ (NSString *)fileNameInPath:(NSString *)aPath {
    NSString *filename = [aPath lastPathComponent];
    filename = [filename stringByDeletingPathExtension];
    return filename;
}

- (NSString *)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (BOOL)isNumberString:(NSString *)aString {
    const char *strTmp = [aString cStringUsingEncoding:NSUTF8StringEncoding];
    for (NSUInteger i = 0; i < aString.length; ++i) {
        if (strTmp[i] < '0' || strTmp[i] > '9') {
            return NO;
        }
    }

    return YES;
}

+ (NSString*)NumberString:(NSString *)aValue{
    if(aValue==nil){
        aValue = @"";
    }
    NSMutableString *tmp = [[NSMutableString alloc] init];
    for (int j = 0; j < [aValue length]; j++) {
        if ([aValue characterAtIndex:j] >= '0' && [aValue characterAtIndex:j] <= '9') {
            [tmp appendFormat:@"%c", [aValue characterAtIndex:j]];
        }
    }
    return tmp;
}

+ (NSString*)NumberString:(NSString *)aValue andSpecialChar:(char)aSpecialChar{
    if(aValue==nil){
        aValue = @"";
    }
    NSMutableString *tmp = [[NSMutableString alloc] init];
    for (int j = 0; j < [aValue length]; j++) {
        if (([aValue characterAtIndex:j] >= '0' && [aValue characterAtIndex:j] <= '9')
            || [aValue characterAtIndex:j] == aSpecialChar) {
            [tmp appendFormat:@"%c", [aValue characterAtIndex:j]];
        }
    }
    return tmp;
}

- (NSDictionary *)urlParameters {
    NSCharacterSet *delimiterSet = [NSCharacterSet characterSetWithCharactersInString:@"&;"];
    NSMutableDictionary *pairs = [NSMutableDictionary dictionary];
    NSScanner *scanner = [[NSScanner alloc] initWithString:self];
    while (![scanner isAtEnd]) {
        NSString *pairString = nil;
        [scanner scanUpToCharactersFromSet:delimiterSet intoString:&pairString];
        [scanner scanCharactersFromSet:delimiterSet intoString:NULL];
        NSArray *kvPair = [pairString componentsSeparatedByString:@"="];
        if (kvPair.count == 2) {
            NSString *key = kvPair[0];
            NSString *value = kvPair[1];
            [pairs setObject:value forKey:key];
        }
    }
    return pairs;
}

#pragma mark--------- udid
static NSString *const kAccessGroup = @"SharedAccessGroup";

+ (NSString *)UDID {
    static NSString *udid = nil;
    if (![NSString isEmpty:udid]) {
        return udid;
    }
    static KeychainItemWrapper *keyChain = nil;
    if (keyChain == nil) {
        NSString *accessGroup = [[NSBundle mainBundle] objectForInfoDictionaryKey:kAccessGroup];
        NSAssert(![NSString isEmpty:accessGroup], @"SharedAccessGroup is nil.");
        keyChain = [[KeychainItemWrapper alloc] initWithIdentifier:@"UDID" accessGroup:accessGroup];
    }

    NSString *temp = [keyChain objectForKey:(__bridge id)kSecValueData];
    if ([NSString isEmpty:temp]) {
        udid = [UIDevice currentDevice].identifierForVendor.UUIDString;
        [keyChain setObject:udid forKey:(__bridge id)kSecValueData];
    } else {
        udid = temp;
    }
    return udid;
}

/**
 *  @brief 4位数随机数
 *
 *  @return 随机数值
 */
//+ (NSString *)Rand4 {
//    //    int n = 1000;
//    //    int m = 9999;
//    //    int ret = n + rand() % (m - n + 1);
//    //    return [NSString stringWithFormat:@"%d",ret];
//    return [@(1111) stringValue];
//}

#pragma mark - 样式
- (NSMutableAttributedString *)attributedStringAtLineSpace:(float)sapce {
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:self];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineSpacing = sapce;
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, self.length)];
    return attributeStr;
}

- (NSMutableAttributedString *)attributedStringAtLeftInset:(float)inset {
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:self];
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.firstLineHeadIndent = inset;
    [attributeStr addAttribute:NSParagraphStyleAttributeName value:style range:NSMakeRange(0, self.length)];
    return attributeStr;
}

+ (NSString *)jsonDecode:(NSString *)jsonString {
    NSString *ret = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    ret = [ret stringByReplacingOccurrencesOfString:@" *\\{ *"
                                         withString:@"{"
                                            options:NSRegularExpressionSearch
                                              range:NSMakeRange(0, [ret length])];
    ret = [ret stringByReplacingOccurrencesOfString:@" *\\} *"
                                         withString:@"}"
                                            options:NSRegularExpressionSearch
                                              range:NSMakeRange(0, [ret length])];
    ret = [ret stringByReplacingOccurrencesOfString:@" *: *"
                                         withString:@":"
                                            options:NSRegularExpressionSearch
                                              range:NSMakeRange(0, [ret length])];
    ret = [ret stringByReplacingOccurrencesOfString:@" *, *"
                                         withString:@","
                                            options:NSRegularExpressionSearch
                                              range:NSMakeRange(0, [ret length])];
    return ret;
}

#pragma mark - format
static NSString *const formatLd = @"%ld";

+ (NSString *)stringWithInteger:(NSInteger)integer {
    return [NSString stringWithFormat:formatLd, (long)integer];
}

- (NSString*)money{
    return [NSString stringWithFormat:@"%@元",self];
}

-(NSUInteger)unsignedLongLongValue {
    return [self integerValue];
}

@end
