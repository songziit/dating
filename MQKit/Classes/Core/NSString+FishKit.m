//
//  NSString+FishKit.m
//  Common
//
//  Created by QFish on 8/10/14.
//  Copyright (c) 2014 iNoknok. All rights reserved.
//

#import "NSString+FishKit.h"

@implementation NSString (FishKit)

- (BOOL)isEmail
{
    return NSNotFound != [self rangeOfString:@"@"].location;
}

@end
