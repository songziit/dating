//
//  NSNumber+safe.m
//  ttttt
//
//  Created by cailu on 15/10/30.
//  Copyright © 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "NSNumber+safe.h"

@implementation NSNumber (safe)
- (BOOL)safeIsEqualToNumber:(NSNumber *)number {
    if (number == nil) {
        return NO;
    } else {
        return [self isEqualToNumber:number];
    }
}

- (NSComparisonResult)safeCompare:(NSNumber *)otherNumber {
    if (otherNumber == nil) {
        return NSOrderedDescending;
    } else {
        return [self compare:otherNumber];
    }
}
@end
