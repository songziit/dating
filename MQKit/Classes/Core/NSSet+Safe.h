//
//  NSSet+Safe.h
//  Pods
//
//  Created by cailu on 15/10/30.
//
//

#import <Foundation/Foundation.h>

@interface NSSet (Safe)
@end

@interface NSMutableSet (Safe)
- (void)addObj:(id)i;
@end