//
//  NSError+SLY.h
//  SLYKit
//
//  Created by neil on 2017/3/1.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (SLY)

+ (NSError *)errorWithTitle:(NSString *)title reason:(NSString *)reason;

+ (NSError *)errorWithTitle:(NSString *)title
                     reason:(NSString *)reason
                       code:(NSInteger)code;

/**
 *  @brief 判断是网络错误
 *
 *  @return YES
 */
- (BOOL)isNetworkError;

@end
