//
//  MQKit.h
//  Pods
//
//  Created by cailu on 15/5/18.
//
//

#ifndef Pods_MQKit_h
#define Pods_MQKit_h

#import "MultiLanguageUtil.h"
#import "NSArray+Util.h"
#import "NSData+Util.h"
#import "NSDate+Util.h"
#import "NSObject+Reflect.h"
#import "NSString+Util.h"
#import "UIButton+ConfigTitle.h"
#import "UIImage+ImageEffects.h"
#import "UIImage+Util.h"
#import "UIImageViewAligned.h"
#import "UIView+SubviewLayout.h"

#import "NSNumber+SLYExtension.h"
#import "UIView+SLYExtension.h"

#import "FishKitMacros.h"

#import "NSArray+FishKit.h"

#import "NSString+FishKit.h"
#import "NSError+SLY.h"

#import "UIColor+PXColors.h"

#import "UIView+Data.h"
#import "UIView+Helper.h"
#import "UIView+Nib.h"

#import "UIScrollView+ReachEnds.h"

#import "UIViewController+Data.h"
#import "UIViewController+Nib.h"
#import "UIViewController+TransparentUINavigationbar.h"

#import "NSArray+SafeAccess.h"
#import "NSDictionary+SafeAccess.h"
#import "NSNumber+safe.h"
#import "NSObject+NSCoder.h"
#import "NSObject+Swizzle.h"
#import "NSSet+Safe.h"

/**
 *  @brief RGB值
 */
#define RGBColor(aRed, aGreen, aBlue) [UIColor colorWithRed:(aRed / 255.0f) green:(aGreen / 255.0f) blue:(aBlue / 255.0f) alpha:1]

#endif
