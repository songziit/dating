//
//  NSSet+Safe.m
//  Pods
//
//  Created by cailu on 15/10/30.
//
//

#import "NSSet+Safe.h"

@implementation NSSet (Safe)

@end

@implementation NSMutableSet (Safe)
- (void)addObj:(id)i {
    if (i != nil) {
        [self addObject:i];
    };
}
@end