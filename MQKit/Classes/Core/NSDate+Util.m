//
//  NSDate+Util.m
//  CommonLayer
//
//  Created by shulianyong on 12/03/30.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MultiLanguageUtil.h"
#import "NSDate+Util.h"

@interface NSDate (PrivateDelegateHandling)
+ (NSDateFormatter *)rfc2822Formatter;
@end

@implementation NSDate (Util)

+ (NSDateFormatter *)rfc2822Formatter {
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        NSLocale *enUS = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
        [formatter setLocale:enUS];
        [formatter setDateFormat:@"EEE, dd MMM yyyy HH:mm:ss ZZ"];
    }
    return formatter;
}

+ (NSDate *)dateFromRFC2822:(NSString *)aRFC2822 {
    NSDateFormatter *formatter = [self rfc2822Formatter];
    return [formatter dateFromString:aRFC2822];
}

+ (NSString *)dateToRFC2822:(NSDate *)aDate {
    NSDateFormatter *formatter = [self rfc2822Formatter];
    return [formatter stringFromDate:aDate];
}

+ (NSDate *)dateWithString:(nonnull NSString *)dateString fromFormat:(nonnull NSString *)aFormat {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *enUS = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:enUS];
    [formatter setDateFormat:aFormat];

    NSDate *date = [formatter dateFromString:dateString];

    return date;
}

+ (NSDate *)tomorrow0 {
    NSDate *tomorrow = [self tomorrow];

    NSString *format = @"yyyy/MM/dd";

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *enUS = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [formatter setLocale:enUS];
    [formatter setDateFormat:format];

    NSString *tomorrow0Value = [tomorrow descriptionAsFormat:format];
    NSDate *tomorrow0 = [formatter dateFromString:tomorrow0Value];

    return tomorrow0;
}

+ (NSDate *)tomorrow {
    NSTimeInterval secondsPerDay = 24 * 60 * 60;
    NSDate *tomorrow = [NSDate dateWithTimeIntervalSinceNow:secondsPerDay];
    return tomorrow;
}

#pragma mark
#pragma mark---------------日期格式显示
- (NSString *)descriptionAsFormat:(NSString *)aFormat {
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:aFormat];
    return [format stringFromDate:self];
}

- (NSString *)descriptionAsFormat:(NSString *)aFormat withTimeZone:(NSTimeZone *)aZone {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:aFormat];
    [formatter setTimeZone:aZone];
    NSString *ret = [formatter stringFromDate:self];
    return ret;
}

- (NSString *)descriptionLocalAsFormat:(NSString *)aFormat {
    return [self descriptionAsFormat:aFormat withTimeZone:[NSTimeZone localTimeZone]];
}

- (NSString *)timeString {
    return [self descriptionAsFormat:@"HH:mm:ss" withTimeZone:[NSTimeZone localTimeZone]];
}

- (NSString *)dateString {
    return [self descriptionAsFormat:@"yyyy-MM-dd" withTimeZone:[NSTimeZone localTimeZone]];
}

- (NSString *)displayDate {
    return [self descriptionAsFormat:@"yyyy-MM-dd HH:mm:ss"];
}

/**
 *  @brief 按天数来展示时间，如果是今天，就只展示时间，昨天则展示昨天，其他都按时间格式展示
 *
 *  @return 天格式的展示
 */
- (NSString *)descriptionToDataFormat {
    return [self descriptionToDataFormat:YES];
}

- (NSString *)descriptionToDataFormat:(BOOL)needTime {
    NSDateComponents *datetime = [NSDate fromDate:self.timeIntervalSince1970
                                           toDate:[NSDate tomorrow0].timeIntervalSince1970];
    NSString *timeFormat = nil;
    if (datetime.day == 0) {
        timeFormat = @"hh.mm";
        //        dateDisplay = [aChat.createdAt descriptionLocalAsFormat:@"hh.mm"];
    } else if (datetime.day == 1) {
        timeFormat = [NSString stringWithFormat:@"%@ - hh.mm", LOCALIZATION(@"Yesterday")];
        //        dateDisplay = [aChat.createdAt descriptionLocalAsFormat:@""];
    } else {
        if (needTime) {
            timeFormat = @"yyyy.MM.dd - hh.mm";
        } else {
            timeFormat = @"yyyy.MM.dd";
        }
    }

    return [self descriptionLocalAsFormat:timeFormat];
}

//时间间隔
+ (NSDateComponents *)fromDate:(NSTimeInterval)aFromDate toDate:(NSTimeInterval)aToDate {
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSUInteger unitFlags = NSMonthCalendarUnit | NSDayCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags
                                                fromDate:[NSDate dateWithTimeIntervalSince1970:aFromDate]
                                                  toDate:[NSDate dateWithTimeIntervalSince1970:aToDate]
                                                 options:0];
    return components;
}

+ (NSDateComponents *)dateComponentsFromDate:(NSTimeInterval)aFromDate {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:aFromDate];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSInteger unitFlags = NSYearCalendarUnit |
                          NSMonthCalendarUnit |
                          NSDayCalendarUnit |
                          NSWeekdayCalendarUnit |
                          NSHourCalendarUnit |
                          NSMinuteCalendarUnit |
                          NSSecondCalendarUnit;
    //int week=0;
    NSDateComponents *comps = [calendar components:unitFlags fromDate:date];
    return comps;
}

+ (NSString *)timeIntervalToLocalString:(NSTimeInterval)aTime WithFormat:(NSString *)aFormat {
    NSDate *tempDate = [NSDate dateWithTimeIntervalSince1970:aTime];
    return [tempDate descriptionLocalAsFormat:aFormat];
}

@end
