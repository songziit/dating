//
//  MultiLanguageUtil.m
//  CommonLayer
//
//  Created by 舒联勇 on 14-5-14.
//  Copyright (c) 2014年 iNoknok.com. All rights reserved.
//

#import "MultiLanguageUtil.h"
#import "NSString+Util.h"

static NSString *const kSaveLanguageDefaultKey = @"kSaveLanguageDefaultKey";
static NSString *const LanguageFileName = @"Localizable";

@interface MultiLanguageUtil ()

@property(nonatomic, strong) NSDictionary *dicLanguage;

@end

@implementation MultiLanguageUtil
@synthesize currentLanguage = _currentLanguage;

/**
 *  @brief 多语言工具实例
 *
 *  @return 多语言工具
 */
+ (instancetype)shareInstance {
    static MultiLanguageUtil *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[MultiLanguageUtil alloc] init];
    });

    return instance;
}

+ (void)load {
    [super load];
    [[self shareInstance] loadDictionaryForLanguage];
}

/**
 *  @brief 设置当前语言
 *
 *  @param currentLanguage 当前语言
 */
- (void)setCurrentLanguage:(NSString *)currentLanguage {
    _currentLanguage = currentLanguage;
    [[NSUserDefaults standardUserDefaults] setObject:_currentLanguage forKey:kSaveLanguageDefaultKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self loadDictionaryForLanguage];
}

- (NSString *)currentLanguage {
    if ([NSString isEmpty:_currentLanguage]) {
        NSString *temp = [[NSUserDefaults standardUserDefaults] objectForKey:kSaveLanguageDefaultKey];
        if ([NSString isEmpty:temp]) {
            temp = @"DeviceLanguage";
        }
        _currentLanguage = temp;
    }
    return _currentLanguage;
}

/**
 *  @brief 加载多语言到字典中
 */
- (void)loadDictionaryForLanguage {
    NSURL *urlPath = [[NSBundle bundleForClass:[self class]] URLForResource:LanguageFileName withExtension:@"strings" subdirectory:nil localization:self.currentLanguage];

    if ([[NSFileManager defaultManager] fileExistsAtPath:urlPath.path]) {
        self.dicLanguage = [NSDictionary dictionaryWithContentsOfFile:urlPath.path];
    }
}

- (NSString *)localizedStringForKey:(NSString *)key {
    NSString *localizedString = nil;
    if (self.dicLanguage == nil) {
        localizedString = [[NSBundle mainBundle] localizedStringForKey:(key) value:@"" table:LanguageFileName];
    } else {
        localizedString = self.dicLanguage[key];
        if (localizedString == nil)
            localizedString = key;
    }
    return localizedString;
}

@end
