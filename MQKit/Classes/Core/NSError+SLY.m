//
//  NSError+SLY.m
//  SLYKit
//
//  Created by neil on 2017/3/1.
//  Copyright © 2017年 Sly. All rights reserved.
//

#import "NSError+SLY.h"

static NSString *CustomErrorDomain = @"com.sly.error";
@implementation NSError (SLY)

+ (NSError *)errorWithTitle:(NSString *)title reason:(NSString *)reason {
    return [self errorWithTitle:title reason:reason code:100000];
}

+ (NSError *)errorWithTitle:(NSString *)title
                     reason:(NSString *)reason
                       code:(NSInteger)code
{
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : title ?: @"",
                                NSLocalizedFailureReasonErrorKey : reason ?: @"" };
    return [NSError errorWithDomain:CustomErrorDomain code:code userInfo:userInfo];
}


- (BOOL)isNetworkError {
    return self.code == NSURLErrorNotConnectedToInternet || self.code == NSURLErrorCancelled || self.code == NSURLErrorTimedOut || self.code == NSURLErrorInternationalRoamingOff || self.code == NSURLErrorDataNotAllowed || self.code == NSURLErrorCannotFindHost || self.code == NSURLErrorCannotConnectToHost;
}

@end
