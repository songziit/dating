//
//  NSData+Util.h
//  CommonLayer
//
//  Created by shulianyong on 12/03/30.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (Util)

/**
 *  @brief 判断是否为空
 *
 *  @param aData 需要判断的数据
 *
 *  @return 是否为空
 */
+ (BOOL)isEmpty:(NSData*)aData;
@end
