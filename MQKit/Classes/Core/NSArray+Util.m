//
//  NSArray+Util.m
//  CommonLayer
//
//  Created by shulianyong on 12/03/30.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "NSArray+Util.h"

@implementation NSArray (Util)

+ (BOOL)isArray:(id)obj {
    return obj && [obj isKindOfClass:[NSArray class]];
}

+ (BOOL)isNotArray:(id)obj {
    return ![self isArray:obj];
}

+ (BOOL)isEmpty:(id)obj {
    return ![NSArray isArray:obj] || [((NSArray *)obj)isEmpty];
}

+ (BOOL)isNotEmpty:(id)obj {
    return ![NSArray isEmpty:obj];
}

- (BOOL)isEmpty {
    if (self == nil) return YES;
    return (BOOL)(self.count == 0);
}

- (BOOL)isNotEmpty {
    if (self == nil) return NO;
    return (BOOL)(self.count > 0);
}

+ (NSInteger)count:(id)obj{
    NSArray *list = obj;
    NSInteger count = [NSArray isEmpty:obj]?0:list.count;
    return count;
}

- (NSString *)stringWithCharacter:(NSString *)aCharacter {
    [self componentsJoinedByString:aCharacter];
    NSMutableString *value = nil;
    if (self.count > 0) {
        if ([self[0] isKindOfClass:[NSString class]]) {
            value = [[NSMutableString alloc] initWithString:[self objectAtIndex:0]];
            for (int i = 1; i < self.count; i++) {
                [value appendString:aCharacter];
                [value appendString:[self objectAtIndex:i]];
            }
        }
    }
    return (value) ? value : @"";
}

- (NSArray *)filterUserArray:(NSArray *)userArray ThroughString:(NSString *)conditionString {
    NSMutableArray *siftArray = [NSMutableArray array];
    [siftArray addObjectsFromArray:userArray];

    //搜索过滤对象
    if (conditionString.length > 0) {
        //按钮名称过滤 [cd]既不区分大小写，也不区分发音符号
        NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF.displayName CONTAINS[cd] %@ ", conditionString];
        [siftArray filterUsingPredicate:pre];
    }
    return siftArray;
}

- (NSArray *)selected {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF.selected == %@", @YES];
    NSArray *selected = [self filteredArrayUsingPredicate:pre];
    return selected;
}

- (id)objectAt:(NSUInteger)index {
    if (index < self.count) {
        return self[index];
    } else {
        return nil;
    }
}
@end
