//
//  NSNumber+SLYExtension.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/14.
//  Copyright (c) 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <CoreText/CoreText.h>
#import "NSNumber+SLYExtension.h"

@implementation NSNumber (SLYExtension)
@dynamic price;

- (NSString *)price {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"]];
    formatter.numberStyle = kCFNumberFormatterCurrencyStyle;
    formatter.usesGroupingSeparator = NO;
    
    NSString *string = [formatter stringFromNumber:[NSNumber numberWithFloat:self.floatValue]];
    return string;
}

- (NSString *)simplePrice {
    NSString *string = self.price;
    if ([string hasSuffix:@".00"]) {
        string = [string stringByDeletingPathExtension];
    } else if ([string hasSuffix:@"0"]) {
        string = [string substringToIndex:string.length - 1];
    }
    return string;
}


- (NSString*)simpleStringValue2{
    NSString *string = self.stringValue2;
    if ([string hasSuffix:@".00"]) {
        string = [string stringByDeletingPathExtension];
    }
    return string;    
}

- (NSString*)stringValue2{
    double value = self.doubleValue;
    return [NSString stringWithFormat:@"%.2f",value];
}

- (NSString *)format:(NSString *)aForamt {
    return [NSString stringWithFormat:aForamt, self];
}

- (NSString *)priceForInt {
    NSString *priceForInt = self.price;
    return [priceForInt stringByDeletingPathExtension];
}

- (NSString *)kilometre{
    NSString *distance = self.stringValue2;
    return [NSString stringWithFormat:@"%@公里",distance];
}

- (NSString*)kilogram{
    NSString *kilogram = self.stringValue2;
    return [NSString stringWithFormat:@"%@公斤",kilogram];
}

- (NSAttributedString *)attributeStringAtPriceForFont:(UIFont *)aFont {
    return [self attributeStringAtPrice:self.price AtFont:aFont];
}

- (NSAttributedString *)attributeStringAtPriceIntForFont:(UIFont *)aFont {
    return [self attributeStringAtPrice:self.priceForInt AtFont:aFont];
}

- (NSAttributedString *)attributeStringAtSimplePriceForFont:(UIFont *)aFont {
    return [self attributeStringAtPrice:self.simplePrice AtFont:aFont];
}

//单独处理商品列表界面cell  价格
- (NSAttributedString *)attributeStringAtPriceInGoodsList:(NSString *)price AtFont:(UIFont *)aFont {
    NSMutableAttributedString *attributeStringAtPrice = [[NSMutableAttributedString alloc] initWithString:price];

    NSString *unit = [price substringToIndex:1];
    NSString *cnUnit = @"￥";
    //间距
    long number = 0;
    if ([unit isEqualToString:cnUnit]) {
        CGRect stringRect = [[unit substringToIndex:1] boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                                                    options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                                 attributes:@{ NSFontAttributeName : aFont }
                                                                    context:nil];
        number = -(stringRect.size.width * 2.f / 14.f);
    }
    [attributeStringAtPrice addAttribute:(NSString *)kCTKernAttributeName value:@(number) range:NSMakeRange(0, 1)];

    return attributeStringAtPrice;
}

- (NSAttributedString *)attributeStringAtPrice:(NSString *)price AtFont:(UIFont *)aFont {
    NSMutableAttributedString *attributeStringAtPrice = [[NSMutableAttributedString alloc] initWithString:price];

    NSString *unit = [price substringToIndex:1];
    NSString *cnUnit = @"￥";

    UIFont *unitFont = [UIFont fontWithName:aFont.fontName size:aFont.pointSize * 3 / 4];
    //间距
    long number = 0;
    if ([unit isEqualToString:cnUnit]) {
        CGRect stringRect = [[unit substringToIndex:1] boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)
                                                                    options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                                                 attributes:@{ NSFontAttributeName : aFont }
                                                                    context:nil];
        number = -(stringRect.size.width * 3.f / 14.f);
    }
    [attributeStringAtPrice addAttribute:(NSString *)kCTKernAttributeName value:@(number) range:NSMakeRange(0, 1)];
    [attributeStringAtPrice addAttribute:NSFontAttributeName
                                   value:unitFont
                                   range:NSMakeRange(0, 1)];

    return attributeStringAtPrice;
}

@end
