//
//  NSObject+Reflect.m
//  CommonLayer
//
//  Created by shulianyong on 13-2-20.
//  Copyright (c) 2013年 cdsf. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "NSObject+Reflect.h"

static const char kUIData;
@implementation NSObject (Reflect)
@dynamic data;

- (void)dataDidChange {
}

- (void)dataWillChange {
}

- (void)setData:(id)data {
    [self dataWillChange];
    objc_setAssociatedObject(self, &kUIData, data, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self dataDidChange];
}

- (id)data {
    return objc_getAssociatedObject(self, &kUIData);
}

extern BOOL isNilOrNull(id value) {
    if (!value) return YES;
    if ([value isKindOfClass:[NSNull class]]) return YES;

    return NO;
}

- (instancetype)initWithJsonString:(NSString *)aJsonString {
    NSError *initError = nil;
    id obj = [NSJSONSerialization JSONObjectWithData:[aJsonString dataUsingEncoding:NSUTF8StringEncoding]
                                             options:kNilOptions
                                               error:&initError];
    return [self initWithDictionary:obj];
}

- (instancetype)initWithDictionary:(NSDictionary *)aDicValue {
    if (isNilOrNull(aDicValue)) {
        return nil;
    }
    self = [self init];
    if (self) {
        [self reflectDataFromOtherObject:aDicValue];
    }
    return self;
}

+ (instancetype)createInstance {
    return [[[self class] alloc] init];
}

- (NSArray *)propertyKeys {
    return [self.class propertyKeys];
}

/**
 *  @brief 实体可用于赋值的值
 *
 *  @return 实体可用于赋值的值
 */
+ (NSArray *)propertyKeys {
    Class tempClass = [self class];
    NSMutableArray *keys = [[NSMutableArray alloc] init];
    while (tempClass != [NSObject class]) {
        @autoreleasepool {
            NSArray *tempKeys = [self propertyKeysFromClass:tempClass];
            if (tempKeys.count > 0) {
                [keys addObjectsFromArray:tempKeys];
            }
        }
        tempClass = [tempClass superclass];
    }
    return keys;
}

+ (NSArray *)propertyKeysFromClass:(Class)aClass {
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(aClass, &outCount);
    NSMutableArray *keys = [[NSMutableArray alloc] initWithCapacity:outCount];
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        NSString *propertyName = [[NSString alloc] initWithCString:property_getName(property) encoding:NSUTF8StringEncoding];

        //如果是readonly的，直接不添加
        //https://developer.apple.com/library/ios/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtPropertyIntrospection.html
        NSString *attName = [[NSString alloc] initWithCString:property_getAttributes(property) encoding:NSUTF8StringEncoding];
        NSArray *attList = [attName componentsSeparatedByString:@","];
        BOOL checkSetter = ![attList containsObject:@"R"];
        if (checkSetter) {
            [keys addObject:propertyName];
        }
    }
    free(properties);
    return keys;
}

- (BOOL)reflectDataFromOtherObject:(NSObject *)dataSource {
    BOOL ret = NO;
    if (dataSource && self != dataSource) {
        NSArray *keys = [self propertyKeys];
        for (NSString *key in keys) {
            @autoreleasepool {
                if ([dataSource isKindOfClass:[NSDictionary class]]) {
                    ret = ([dataSource valueForKey:key] == nil) ? NO : YES;
                } else {
                    ret = [dataSource respondsToSelector:NSSelectorFromString(key)];
                }
                if (ret) {
                    id propertyValue = [dataSource valueForKey:key];

                    //该值不为NSNULL，并且也不为nil
                    if (![propertyValue isKindOfClass:[NSNull class]] && propertyValue != nil) {
                        SEL currentSEL = NSSelectorFromString(key);
                        if ([self respondsToSelector:currentSEL]) {
                            [self setValue:propertyValue forKey:key];
                        }
                    }
                }
            }
        }
    } else {
        ret = NO;
    }
    return ret;
}

- (BOOL)reflectViewFromObject:(NSObject *)dataSource {
    BOOL ret = NO;
    if ([dataSource isKindOfClass:[NSDictionary class]]) {
        NSArray *keys = [(NSDictionary *)dataSource allKeys];
        [keys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
            if ([self respondsToSelector:NSSelectorFromString(key)]) {
                NSString *result = [(NSDictionary *)dataSource objectForKey:key];
                if ([result isKindOfClass:[NSNumber class]]) {
                    result = [(NSNumber *)result stringValue];
                } else if ([result isKindOfClass:[NSNull class]]) {
                    result = @"";
                }

                UILabel *lblValue = [self valueForKey:key];
                if (lblValue && [lblValue respondsToSelector:@selector(setAttributedText:)] && [result isKindOfClass:[NSAttributedString class]]) {
                    lblValue.attributedText = (id)result;
                } else if (lblValue && [lblValue respondsToSelector:@selector(setText:)]) {
                    lblValue.text = result;
                }
            }
        }];
    }
    return ret;
}

- (BOOL)reflectViewFromObjectWithoutImageView:(NSObject *)dataSource {
    BOOL ret = NO;
    if ([dataSource isKindOfClass:[NSDictionary class]]) {
        NSArray *keys = [(NSDictionary *)dataSource allKeys];
        [keys enumerateObjectsUsingBlock:^(NSString *key, NSUInteger idx, BOOL *stop) {
            if ([self respondsToSelector:NSSelectorFromString(key)]) {
                NSString *result = [(NSDictionary *)dataSource objectForKey:key];
                if ([result isKindOfClass:[NSNumber class]]) {
                    result = [(NSNumber *)result stringValue];
                } else if ([result isKindOfClass:[NSNull class]]) {
                    result = @"";
                }
                
                UILabel *lblValue = [self valueForKey:key];
                if (lblValue && [lblValue respondsToSelector:@selector(setAttributedText:)] && [result isKindOfClass:[NSAttributedString class]]) {
                    lblValue.attributedText = (id)result;
                } else if (lblValue && ![lblValue isKindOfClass:[UIImageView class]] && [lblValue respondsToSelector:@selector(setText:)]) {
                    lblValue.text = result;
                }
            }
        }];
    }
    return ret;
}

#pragma mark--------------- deep Copy
- (void)encodeWithCoder:(NSCoder *)aCoder {
    NSArray *allKeys = self.propertyKeys;
    id objValue = nil;
    for (NSString *key in allKeys) {
        objValue = [self valueForKey:key];
        if (objValue) {
            [aCoder encodeObject:objValue forKey:key];
        }
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [self init];
    if (self) {
        NSArray *allKeys = self.propertyKeys;
        id objValue = nil;
        for (NSString *key in allKeys) {
            objValue = [aDecoder decodeObjectForKey:key];
            if (objValue)
                [self setValue:objValue forKey:key];
        }
    }
    return self;
}

- (instancetype)deepCopy {
    NSData *tempData = [NSKeyedArchiver archivedDataWithRootObject:self];
    id copyValue = nil;
    if (tempData) {
        copyValue = [NSKeyedUnarchiver unarchiveObjectWithData:tempData];
    }
    return copyValue;
}

#pragma mark - 动态添加方法

- (void)addMethod:(id)block withMethodName:(NSString *)aMethodName {
    IMP methodIMP = imp_implementationWithBlock(block);
    SEL methodSelector = sel_registerName([aMethodName UTF8String]);

    const char *types = "v@:";
    BOOL result = class_addMethod(self.class, methodSelector, methodIMP, types);
    if (!result) {
        class_replaceMethod(self.class, methodSelector, methodIMP, types);
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
}

@end
