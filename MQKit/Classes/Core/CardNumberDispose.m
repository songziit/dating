//
//  CardNumberDispose.m
//  Pods
//
//  Created by mazo on 6/4/15.
//
//

#import "CardNumberDispose.h"

@implementation CardNumberDispose

+ (NSString*)returnCardLastNumber:(NSString*)orginalString withCount:(NSInteger)count {
    if (orginalString.length <= count) {
        return orginalString;
    }

    NSInteger length = [orginalString length];
    NSInteger index = 0;
    ;
    NSInteger subStringFromIndex = 0;
    for (NSInteger i = length; i >= 0; --i) {
        index++;
        if (index == count) {
            subStringFromIndex = i;
            break;
        }
    }

    return [orginalString substringFromIndex:subStringFromIndex - 1];
}

@end
