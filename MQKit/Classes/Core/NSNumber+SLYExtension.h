//
//  NSNumber+SLYExtension.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/14.
//  Copyright (c) 2015年 MeiQi iOS Dev Team. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (SLYExtension)

/**
 *  12356.78 -> ￥ 12356.78
 *  12356 -> ￥ 12356.00
 *  12356.1 -> ￥ 12356.10
 *  @brief 返回价格
 */
@property(nonatomic, readonly) NSString *price;
@property(nonatomic, readonly) NSString *priceForInt;

- (NSAttributedString *)attributeStringAtPriceForFont:(UIFont *)aFont;
- (NSAttributedString *)attributeStringAtPriceIntForFont:(UIFont *)aFont;
- (NSAttributedString *)attributeStringAtSimplePriceForFont:(UIFont *)aFont;
- (NSAttributedString *)attributeStringAtPriceInGoodsList:(NSString *)price AtFont:(UIFont *)aFont;

- (NSString *)format:(NSString *)aForamt;


/**
 *  12356.78 -> ￥ 12356.78
 *  12356 -> ￥ 12356
 *  12356.1 -> ￥ 12356.1
 @return 价格
 */
- (NSString *)simplePrice;

- (NSString*)simpleStringValue2;

- (NSString*)stringValue2;

- (NSString*)kilometre;

- (NSString*)kilogram;

@end
