//
//  NSData+Util.m
//  CommonLayer
//
//  Created by shulianyong on 12/03/30.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NSData+Util.h"
#import "NSString+Util.h"

@implementation NSData (Util)

#pragma mark - Public Methods

+ (BOOL)isEmpty:(NSData*)aData {
    if (aData == nil) return YES;
    NSString* string = [[NSString alloc] initWithData:aData encoding:NSASCIIStringEncoding];
    return [NSString isEmpty:string];
}
@end