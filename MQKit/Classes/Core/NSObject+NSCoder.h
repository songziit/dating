//
//  NSObject+KeyedArchiver.h
//  Foomoo
//
//  Created by QFish on 7/19/14.
//  Copyright (c) 2014 QFish.inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (NSCoder)

- (NSData *)archivedData;

+ (instancetype)objectFromArchivedData:(NSData *)data;

/**
 *  @brief  获取归档文件
 *
 *  @param aFilePath 指定路径
 *
 *  @return 返回反归档后的数据
 */
+ (instancetype)objectFromArchivedFile:(NSString*)aFilePath;

/**
 *  @brief 将文件存在指定路径aFilePath
 *
 *  @param aFilePath 指定路径
 *
 *  @return 是否写文件 承购
 */
- (BOOL)writeToFile:(NSString*)aFilePath;
@end
