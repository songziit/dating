//
//  NSArray+Util.h
//  CommonLayer
//
//  Created by shulianyong on 12/03/30.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Util)

/**
 *  @brief  是否为array
 *
 *  @param obj id
 *
 *  @return BOOL
 */
+ (BOOL)isArray:(id)obj;

/**
 *  @brief  是否为array
 *
 *  @param obj id
 *
 *  @return BOOL
 */
+ (BOOL)isNotArray:(id)obj;

/**
 *  @brief  是否为array并且是否为空
 *
 *  @param obj  id
 *
 *  @return BOOL
 */
+ (BOOL)isEmpty:(id)obj;

/**
 *  @brief  是否为array并且是否为空
 *
 *  @param obj  id
 *
 *  @return BOOL
 */
+ (BOOL)isNotEmpty:(id)obj;

/**
 获取数组长度
 
 @param obj 数组
 @return 长度
 */
+ (NSInteger)count:(id)obj;

/**
 *  @brief 将数据用特殊符号隔断
 *
 *  @param aCharacter 特殊符号
 *
 *  @return 隔断后的字符串
 */
- (NSString *)stringWithCharacter:(NSString *)aCharacter;

/**
 *  @brief 筛选符合条件的用户
 *
 *  @param userArray       所有用户
 *  @param conditionString 筛选条件
 *
 *  @return 符合条件的用户数组
 */
- (NSArray *)filterUserArray:(NSArray *)userArray ThroughString:(NSString *)conditionString;

- (NSArray *)selected;

/**
 *  防止数组越界
 *
 *  @param index    NSUInteger
 *
 *  @return id
 */
- (id)objectAt:(NSUInteger)index;
@end
