//
//  DismissButton.h
//  iNoknok
//
//  Created by 舒联勇 on 14/8/26.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DismissButton : UIButton

/**
 *  @brief 按钮所在的ViewController
 */
@property (nonatomic,weak) IBOutlet UIViewController *parentController;


@end
