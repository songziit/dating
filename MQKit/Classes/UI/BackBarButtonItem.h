//
//  BackBarButtonItem.h
//  iNoknok
//
//  Created by 舒联勇 on 14/8/7.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BackBarButtonItem : UIBarButtonItem

/**
 *  @brief 按钮所在的ViewController
 */
@property (nonatomic,weak) IBOutlet UIViewController *parentController;


@end
