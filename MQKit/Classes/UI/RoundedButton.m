//
//  CircleButton.m
//  iNoknok
//
//  Created by 舒联勇 on 14/8/5.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import "RoundedButton.h"

@implementation RoundedButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.masksToBounds = YES;
    self.userInteractionEnabled = YES;
    self.layer.cornerRadius = self.bounds.size.height/2.0f;
}

@end
