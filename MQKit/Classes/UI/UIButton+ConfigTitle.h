//
//  UIButton+ConfigTitle.h
//  CommonLayer
//
//  Created by 舒联勇 on 14/10/7.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ConfigTitle)

@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *text;

/**
 *  @brief 设置文字
 *
 *  @param title 标题
 */
- (void)setTitle:(NSString *)title;

/**
 *  @brief 设置文字
 *
 *  @param title 标题
 */
- (void)setText:(NSString *)title;

@end
