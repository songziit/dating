//
//  ImaeTitleBarButtonItem.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/27.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "ImaeTitleBarButtonItem.h"

@implementation ImaeTitleBarButtonItem

- (void)awakeFromNib {
    [super awakeFromNib];
    [self custom];
}

- (void)prepareForInterfaceBuilder {
    [self custom];
}

- (void)custom {
    [self setBackgroundImage:[self backgroundImageForImage:self.leftImage]
                    forState:UIControlStateNormal
                  barMetrics:UIBarMetricsDefault];
}

- (UIImage*)backgroundImageForImage:(UIImage*)image {
    return [image stretchableImageWithLeftCapWidth:image.size.width - 2 topCapHeight:0];
}

@end
