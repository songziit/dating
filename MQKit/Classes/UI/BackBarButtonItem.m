//
//  BackBarButtonItem.m
//  iNoknok
//
//  Created by 舒联勇 on 14/8/7.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import "BackBarButtonItem.h"

@implementation BackBarButtonItem

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setTarget:self];
    [self setAction:@selector(click_back:)];
    
}

/**
 *  @brief 返回事件
 *
 *  @param sender
 */
- (void)click_back:(id)sender
{
    [self.parentController.navigationController popViewControllerAnimated:YES];
}

@end
