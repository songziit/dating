//
//  DismissBarButtonItem.m
//  iNoknok
//
//  Created by 舒联勇 on 14/8/5.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import "DismissBarButtonItem.h"

@implementation DismissBarButtonItem

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setTarget:self];
    [self setAction:@selector(disMiss:)];
    
}

- (void)disMiss:(id)sender
{
    [self.parentController dismissViewControllerAnimated:YES completion:nil];
}

@end
