//
//  DismissButton.m
//  iNoknok
//
//  Created by 舒联勇 on 14/8/26.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import "DismissButton.h"

@implementation DismissButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self addTarget:self action:@selector(disMiss:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)disMiss:(id)sender
{
    [self.parentController dismissViewControllerAnimated:YES completion:nil];
}

@end
