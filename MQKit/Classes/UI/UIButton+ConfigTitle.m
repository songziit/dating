//
//  UIButton+ConfigTitle.m
//  CommonLayer
//
//  Created by 舒联勇 on 14/10/7.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import "UIButton+ConfigTitle.h"

@implementation UIButton (ConfigTitle)

- (void)setTitle:(NSString *)title
{
    [self setTitle:title forState:UIControlStateNormal];
}

- (NSString *)title
{
    return [self titleForState:UIControlStateNormal];
}

- (void)setText:(NSString *)title
{
    [self setTitle:title];
}

- (NSString *)text
{
    return self.title;
}

@end
