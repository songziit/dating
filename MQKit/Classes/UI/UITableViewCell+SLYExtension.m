//
//  UITableViewCell+SLYExtension.m
//  MeiQiReferrer
//
//  Created by neil on 15/5/12.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "UITableViewCell+SLYExtension.h"

@implementation UITableViewCell (SLYExtension)

+ (CGFloat)height
{
    return 44.f;
}

@end
