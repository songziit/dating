//
//  BottomLineButton.m
//  iNoknok
//
//  Created by 舒联勇 on 14/6/10.
//  Copyright (c) 2014年 iNoknok.com. All rights reserved.
//

#import <CoreText/CoreText.h>
#import "BottomLineButton.h"

@implementation BottomLineButton

- (void)awakeFromNib {
    [super awakeFromNib];

    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text];

    [attString addAttribute:(NSString *)kCTUnderlineStyleAttributeName value:[NSNumber numberWithInt:kCTUnderlineStyleSingle] range:(NSRange){0, [attString length]}];
    self.titleLabel.attributedText = attString;

    //    self.myLabel.attributedText = attString;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];

    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self.titleLabel.text];

    [attString addAttribute:(NSString *)kCTUnderlineStyleAttributeName value:[NSNumber numberWithInt:kCTUnderlineStyleSingle] range:(NSRange){0, [attString length]}];
    self.titleLabel.attributedText = attString;
}

@end
