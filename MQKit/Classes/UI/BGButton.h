//
//  BGButton.h
//  UserCar
//
//  Created by 舒联勇 on 14-6-1.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import <UIKit/UIKit.h>

//IB_DESIGNABLE
@interface BGButton : UIButton

@property (nonatomic,assign) IBInspectable CGSize stretchableSize;
@property (nonatomic,assign) IBInspectable BOOL isTextColorImage;

@end
