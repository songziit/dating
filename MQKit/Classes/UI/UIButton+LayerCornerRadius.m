//
//  UIButton+LayerCornerRadius.m
//  MeiQiReferrer
//
//  Created by neil on 15/3/23.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "UIButton+LayerCornerRadius.h"

@implementation UIButton (LayerCornerRadius)

@dynamic cornerRadius,boardColor,borderWidth;
- (void)setCornerRadius:(CGFloat)cornerRadius
{
    self.layer.masksToBounds = YES;
    self.userInteractionEnabled = YES;
    self.layer.cornerRadius = cornerRadius;
}

- (void)setBoardColor:(UIColor *)boardColor
{
    if (self.layer.borderWidth == 0) {
        self.layer.borderWidth = 1;
    }
    self.layer.borderColor = boardColor.CGColor;
}

- (void)setBorderWidth:(CGFloat)borderWidth
{
    self.layer.borderWidth = borderWidth;
}

@end
