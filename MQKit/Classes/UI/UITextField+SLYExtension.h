//
//  UITextField+SLYExtension.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/2.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (SLYExtension)

@property (nonatomic,assign) IBInspectable CGFloat leftInset;
@property (nonatomic,strong) IBInspectable UIImage *leftImage;
@property (nonatomic,strong) IBInspectable UIColor *placeholderColor;

@end
