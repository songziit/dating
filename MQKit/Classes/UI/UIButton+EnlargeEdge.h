//
//  UIButton+EnlargeEdge.h
//  iNoknok
//
//  Created by 舒联勇 on 14/7/23.
//  Copyright (c) 2014年 iNoknok.com. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  @brief 扩大响应区域
 */
@interface UIButton (EnlargeEdge)

/**
 *  扩大整个按钮的大小
 */
@property (nonatomic,assign,readwrite) IBInspectable CGFloat enlargeEdge;

/**
 *  @brief 按需扩大按钮大小
 *
 *  @param topEdge    上边距
 *  @param rightEdge  右边距
 *  @param bottomEdge 下边距
 *  @param leftEdge   左边距
 */
- (void)setEnlargeEdgeWithTop:(CGFloat)topEdge
                        right:(CGFloat)rightEdge
                       bottom:(CGFloat)bottomEdge
                         left:(CGFloat)leftEdge;

@end