//
//  ImageButton.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/17.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "ImageButton.h"

@implementation ImageButton
@dynamic rightImage,topImage;

- (void)setTitle:(NSString *)title forState:(UIControlState)state
{
    [super setTitle:title forState:state];
    if (self.rightImage) {
        self.rightImage = self.rightImage;
    }
    
    if (self.topImage) {
        self.topImage = self.topImage;
    }
}

@end
