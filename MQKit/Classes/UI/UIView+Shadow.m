//
//  UIView+Shadow.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/1.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "UIView+Shadow.h"

@implementation UIView (Shadow)
@dynamic shadowEnable;

- (void)setShadowEnable:(BOOL)shadowEnable
{
    if (shadowEnable) {
        self.layer.shadowOpacity = 0.5f;
        self.layer.shadowOffset = CGSizeMake(0, -2);
        self.layer.shadowColor = [[UIColor colorWithRed:207.0/255 green:207.0/255 blue:207.0/255 alpha:1] CGColor];
        self.layer.shadowRadius = 2.f;
    }
}

- (BOOL)shadowEnable
{
    return self.layer.shadowRadius>0;
}

- (void)setRadiusShadowColor:(UIColor *)radiusShadowColor{
    // 阴影
    self.layer.masksToBounds = NO;
    self.layer.shadowOpacity = 1;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowRadius = 10;
    
    // 任意圆角
    CGPathRef path = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                           byRoundingCorners:UIRectCornerTopLeft cornerRadii:CGSizeMake(40, 40)].CGPath;
    CAShapeLayer *lay = [CAShapeLayer layer];
    lay.path = path;
    self.layer.mask = lay;
    
}

@end
