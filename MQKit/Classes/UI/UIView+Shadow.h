//
//  UIView+Shadow.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/1.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Shadow)

/**
 *  是否开启顶部阴影
 */
@property (nonatomic,assign) IBInspectable BOOL shadowEnable;

/**
 圆角阴影color
 */
@property (strong,nonatomic) IBInspectable UIColor *radiusShadowColor;

@end
