//
//  ImaeTitleBarButtonItem.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/27.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

//IB_DESIGNABLE
@interface ImaeTitleBarButtonItem : UIBarButtonItem

@property (nonatomic,strong) IBInspectable UIImage *leftImage;

@end
