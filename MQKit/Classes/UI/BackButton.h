//
//  BackButton.h
//  iNoknok
//
//  Created by 舒联勇 on 14/6/13.
//  Copyright (c) 2014年 iNoknok.com. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  @brief NavigationController的popup按钮
 */
@interface BackButton : UIButton

/**
 *  @brief 按钮所在的ViewController
 */
@property (nonatomic,weak) IBOutlet UIViewController *parentController;

@end
