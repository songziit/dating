//
//  BottomLineButton.h
//  iNoknok
//
//  Created by 舒联勇 on 14/6/10.
//  Copyright (c) 2014年 iNoknok.com. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  @brief 有下划画的按钮
 */
@interface BottomLineButton : UIButton

@end
