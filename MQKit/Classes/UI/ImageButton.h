//
//  ImageButton.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/17.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageButton : UIButton

@property (nonatomic,strong) IBInspectable UIImage *rightImage;

@property (nonatomic,strong) IBInspectable UIImage *topImage;


@end
