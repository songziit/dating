//
//  UITableViewCell+SLYExtension.h
//  MeiQiReferrer
//
//  Created by neil on 15/5/12.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (SLYExtension)

/**
 *   @brief  cell的高度，已知的高度
 *
 *   @return cell的高度，已知的高度
 */
+ (CGFloat)height;

@end
