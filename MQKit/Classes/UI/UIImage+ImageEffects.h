//
//  UIImage+ImageEffects.h
//  CommonLayer
//
//  Created by 舒联勇 on 14-5-21.
//  Copyright (c) 2014年 iNoknok.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageEffects)

/**
 *  @brief 轻量级毛玻璃
 *
 *  @return 毛玻璃图片
 */
- (UIImage *)applyLightEffect;
/**
 *  @brief 更深的毛玻璃
 *
 *  @return 毛玻璃图片
 */
- (UIImage *)applyExtraLightEffect;

/**
 *  @brief 黑色毛玻璃
 *
 *  @return 毛玻璃图片
 */
- (UIImage *)applyDarkEffect;

/**
 *  @brief 混合颜色的毛玻璃
 *
 *  @param tintColor 颜色值
 *
 *  @return 毛玻璃图片
 */
- (UIImage *)applyTintEffectWithColor:(UIColor *)tintColor;


- (UIImage *)applyBlurWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;

- (UIImage *)applyBlurWithCrop:(CGRect) bounds resize:(CGSize) size blurRadius:(CGFloat) blurRadius tintColor:(UIColor *) tintColor saturationDeltaFactor:(CGFloat) saturationDeltaFactor maskImage:(UIImage *) maskImage;

@end
