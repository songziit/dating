//
//  BottomBGButton.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/7.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "BottomBGButton.h"
#import <MQKit/UIView+Helper.h>

@implementation BottomBGButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self customize];
}

- (void)prepareForInterfaceBuilder
{
    [super prepareForInterfaceBuilder];
    [self customize];
}

- (void)customize
{
    UIImage *temp = [self backgroundImageForState:UIControlStateNormal];
    if (temp) {
        temp = [self backgroundImageForImage:temp];
        [self setBackgroundImage:temp forState:UIControlStateNormal];
    }
    
    UIImage *imgSelected = [self backgroundImageForState:UIControlStateSelected];
    if (imgSelected) {
        imgSelected = [self backgroundImageForImage:imgSelected];
        [self setBackgroundImage:imgSelected forState:UIControlStateSelected];
    }
    
    UIImage *hTemp = [self backgroundImageForState:UIControlStateHighlighted];
    if (hTemp) {
        hTemp = [self backgroundImageForImage:hTemp];
        [self setBackgroundImage:hTemp forState:UIControlStateHighlighted];
    }
}

- (UIImage*)backgroundImageForImage:(UIImage*)image
{
    UIColor *color = [UIColor clearColor];
    CGFloat width = self.width;
    CGFloat height = self.height;
    CGRect bounds = CGRectMake(0,0,width,height);
    
    UIGraphicsBeginImageContextWithOptions(bounds.size, NO, [UIScreen mainScreen].scale);
    CGContextRef bitmapContext = UIGraphicsGetCurrentContext();
    CGContextSaveGState(bitmapContext);
    
    CGContextSetFillColorWithColor(bitmapContext, color.CGColor);
    CGContextFillRect(bitmapContext, bounds);
    
    CGRect maskFrame = self.bounds;
    maskFrame.size = image.size;
    maskFrame.origin.y = self.height-image.size.height;
    maskFrame.origin.x = self.width/2-image.size.height/2;
    [image drawInRect:maskFrame];
    
    UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
    return coloredImage;
}

@end
