//
//  BGButton.m
//  UserCar
//
//  Created by 舒联勇 on 14-6-1.
//  Copyright (c) 2014年 shulianyong. All rights reserved.
//

#import "BGButton.h"
#import <MQKit/UIImage+Util.h>

@implementation BGButton

#pragma mark - 生命周期

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self customize];
}

- (void)prepareForInterfaceBuilder
{
    [self customize];
}

#pragma mark - 设置

- (void)customize
{
    UIImage *temp = [self backgroundImageForState:UIControlStateNormal];
    if (temp) {
        if (self.isTextColorImage) {
            temp = [temp imageTintedWithColor:[self titleColorForState:UIControlStateNormal]];
        }
        [self setBackgroundImage:temp forState:UIControlStateNormal];
    }
    
    UIImage *imgSelected = [self backgroundImageForState:UIControlStateSelected];
    if (imgSelected) {
        if (self.isTextColorImage) {
            imgSelected = [imgSelected imageTintedWithColor:[self titleColorForState:UIControlStateSelected]];
        }
        [self setBackgroundImage:imgSelected forState:UIControlStateSelected];
    }
    
    UIImage *hTemp = [self backgroundImageForState:UIControlStateHighlighted];
    if (hTemp) {
        if (self.isTextColorImage) {
            hTemp = [hTemp imageTintedWithColor:[self titleColorForState:UIControlStateHighlighted]];
        }
        [self setBackgroundImage:hTemp forState:UIControlStateHighlighted];
    }
    
    if (temp) {
        UIImage *disableImage = [temp imageTintedWithColor:[self titleColorForState:UIControlStateDisabled]];
        [self setBackgroundImage:disableImage forState:UIControlStateDisabled];
    }
}

#pragma  mark - 属性管理

- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state
{
    CGSize imageSize = self.stretchableSize;
    if (CGSizeEqualToSize(imageSize, CGSizeZero))
    {
        imageSize = CGSizeMake(image.size.width/2, image.size.height/2);
    }    
    UIImage *temp = [image stretchableImageWithLeftCapWidth:imageSize.width topCapHeight:imageSize.height];
    [super setBackgroundImage:temp forState:state];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
