//
//  BackButton.m
//  iNoknok
//
//  Created by 舒联勇 on 14/6/13.
//  Copyright (c) 2014年 iNoknok.com. All rights reserved.
//

#import "BackButton.h"

@implementation BackButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)back:(id)sender
{
    [self.parentController.navigationController popViewControllerAnimated:YES];
}

@end
