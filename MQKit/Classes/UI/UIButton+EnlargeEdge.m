//
//  UIButton+EnlargeEdge.m
//  iNoknok
//
//  Created by 舒联勇 on 14/7/23.
//  Copyright (c) 2014年 iNoknok.com. All rights reserved.
//

#import "UIButton+EnlargeEdge.h"
#import <objc/runtime.h>

@implementation UIButton (EnlargeEdge)

@dynamic enlargeEdge;

static char topNameKey;
static char rightNameKey;
static char bottomNameKey;
static char leftNameKey;

/**
 *  @brief 扩大整个按钮的大小
 *
 *  @param aEdge 需要扩大的大小
 */
- (void)setEnlargeEdge:(CGFloat)aEdge
{
    [self setEnlargeEdgeWithTop:aEdge right:aEdge bottom:aEdge left:aEdge];
}

/**
 *  @brief 按需扩大按钮大小
 *
 *  @param topEdge    上边距
 *  @param rightEdge  右边距
 *  @param bottomEdge 下边距
 *  @param leftEdge   左边距
 */
- (void)setEnlargeEdgeWithTop:(CGFloat)topEdge
                        right:(CGFloat)rightEdge
                       bottom:(CGFloat)bottomEdge
                         left:(CGFloat)leftEdge
{
    
    objc_setAssociatedObject(self, &topNameKey, @(topEdge), OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &rightNameKey, @(rightEdge), OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &bottomNameKey, @(bottomEdge), OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &leftNameKey, @(leftEdge), OBJC_ASSOCIATION_COPY_NONATOMIC);
}

/**
 *  @brief 扩大的按钮点击区域
 *
 *  @return 点击区域
 */
- (CGRect)enlargedRect
{
    
    NSNumber* topEdge = objc_getAssociatedObject(self, &topNameKey);
    NSNumber* rightEdge = objc_getAssociatedObject(self, &rightNameKey);
    NSNumber* bottomEdge = objc_getAssociatedObject(self, &bottomNameKey);
    NSNumber* leftEdge = objc_getAssociatedObject(self, &leftNameKey);
    
    if (topEdge && rightEdge && bottomEdge && leftEdge)
    {
        return CGRectMake(self.bounds.origin.x - leftEdge.floatValue,
                          self.bounds.origin.y - topEdge.floatValue,
                          self.bounds.size.width + leftEdge.floatValue + rightEdge.floatValue,
                          self.bounds.size.height + topEdge.floatValue + bottomEdge.floatValue);
    }
    else
    {
        return self.bounds;
    }
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    CGRect rect = [self enlargedRect];
    
    if (CGRectEqualToRect(rect, self.bounds))
    {
        return [super pointInside:point withEvent:event];
    }
    return CGRectContainsPoint(rect, point);
}

@end
