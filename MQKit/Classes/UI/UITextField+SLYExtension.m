//
//  UITextField+SLYExtension.m
//  MeiQiReferrer
//
//  Created by neil on 15/4/2.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import "UITextField+SLYExtension.h"
#import <objc/runtime.h>
#import "UIView+Helper.h"

static const char LeftInsetKey,LeftImageKey,PlaceholderColorKey;

@implementation UITextField (SLYExtension)
@dynamic leftInset,leftImage;

- (void)setLeftInset:(CGFloat)leftInset
{
    objc_setAssociatedObject(self, &LeftInsetKey, @(leftInset), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    UIView *leftView = self.leftView;
    if (leftView==nil) {
        leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leftInset, 1)];
        leftView.backgroundColor = [UIColor clearColor];
        self.leftViewMode = UITextFieldViewModeAlways;
        self.leftView = leftView;
    }
}

- (CGFloat)leftInset
{
    NSNumber *leftInset = objc_getAssociatedObject(self, &LeftInsetKey);
    return leftInset?leftInset.floatValue:0.f;
}

- (void)setLeftImage:(UIImage *)leftImage
{
    objc_setAssociatedObject(self, &LeftImageKey, leftImage, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    UIImageView *image = [[UIImageView alloc] initWithImage:leftImage];
    image.contentMode = UIViewContentModeRight;
    if (self.leftInset) {
        image.width = self.leftInset;
    }
    else
    {
        image.width = leftImage.size.width*3/2;
    }
    self.leftViewMode = UITextFieldViewModeAlways;
    self.leftView = image;
}

- (UIImage *)leftImage
{
    return objc_getAssociatedObject(self, &LeftImageKey);
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    objc_setAssociatedObject(self, &PlaceholderColorKey, placeholderColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self setValue:placeholderColor forKeyPath:@"_placeholderLabel.textColor"];
}

- (UIColor *)placeholderColor
{
    return objc_getAssociatedObject(self,&PlaceholderColorKey);
}

@end
