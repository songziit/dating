//
//  UIView+SubviewLayout.h
//  Common
//
//  Created by 舒联勇 on 14/10/13.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SubviewLayout)


/**
 *  @brief 设置子View
 *
 *  @param subview 子View
 */
- (void)setSubview:(UIView *)subview;

/**
 *  @brief 根据边距，设置子View
 *
 *  @param subview 子View
 *  @param edge    内边距
 */
- (void)setSubview:(UIView *)subview withEdge:(UIEdgeInsets)edge;

/**
 *  @brief 添加子View
 *
 *  @param subview 子View
 *  @param edge    边距
 */
- (void)addSubview:(UIView *)subview withEdge:(UIEdgeInsets)edge;

/**
 *  @brief 添加子view
 *
 *  @param subview 子view
 *  @param index   位置
 *  @param edge    边距
 */
- (void)insertSubview:(UIView *)subview atIndex:(NSInteger)index withEdge:(UIEdgeInsets)edge;;

/**
 *  @brief 设置约束大小
 *
 *  @param size 大小
 */
- (void)setConstraintSize:(CGSize)size;

- (void)layoutSubView:(UIView *)subView toEdge:(NSLayoutAttribute)attribute withConstant:(CGFloat)constant;

- (void)setBoardColor:(UIColor*)aColor;

#pragma mark - 只读属性
- (NSLayoutConstraint*)topConstant;
- (NSLayoutConstraint*)bottomConstant;
- (NSLayoutConstraint*)leftConstant;
- (NSLayoutConstraint*)rightConstant;

@end
