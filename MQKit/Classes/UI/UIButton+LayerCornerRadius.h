//
//  UIButton+LayerCornerRadius.h
//  MeiQiReferrer
//
//  Created by neil on 15/3/23.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (LayerCornerRadius)

@property (nonatomic,assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic,strong) IBInspectable UIColor *boardColor;
@property (nonatomic,assign) IBInspectable CGFloat borderWidth;

@end
