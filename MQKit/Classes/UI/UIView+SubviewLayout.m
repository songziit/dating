//
//  UIView+SubviewLayout.m
//  Common
//
//  Created by 舒联勇 on 14/10/13.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import "UIView+SubviewLayout.h"

@implementation UIView (SubviewLayout)

- (void)setSubview:(UIView *)subview
{
    [self setSubview:subview withEdge:UIEdgeInsetsZero];
}

- (void)setSubview:(UIView *)subview withEdge:(UIEdgeInsets)edge
{
    for (UIView *temp in self.subviews) {
        [temp removeFromSuperview];
    }
    [self addSubview:subview withEdge:edge];
}

- (void)addSubview:(UIView *)subview withEdge:(UIEdgeInsets)edge
{
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:subview];
    [self configSubView:subview withEdge:edge];
    [self layoutIfNeeded];
}

- (void)insertSubview:(UIView *)subview atIndex:(NSInteger)index withEdge:(UIEdgeInsets)edge
{
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    [self insertSubview:subview atIndex:index];
    [self configSubView:subview withEdge:edge];
    [self layoutIfNeeded];
}

- (void)setConstraintSize:(CGSize)size
{
    [self addConstraint: [NSLayoutConstraint constraintWithItem:self
                                 attribute:NSLayoutAttributeWidth
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self
                                 attribute:NSLayoutAttributeWidth
                                multiplier:1.0f
                                  constant:size.width]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self
                                 attribute:NSLayoutAttributeHeight
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:self
                                 attribute:NSLayoutAttributeHeight
                                multiplier:1.0f
                                  constant:size.height]];
}

#pragma mark util

- (void)configSubView:(UIView *)subview withEdge:(UIEdgeInsets)edge
{
    [self layoutSubView:subview toEdge:NSLayoutAttributeLeft withConstant:edge.left];
    [self layoutSubView:subview toEdge:NSLayoutAttributeRight withConstant:edge.right];
    [self layoutSubView:subview toEdge:NSLayoutAttributeTop withConstant:edge.top];
    [self layoutSubView:subview toEdge:NSLayoutAttributeBottom withConstant:edge.bottom];
}

- (void)layoutSubView:(UIView *)subView toEdge:(NSLayoutAttribute)attribute withConstant:(CGFloat)constant
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:subView
                                                          attribute:attribute
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:self
                                                          attribute:attribute
                                                         multiplier:1.0f
                                                           constant:constant]];
}

- (void)setBoardColor:(UIColor*)aColor
{
    self.layer.borderColor = aColor.CGColor;
}

#pragma mark - 只读属性

- (NSLayoutConstraint*)constantWithAttribute:(NSLayoutAttribute)attribute
{
    NSLayoutConstraint *retConstant = nil;
    for (NSLayoutConstraint *temp in self.superview.constraints) {
        if ((temp.firstItem == self && temp.firstAttribute == attribute) || (temp.secondItem == self && temp.secondAttribute == attribute)) {
            retConstant = temp;
        }
    }
    return retConstant;
}

- (NSLayoutConstraint*)topConstant
{
    return [self constantWithAttribute:NSLayoutAttributeTop];
}
- (NSLayoutConstraint*)bottomConstant
{
    return [self constantWithAttribute:NSLayoutAttributeBottom];
}

- (NSLayoutConstraint*)leftConstant
{
    return [self constantWithAttribute:NSLayoutAttributeLeft];
    
}
- (NSLayoutConstraint*)rightConstant
{
    return [self constantWithAttribute:NSLayoutAttributeRight];
}

@end
