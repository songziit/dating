//
//  UIView+SLYExtension.h
//  MeiQiReferrer
//
//  Created by neil on 15/4/3.
//  Copyright (c) 2015年 YingTongLeJia iOS Dev Team. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SLYExtension)

/**
 *   @brief  对应的ViewController
 */
@property (nonatomic,readonly) UIViewController *viewController;

@property (nonatomic,assign) IBInspectable CGFloat cornerRadius;
@property (nonatomic,strong) IBInspectable UIColor *boardColor;
@property (nonatomic,assign) IBInspectable CGFloat borderWidth;

+ (void)setExclusiveTouchForButtons:(UIView*)view withExclusiveTouch:(BOOL)exclusiveTouch;

@end
