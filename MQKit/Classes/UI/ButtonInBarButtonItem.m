//
//  ButtonInBarButtonItem.m
//  iNoknok
//
//  Created by 舒联勇 on 14/9/17.
//  Copyright (c) 2014年 iNoknok. All rights reserved.
//

#import "ButtonInBarButtonItem.h"

@implementation ButtonInBarButtonItem

- (void)awakeFromNib
{
    [super awakeFromNib];
    UIView *customView = self.customView;
    if ([customView isKindOfClass:UIButton.class]) {
        UIButton *btnCustomView = (UIButton*)customView;
        
        UIColor *enableColor = self.tintColor;
        UIColor *unableColor = [self.tintColor colorWithAlphaComponent:0.3];
        
        [btnCustomView setTitleColor:enableColor forState:UIControlStateNormal];
        [btnCustomView setTitleColor:unableColor forState:UIControlStateDisabled];
        
        btnCustomView.tintColor = self.tintColor;
    }
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    UIView *customView = self.customView;
    if ([customView isKindOfClass:UIButton.class]) {
        UIButton *btnCustomView = (UIButton*)customView;
        btnCustomView.enabled = enabled;
    }
}

@end
